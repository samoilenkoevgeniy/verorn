<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormProcessCertificate extends Model
{
    protected $fillable = [
        "cert_id", "form_process_id", "value", "date"
    ];
}
