<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationCertificate extends Model
{
    protected $fillable = [
        'id',
        "type",
        "number",
        "date",
        "expiration_date",
        "job_application_id",
    ];

    /**
     * @param $value
     */
    public function setDateAttribute($value)
    {
        try {
            $pieces = explode("/", $value);
            $this->attributes['date'] = $pieces[2]."-".$pieces[0]."-".$pieces[1];
        }catch (\Exception $e) {
            $this->attributes['date'] = "00-00-0000";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getDateAttribute($value)
    {
        try {
            $pieces = explode("-", $value);
            return $pieces[1]."/".$pieces[2]."/".$pieces[0];
        } catch(\Exception $e) {
            return "00/00/0000";
        }
    }

    /**
     * @param $value
     */
    public function setExpirationDateAttribute($value)
    {
        $pieces = explode("/", $value);
        try {
            $this->attributes['expiration_date'] = $pieces[2]."-".$pieces[0]."-".$pieces[1];
        }catch (\Exception $e) {
            $this->attributes['expiration_date'] = "00-00-0000";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getExpirationDateAttribute($value)
    {
        $pieces = explode("-", $value);
        try{
            return $pieces[1]."/".$pieces[2]."/".$pieces[0];
        }catch (\Exception $e) {
            return "00/00/0000";
        }
    }
}
