<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationFormExperience extends Model
{
    protected $fillable = [
        "name", "form_id"
    ];
}
