<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobApplication extends Model
{
    use SoftDeletes;

    protected $fillable = [
        "question_1",
        "question_2",
        "question_3",
        "first_name",
        "mi",
        "last_name",
        "birth_date",
        "maiden",
        "current_address",
        "city",
        "city_state",
        "zip_code",
        "good_until",
        "permanent_address",
        "home_phone",
        "work_phone",
        "cellular_phone",
        "social_security",
        "email_address",
        "employment_desired",
        "date_available",
        "shift_preferred",
        "how_did_hear",
        "emergency_contact",
        "relationship",
        "contact_phone",
        "other",
        "crime",
        "license",
        "limitations",
        "privileges",
        "malpractice_claim",
        "submit_criminal_background",
        "name_signature",
        "date_signature",
        "user_id",
        "permanent_city",
        "permanent_city_state",
        "permanent_zip_code",
        "crime_description",
        "license_description",
        "limitations_description",
        "privileges_description",
        "malpractice_claim_description",
        "total_years_of_exp",
        "date_pass_exams",
        "not_rn",
        "edu_pass_board_year",
        "Declined",
        "GenderCode",
        "IsHispanic",
        "PrimaryRaceID",
        "SecondaryRaceID",
        "IsSpecialDisabled",
        "IsVietnamVeteran",
        "IsOther",
        "IsRecentlySeparated",
        "IsDisabledVeteran",
        "IsWarVeteran",
        "IsNonCombat",
        "IsRecentlySeparatedNew",
        "IsDisabled",
        "DisabilityDescription",
        "united_states_citizen"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specialities()
    {
        return $this->hasMany(JobApplicationSpeciality::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emrs()
    {
        return $this->hasMany(JobApplicationEmr::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function licensies()
    {
        return $this->hasMany(JobApplicationLicense::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function educations()
    {
        return $this->hasMany(JobApplicationEducation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certificates()
    {
        return $this->hasMany(JobApplicationCertificate::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employments()
    {
        return $this->hasMany(JobApplicationEmployment::class);
    }

    /**
     * @return mixed
     */
    public function getJobApplication()
    {
        $user_id = \Auth::user()->id;
        $jobApplication = JobApplication::whereUserId($user_id)->first();
        return $jobApplication;
    }

    /**
     * @param $value
     */
    public function setBirthDateAttribute($value)
    {
        $pieces = explode("/", $value);
        try {
            $this->attributes['birth_date'] = $pieces[2]."-".$pieces[0]."-".$pieces[1];
        }catch (\Exception $e) {
            $this->attributes['birth_date'] = "00-00-0000";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getBirthDateAttribute($value)
    {
        $pieces = explode("-", $value);
        try {
            return $pieces[1]."/".$pieces[2]."/".$pieces[0];
        }catch (\Exception $e) {
            return "00/00/0000";
        }
    }

    /**
     * @param $value
     */
    public function setDateSignatureAttribute($value)
    {
        $pieces = explode("/", $value);
        try {
            $this->attributes['date_signature'] = $pieces[2]."-".$pieces[0]."-".$pieces[1];
        }catch (\Exception $e) {
            $this->attributes['date_signature'] = "00-00-0000";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getDateSignatureAttribute($value)
    {
        $pieces = explode("-", $value);
        try {
            return $pieces[1]."/".$pieces[2]."/".$pieces[0];
        }catch (\Exception $e) {
            return "00/00/0000";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $value);
        return $carbon->format("m-d-Y g:i A")." EST";
    }

    public static function isFilled($user_id)
    {
        if(JobApplication::whereUserId($user_id)->first()) {
            return true;
        } else {
            return false;
        }
    }
}
