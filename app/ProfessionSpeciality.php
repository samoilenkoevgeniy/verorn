<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessionSpeciality extends Model
{
    protected $fillable = [
        "profession_id", "speciality_id"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function speciality()
    {
        return $this->belongsTo(Speciality::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profession()
    {
        return $this->belongsTo(Profession::class);
    }
}
