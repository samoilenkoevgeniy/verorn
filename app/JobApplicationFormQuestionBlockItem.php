<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationFormQuestionBlockItem extends Model
{
    protected $fillable = [
        "text", "is_question", "parent_id", "block_id"
    ];

    public static function boot(){

        parent::boot();

        static::creating(function($obj){
            if($obj->text == ""){
                return;
            }
        });
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questionBlock()
    {
        return $this->belongsTo(JobApplicationFormQuestionBlock::class, "block_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(JobApplicationFormQuestionBlockItem::class, "parent_id");
    }
}
