<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryState extends Model
{
    protected $fillable = [
        "text", "country", "abbrev"
    ];
}
