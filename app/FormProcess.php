<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class FormProcess extends Model
{
    protected $fillable = [
        "first_name", "last_name", "social_number",
        "date", "email",
        "age_specific_1", "age_specific_2", "age_specific_3",
        "print_name", "signature", "conf_date",
        "user_id", "form_id",
        "app_care"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formProcessQuestions()
    {
        return $this->hasMany(FormProcessQuestion::class, "form_process_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formProcessCertificates()
    {
        return $this->hasMany(FormProcessCertificate::class, "form_process_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function formProcessExperiences()
    {
        return $this->hasMany(FormProcessExperience::class, "form_process_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sourceForm()
    {
        return $this->belongsTo(JobApplicationForm::class, "form_id");
    }

    /**
     * @param $value
     */
    public function setDateAttribute($value)
    {
        $pieces = explode("/", $value);
        try{
            $this->attributes['date'] = $pieces[2]."-".$pieces[0]."-".$pieces[1];
        }catch (\Exception $e) {
            $this->attributes['date'] = "0000-00-00";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getDateAttribute($value)
    {
        try {
            $pieces = explode("-", $value);
            return $pieces[1]."/".$pieces[2]."/".$pieces[0];
        }catch (\Exception $e) {
            return "00-00-0000";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $value);
        return $carbon->format("m-d-Y g:i A")." EST";
    }

    /**
     * @param $user_id
     * @param $form_id
     * @return bool
     */
    public static function isFilled($user_id, $form_id)
    {
        if(FormProcess::whereUserId($user_id)->whereFormId($form_id)->first()) {
            return true;
        } else {
            return false;
        }
    }
}
