<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationFormCertificate extends Model
{
    protected $fillable = [
        "name", "text", "form_id", "certificate[2][date]"
    ];
}
