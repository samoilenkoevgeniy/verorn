<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationForm extends Model
{
    protected $fillable = [
        "name", "header", "age_specific_block",
        "primary_experience_block", "certification_block"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questionBlocks()
    {
        return $this->hasMany(JobApplicationFormQuestionBlock::class, "form_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certificates()
    {
        return $this->hasMany(JobApplicationFormCertificate::class, "form_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function experiences()
    {
        return $this->hasMany(JobApplicationFormExperience::class, "form_id");
    }
}
