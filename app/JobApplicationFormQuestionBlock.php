<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationFormQuestionBlock extends Model
{
    protected $fillable = [
        "name", "initials", "form_id", "left"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo(JobApplicationForm::class, "form_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(JobApplicationFormQuestionBlockItem::class, "block_id")->whereNull("parent_id");
    }

    /**
     * @return mixed
     */
    public function allItems()
    {
        return $this->hasMany(JobApplicationFormQuestionBlockItem::class, "block_id");
    }
}
