<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        if(\Auth::user()->role == "applicant") {
            $user = \Auth::user();
            if(!\Cache::get("who_is_my_recruiter_".$user->id)) {
                if($user->user_id) {
                    $recruiter = User::where("user_id", $user->user_id)->first();
                    \Cache::add("who_is_my_recruiter_".$user->id, "Your recruiter is <strong>".$recruiter->name."</strong>", 120);
                } else {
                    \Cache::add("who_is_my_recruiter_".$user->id, "You do not have a recruiter <a href='#' data-toggle='modal' data-target='#chooseRecruiter'>choose</a>", 120);
                }
            }
        }

        return $next($request);
    }
}
