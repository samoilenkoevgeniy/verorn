<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserProfileUpdateRequest;
use App\JobApplication;
use App\ProfessionSpeciality;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{

    /**
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = \Auth::user();
        return response()->view("applicant.profile.edit", compact("user"));
    }

    /**
     * @param int $user_id
     * @return \Illuminate\Http\Response
     */
    public function editRecruiter($user_id = 0)
    {
        $user = User::find($user_id);
        $recruiter = 1;
        return response()->view("applicant.profile.edit", compact("user", "recruiter"));
    }

    /**
     * @param UserProfileUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserProfileUpdateRequest $request)
    {
        $data = $request->all();
        if($request->has("password")) {
            $data['password'] = \Hash::make($request->input("password"));
        } else {
            unset($data['password']);
        }
        if(!$request->input("user_id")) {
            $user = \Auth::user();
        } else {
            $user = User::find($request->input("user_id"));
        }
        unset($data['user_id']);
        $user->update($data);
        if($request->input("redirect")) {
            return redirect()->back()->with("message", "Profile Update Successful");
        } else {
            return redirect($request->input("redirect"))->with("message", "Recruiter Update Successful!");
        }
    }

    /**
     * @param $user_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($user_id)
    {
        $users = User::where("user_id", $user_id)->get();
        foreach($users as $user) {
            $user->user_id = NULL;
            $user->save();
        }
        $jobApplications = JobApplication::where("user_id", $user_id)->get();
        foreach($jobApplications as $application)
        {
            $application->delete();
        }
        $user = User::find($user_id);
        $user->delete();

        return redirect()->back()->with("message", "Success!");
    }

    /**
     * @param int $user_id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resetPassword($user_id = 0, Request $request)
    {
        $new_password = $request->input("password");
        $user = User::find($user_id);
        $user->password = \Hash::make($new_password);
        $user->save();
        if(ENV("EMAIL", true)) {
            \Mail::send('auth.emails.newPassword', ['user' => $user, "password" => $new_password], function ($m) use ($user) {
                $m->from('noreply@verornportal.com', 'Vero RN Portal');
                $m->to($user->email, $user->name)->subject('Password Reset');
            });
        }
        return redirect()->back()->with("message", "Password successfully reset");
    }

    /**
     * @param int $profession_id
     * @return array
     */
    public function getSpecialities($profession_id = 0)
    {
        $result = [];
        $items = ProfessionSpeciality::whereProfessionId($profession_id)->get();
        foreach($items as $item) {
            $result[] = $item->speciality;
        }
        return $result;
    }

}
