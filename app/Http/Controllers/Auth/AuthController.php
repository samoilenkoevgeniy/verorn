<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'referral_email' => 'email',
            //'speciality' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $data['password'] = \Hash::make($data['password']);
        return User::create($data);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        $data = $request->all();
        if($request->has("referral_email")) {
            $referral_email = $request->input("referral_email");
            $user = User::where("email", $referral_email)->where("role", "recruiter")->first();
            if(!$user) {
                return redirect()->back()->with("error_referral", "Recruiter with any e-mail found")->withInput();
            } else {
                $data['user_id'] = $user->id;
            }
        } else {
            $data['user_id'] = NULL;
        }
        $data['role'] = "applicant";
        Auth::guard($this->getGuard())->login($this->create($data));

        if(ENV("EMAIL", true)) {
            foreach(User::whereRole("administrator")->get() as $user) {
                \Mail::send('admin.emails.newCandidate', ['candidate' => $data], function ($m) use ($user) {
                    $m->from('noreply@verornportal.com', 'Vero RN Portal');
                    $m->to($user->email, $user->name)->subject('New Candidate Registered');
                });
            }
        }

        return redirect($this->redirectPath());
    }
}
