<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RecruiterController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['role'] = "recruiter";
        $data['password'] = \Hash::make($data['password']);
        User::create($data);
        return redirect()->back()->with("message", "Successfully create a recruiter!");
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getRecruiters(Request $request)
    {
        $users = User::where("role", "recruiter")->get();
        $current_user = User::find($request->input("user_id"));
        $result = "<select data-user-id='".$request->input("user_id")."' class='setRecruiterSelect' name='user_id'>";
        $result .= "<option value='0'>No recruiter</option>";
        foreach($users as $user) {
            if($current_user->user_id == $user->id) {
                $result .= "<option value='".$user->id."' selected>".$user->name."</option>";
            } else {
                $result .= "<option value='".$user->id."'>".$user->name."</option>";
            }
        }
        $result .= "</select>";
        return $result;
    }

}
