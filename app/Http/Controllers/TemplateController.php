<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TemplateController extends Controller
{
    /**
     * @param string $thing
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getTemplate($thing = "", Request $request)
    {
        $counter = $request->input("counter");
        $counter++;
        return response()->view("applicant.partials.application".$thing."Item", compact("counter"));
    }

}
