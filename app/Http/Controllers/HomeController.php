<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\JobApplication;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((\Auth::user()->first_login == 0) && \Auth::user()->role == "applicant") {
            return redirect("/first_login");
        }
        $job_application = JobApplication::where("user_id", \Auth::user()->id)->first();
        if(\Auth::user()->role == "recruiter") {
            $searchName = \Session::get("searchName");
            $searchEmail = \Session::get("searchEmail");
            $recruiter_id = \Session::get("recruiter_id");
            $application = \Session::get("application");

            $users = User::where(function ($query) {
                if(\Session::get("searchName")) {
                    $query->where("name", "like", "%".\Session::get("searchName")."%");
                }
                if(\Session::get("searchEmail")) {
                    $query->where("email", "like", "%".\Session::get("searchEmail")."%");
                }
                if(\Session::get("recruiter_id") && \Session::get("recruiter_id") != 0) {
                    $query->where("user_id", \Session::get("recruiter_id"));
                }
                if(\Auth::user()->role == "recruiter"){
                    $query->where("user_id", \Auth::user()->id);
                }
            })->where("role", "applicant")->paginate(10);
        } else {
            $users = [];
            $searchEmail = "";
            $searchName = "";
            $application = 0;
        }
        return view('home', compact("job_application", "users", "searchName", "searchEmail", "application"));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function firstLogin(Request $request)
    {
        $user = \Auth::user();
        if($user->first_login) {
            return redirect("/home");
        }
        $user_id = $request->input("user_id");
        if($user_id) {
            $check_user = User::find($user_id);
            if($user_id != "NULL" && $check_user && $check_user->role == 'recruiter') {
                $recruiter = User::find($user_id);
                $user->user_id = $user_id;
                \Cache::forget("who_is_my_recruiter_".$user->id);
                \Cache::add("who_is_my_recruiter_".$user->id, "Your recruiter is <strong>".$recruiter->name."</strong>", 30);
            }
            $user->first_login = 1;
            $user->save();
            return redirect("/home");
        }else {
            return response()->view("applicant.first_login_candidate", compact("user"));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addRecruiterFromModal(Request $request)
    {
        $message = "";
        if($request->has("user_id")) {
            $user_id = $request->input("user_id");
            $check_user = User::find($user_id);
            if($user_id != "NULL" && $check_user && $check_user->role == 'recruiter') {
                $user = \Auth::user();
                $user->user_id = $user_id;
                $user->save();
                \Cache::forget("who_is_my_recruiter_".$user->id);
                $message = "Successfully updated!";
            } else {
                return redirect()->back();
            }
        }
        return redirect()->back()->with("message", $message);
    }
}
