<?php

namespace App\Http\Controllers;

use App\JobApplication;
use App\JobApplicationCertificate;
use App\JobApplicationEducation;
use App\JobApplicationEmployment;
use App\JobApplicationEmr;
use App\JobApplicationLicense;
use App\JobApplicationSpeciality;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class JobApplicationController extends Controller
{

    /**
     * @param Request $request
     */
    public function processFirst(Request $request)
    {
        if($request->input("job_application_id") != "0") {
            $application = JobApplication::find($request->input("job_application_id"));
            $data = $request->all();
            if(!array_key_exists("united_states_citizen", $data)) {
                $data['united_states_citizen'] = 0;
            }
            $application->update($data);
        }
    }

    /**
     * @param Request $request
     */
    public function processSecond(Request $request)
    {
        $application = JobApplication::find($request->input("job_application_id"));

        $application->specialities()->delete();
        $application->emrs()->delete();

        $application->total_years_of_exp = $request->input("total_years_of_exp");
        $application->save();

        $this->saveMany($request->input("specialities"), "speciality", $application->id, "JobApplicationSpeciality");
        $this->saveMany($request->input("emrs"), "charting_equipment", $application->id, "JobApplicationEmr");

    }

    /**
     * @param Request $request
     */
    public function processThird(Request $request)
    {
        $application = JobApplication::find($request->input("job_application_id"));
        $application->educations()->delete();

        if(!array_key_exists("not_rn", $request->all())) {
            $application->not_rn = 0;
        } else {
            $application->not_rn = 1;
        }
        $application->edu_pass_board_year = $request->input("edu_pass_board_year");
        $application->save();
        $this->saveMany($request->input("educations"), "place", $application->id, "JobApplicationEducation");

    }

    /**
     * @param Request $request
     */
    public function processFourth(Request $request)
    {
        $application = JobApplication::find($request->input("job_application_id"));
        $data = $request->all();
        $application->update($data);
    }

    /**
     * @param Request $request
     */
    public function processFifth(Request $request)
    {
        $application = JobApplication::find($request->input("job_application_id"));
        $application->licensies()->delete();

        $application->date_pass_exams = $request->input("date_pass_exams");
        $application->save();
        $this->saveMany($request->input("licensies"), "state", $application->id, "JobApplicationLicense");
    }

    /**
     * @param Request $request
     */
    public function processSixth(Request $request)
    {
        $application = JobApplication::find($request->input("job_application_id"));
        $application->certificates()->delete();

        $this->saveMany($request->input("certificates"), "type", $application->id, "JobApplicationCertificate");
    }

    /**
     * @param Request $request
     */
    public function processSeventh(Request $request)
    {
        $application = JobApplication::find($request->input("job_application_id"));
        $application->employments()->delete();

        $this->saveMany($request->input("employments"), "facility", $application->id, "JobApplicationEmployment");
    }

    /**
     * @param Request $request
     */
    public function processAteth(Request $request)
    {
        $application = JobApplication::find($request->input("job_application_id"));
        $data = $request->all();

        $application->update($data);
    }

    public function processNinth(Request $request)
    {
        $application = JobApplication::find($request->input("job_application_id"));
        $data = $request->all();

        $application->update($data);

        if(ENV("EMAIL", true)){
            if(\Auth::user()->user_id) {
                $recruiter = User::find(\Auth::user()->user_id);
                if($recruiter) {
                    \Mail::send('admin.emails.newSubmitApplication', ['app' => $application, "user" => \Auth::user()], function ($m) use ($recruiter) {
                        $m->from('noreply@verornportal.com', 'Vero RN Portal');
                        $m->to($recruiter->email, $recruiter->name)->subject('New Job Application Submitted');
                    });
                }
            }
        }

        if(ENV("EMAIL", true)) {
            if(!$request->has("redirect")) {
                foreach(User::whereRole("administrator")->get() as $user) {
                    \Mail::send('admin.emails.newSubmitApplication', ['app' => $application, "user" => \Auth::user()], function ($m) use ($user) {
                        $m->from('noreply@verornportal.com', 'Vero RN Portal');
                        $m->to($user->email, $user->name)->subject('New Job Application Submitted');
                    });
                }
            }
        }

        $redirect_path = "/home";
        if($request->has("redirect")) {
            $redirect_path = $request->input("redirect");
        }
        return response()->redirectTo($redirect_path)->with("message", "Updated Successfully!");
    }

    /**
     * @param $data_source
     * @param $field
     * @param $application_id
     * @param $instance
     */
    public function saveMany($data_source, $field, $application_id, $instance)
    {
        if(count($data_source[$field])) {
            $keys = array_keys($data_source[$field]);
            foreach ($keys as $key_1 => $key) {
                $keys_array = array_keys($data_source);
                $data = [];
                foreach ($keys_array as $key_array) {
                    if (array_key_exists($key_array, $data_source)) {
                        $data[$key_array] = $data_source[$key_array][$key];
                    }
                }
                $data["job_application_id"] = $application_id;

                if($instance == "JobApplicationEmployment") {
                    if($data_source["present"][$key] && array_key_exists("present", $data_source)) {
                        $data["present"] = 1;
                    } else {
                        $data['present'] = 0;
                    }
                }

                $model = new \ReflectionClass('\App\\' . $instance);
                $model = $model->newInstance();
                $model::create($data);
            }
        }
    }

    /**
     * @param Request $request
     * @todo refactoring tomorrow
     * @return \Illuminate\Http\RedirectResponse
     */
    public function process(Request $request)
    {
        if($request->input("job_application_id") != "0") {
            $application = JobApplication::find($request->input("job_application_id"));
            $data = $request->all();
            if(!array_key_exists("not_rn", $data)) {
                $data['not_rn'] = 0;
            }
            $application->update($data);
            $message = "You have successfully update the application!";
            $application->certificates()->delete();
            $application->specialities()->delete();
            $application->licensies()->delete();
            $application->educations()->delete();
            $application->employments()->delete();
            $application->emrs()->delete();
        } else {
            $application = JobApplication::create($request->all());
            $message = "You have successfully left the application!";
        }

        $specialities = $request->input("specialities");
        if(count($specialities['speciality'])) {
            $keys = array_keys($specialities['speciality']);
            foreach($keys as $key) {
                JobApplicationSpeciality::create([
                    "speciality" => $specialities['speciality'][$key],
                    "years" => $specialities['years'][$key],
                    "as_of" => $specialities['as_of'][$key],
                    "job_application_id" => $application->id,
                ]);
            }
        }

        $emrs = $request->input("emrs");
        if(count($emrs['charting_equipment'])) {
            $keys = array_keys($emrs['charting_equipment']);
            foreach($keys as $key) {
                JobApplicationEmr::create([
                    "charting_equipment" => $emrs['charting_equipment'][$key],
                    "years" => $emrs['years'][$key],
                    "job_application_id" => $application->id,
                ]);
            }
        }


        $licensies = $request->input("licensies");
        if(count($licensies['state'])) {
            $keys = array_keys($licensies['state']);
            foreach($keys as $key) {
                JobApplicationLicense::create([
                    "state" => $licensies['state'][$key],
                    "number" => $licensies['number'][$key],
                    "expiration_date" => $licensies['expiration_date'][$key],
                    "is_active" => $licensies['is_active'][$key],
                    "job_application_id" => $application->id,
                ]);
            }
        }

        $certificates = $request->input("certificates");
        if(count($certificates['type'])) {
            $keys = array_keys($certificates['type']);
            foreach($keys as $key) {
                JobApplicationCertificate::create([
                    "type" => $certificates['type'][$key],
                    "number" => $certificates['number'][$key],
                    "date" => $certificates['date'][$key],
                    "expiration_date" => $certificates['expiration_date'][$key],
                    "job_application_id" => $application->id,
                ]);
            }
        }

        $educations = $request->input("educations");
        if(count($educations['place'])) {
            $keys = array_keys($educations['place']);
            foreach($keys as $key) {
                JobApplicationEducation::create([
                    "place" => $educations['place'][$key],
                    "city_state" => $educations['city_state'][$key],
                    "state" => $educations['state'][$key],
                    "month_year_graduated" => $educations['month_year_graduated'][$key],
                    "type_of_degree" => $educations['type_of_degree'][$key],
                    "job_application_id" => $application->id,
                ]);
            }
        }

        $employments = $request->input("employments");
        if(count($employments['facility'])) {
            $keys = array_keys($employments['facility']);
            foreach($keys as $key_1 => $key) {
                $keys_array = array_keys($employments);
                $data = [];
                foreach($keys_array as $key_array) {
                    if(array_key_exists($key_array, $employments)) {
                        $data[$key_array] = $employments[$key_array][$key];
                    }
                }
                $data["job_application_id"] = $application->id;
                if($employments["present"][$key] && array_key_exists("present", $employments)) {
                    $data["present"] = 1;
                } else {
                    $data['present'] = 0;
                }
                JobApplicationEmployment::create($data);
            }
        }

        if(ENV("EMAIL", true)){
            if(\Auth::user()->user_id) {
                $recruiter = User::find(\Auth::user()->user_id);
                if($recruiter) {
                    \Mail::send('admin.emails.newSubmitApplication', ['app' => $application, "user" => \Auth::user()], function ($m) use ($recruiter) {
                        $m->from('noreply@verornportal.com', 'Vero RN Portal');
                        $m->to($recruiter->email, $recruiter->name)->subject('New Job Application Submitted');
                    });
                }
            }
        }

        if(ENV("EMAIL", true)) {
            if(!$request->has("redirect")) {
                foreach(User::whereRole("administrator")->get() as $user) {
                    \Mail::send('admin.emails.newSubmitApplication', ['app' => $application, "user" => \Auth::user()], function ($m) use ($user) {
                        $m->from('noreply@verornportal.com', 'Vero RN Portal');
                        $m->to($user->email, $user->name)->subject('New Job Application Submitted');
                    });
                }
            }
        }
        $redirect_path = "/home";
        if($request->has("redirect")) {
            $redirect_path = $request->input("redirect");
        }
        return response()->redirectTo($redirect_path)->with("message", "Updated Successfully!");
    }

    /**
     * @param int $application_id
     * @return \Illuminate\Http\Response
     */
    public function edit($application_id = 0)
    {
        $application = JobApplication::find($application_id);
        return response()->view("applicant.application", compact("application"));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function show()
    {
        $application = JobApplication::where("user_id", \Auth::user()->id)->first();
        if($application) {
            return redirect("/applicant/application/edit/".$application->id);
        } else {
            $application = JobApplication::create([
                "user_id" => \Auth::user()->id
            ]);
            return redirect("/applicant/application/edit/".$application->id);
        }
    }
}
