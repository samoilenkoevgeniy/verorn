<?php

namespace App\Http\Controllers;

use App\FormProcess;
use App\FormProcessCertificate;
use App\FormProcessExperience;
use App\FormProcessQuestion;
use App\JobApplicationForm;
use App\JobApplicationFormQuestionBlockItem;
use App\User;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FormProcessController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forms = JobApplicationForm::all();
        return response()->view("applicant.forms", compact("forms"));
    }

    /**
     * @param Request $request
     * @param $form_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function process(Request $request, $form_id)
    {
        $form = JobApplicationForm::find($form_id);
        $data = $request->all();
        $data['form_id'] = $form_id;
        $formProcess = FormProcess::whereUserId($request->input("user_id"))->whereFormId($form_id)->first();
        $data['app_care_res'] = '';
        if(array_key_exists("app_care", $data)) {
            foreach($data['app_care'] as $item) {
                if($data['app_care_res'] == "") {
                    $data['app_care_res'] .= $item;
                } else{
                    $data['app_care_res'] .= ",".$item;
                }
            }
        }
        $data['app_care'] = $data['app_care_res'];
        if(!$formProcess) {
            $formProcess = FormProcess::create($data);
        } else {
            $formProcess->update($data);
            $formProcess->formProcessQuestions()->delete();
            $formProcess->formProcessCertificates()->delete();
            $formProcess->formProcessExperiences()->delete();
        }

        foreach(JobApplicationFormQuestionBlockItem::all() as $item) {
            if($request->has("question_".$item->id)) {
                FormProcessQuestion::create([
                    "q_id" => $item->id,
                    "value" => $request->input("question_".$item->id),
                    "form_process_id" => $formProcess->id
                ]);
            }
        }

        if($request->input("certificate")) {
            foreach($request->input("certificate") as $cert) {
                if($cert['name']) {
                    FormProcessCertificate::create([
                        "date" => $cert['date'],
                        "cert_id" => $cert['cert_id'],
                        "value" => array_key_exists("text", $cert) ? $cert['text'] : "",
                        "form_process_id" => $formProcess->id
                    ]);
                }
            }
        }

        if($request->input("experience")) {
            foreach($request->input("experience") as $exp) {
                if($exp['name']) {
                    FormProcessExperience::create([
                        "exp_id" => $exp['exp_id'],
                        "years" => $exp['years'],
                        "form_process_id" => $formProcess->id
                    ]);
                }
            }
        }

        if(\Auth::user()->user_id) {
            $recruiter = User::find(\Auth::user()->user_id);
            if(ENV("EMAIL", true)){
                if($recruiter) {
                    \Mail::send('admin.emails.newSubmitForm', ['form' => $formProcess, "user" => Auth::user()], function ($m) use ($recruiter) {
                        $m->from('noreply@verornportal.com', 'Vero RN Portal');
                        $m->to($recruiter->email, $recruiter->name)->subject('New Skills Checklist Submitted');
                    });
                }

            }
        }

        if(!$request->has("redirect")) {
            foreach(User::whereRole("administrator")->get() as $user) {
                \Mail::send('admin.emails.newSubmitForm', ['form' => $formProcess, "user" => Auth::user()], function ($m) use ($user) {
                    $m->from('noreply@verornportal.com', 'Vero RN Portal');
                    $m->to($user->email, $user->name)->subject('New Skills Checklist Submitted');
                });
            }
        }

        $redirect_path = "/home";
        if($request->has("redirect")) {
            $redirect_path = $request->input("redirect");
        }
        return response()->redirectTo($redirect_path)->with("message", "Updated Successfully!");
    }
}
