<?php

namespace App\Http\Controllers;

use App\FormProcess;
use App\JobApplication;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminDashboardController extends Controller
{

    /**
     * @return \Illuminate\Http\Response
     */
    public function Candidates()
    {
        if(\Auth::user()->role == "applicant") {
            return redirect("/home");
        }
        $searchName = \Session::get("searchName");
        $searchEmail = \Session::get("searchEmail");
        $recruiter_id = \Session::get("recruiter_id");
        $application = \Session::get("application");
        $users = User::where(function ($query) {
            if(\Session::get("searchName")) {
                $query->where("name", "like", "%".\Session::get("searchName")."%");
            }
            if(\Session::get("searchEmail")) {
                $query->where("email", "like", "%".\Session::get("searchEmail")."%");
            }
            if(\Session::get("recruiter_id") && \Session::get("recruiter_id") != 0 ) {
                $query->where("user_id", \Session::get("recruiter_id"));
            }elseif(\Session::get("recruiter_id") == "no_recruiter"){
                $query->where("user_id", NULL);
            }
            if(\Auth::user()->role == "recruiter"){
                $query->where("user_id", \Auth::user()->id);
            }
        })->where("role", "applicant")->paginate(10);
        if($application) {
            foreach($users as $key=>$user) {
                $check = JobApplication::where("user_id", $user->id)->first();
                if(!$check) {
                    unset($users[$key]);
                }
            }
        }
        return response()->view("admin.dashboard", compact("users", "searchName", "searchEmail", "recruiter_id", "application"));
    }

    /**
     * @param int $user_id
     * @return \Illuminate\Http\Response
     */
    public function candidate($user_id = 0)
    {
        if(\Auth::user()->role == "applicant") {
            return redirect("/home");
        }
        $user = User::find($user_id);
        $jobApplication = JobApplication::whereUserId($user_id)->first();
        $forms = FormProcess::whereUserId($user_id)->get();
        return response()->view("admin.candidate", compact("user", "jobApplication", "forms"));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function search(Request $request)
    {
        foreach($request->all() as $key => $filter) {
            \Session::forget($key);
            \Session::put($key, $filter);
        }

        if(!$request->has("application")) {
            \Session::forget("application");
        }

        if($request->input("redirect")) {
            return response()->redirectTo($request->input("redirect"));
        } else {
            return redirect()->back();
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeRecruiter(Request $request)
    {
        $user = User::find($request->input("user_id"));
        if($request->input("recruiter_id") != 0) {
            $user->user_id = $request->input("recruiter_id");
        } else {
            $user->user_id = NULL;
        }
        $user->save();
        if($request->input("recruiter_id") != 0) {
            $recruiter = User::find($request->input("recruiter_id"))->name;
        } else {
            $recruiter = "No Recruiter";
        }
        \Cache::forget("who_is_my_recruiter_".$user->id);
        return $recruiter;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function Recruiters()
    {
        $users = User::where("role", "recruiter")->get();
        return response()->view("admin.recruiters", compact("users"));
    }

}
