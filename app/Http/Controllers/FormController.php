<?php

namespace App\Http\Controllers;

use App\FormProcess;
use App\Http\Requests\AddJobApplicationForm;
use App\JobApplication;
use App\JobApplicationForm;
use App\JobApplicationFormCertificate;
use App\JobApplicationFormExperience;
use App\JobApplicationFormQuestionBlock;
use App\JobApplicationFormQuestionBlockItem;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Symfony\Component\DomCrawler\Crawler;

class FormController extends Controller
{

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forms = JobApplicationForm::all();
        return response()->view("admin.forms.index", compact("forms"));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = 0)
    {
        $form = JobApplicationForm::find($id);
        return response()->view("admin.forms.edit", compact("form"));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $form = JobApplicationForm::find($id);
        $form->delete();
        return redirect()->back();
    }

    /**
     * @param AddJobApplicationForm $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AddJobApplicationForm $request, $id = 0)
    {
        if($id == 0) {
            JobApplicationForm::create($request->all());
            $message = "Form created successful!";
            return redirect()->back()->with("message", $message);
        } else {
            $form = JobApplicationForm::findOrNew($id);
            $data = $request->all();
            $checkboxes = [
                "age_specific_block", "certification_block",
                "primary_experience_block"
            ];
            foreach($checkboxes as $checkbox) {
                if(!isset($data[$checkbox])) {
                    $data[$checkbox] = 0;
                }
            }
            $form->update($data);
            $message = "Form updated successful!";
            return response()->redirectTo("/admin/forms")->with("message", $message);
        }

    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function addQuestionBlock($id = 0)
    {
        $questionBlock = JobApplicationFormQuestionBlock::create([
            "form_id" => $id
        ]);
        return response()->view("admin.forms.partials.questionBlockItem", compact("questionBlock"));
    }

    /**
     * @param Request $request
     * @param int $block_id
     * @return array
     */
    public function updateQuestionBlockRow(Request $request,$block_id = 0)
    {
        $questionBlock = JobApplicationFormQuestionBlock::find($block_id);
        $field = $request->input("field");
        $value = $request->input("value");
        $questionBlock->$field = $value;
        $questionBlock->save();
        return [];
    }

    /**
     * @param int $block_id
     * @return array
     */
    public function removeQuestionBlock($block_id = 0)
    {
        $questionBlock = JobApplicationFormQuestionBlock::find($block_id);
        $questionBlock->delete();
        return [];
    }

    /**
     * @param Request $request
     * @param int $question_block_id
     * @return \Illuminate\Http\Response
     */
    public function addQuestionBlockItem(Request $request, $question_block_id = 0)
    {
        $data = [
            "block_id" => $question_block_id,
            "parent_id" => $request->input("parent_id")
        ];
        if($data['parent_id'] == "NULL") {
            unset($data['parent_id']);
        }
        $question = JobApplicationFormQuestionBlockItem::create($data);
        return response()->view("admin.forms.partials.questionBlockItemQuestion", compact("question"));
    }

    /**
     * @param Request $request
     * @param int $block_id
     * @return array
     */
    public function updateQuestionBlockItemRow(Request $request,$block_id = 0)
    {
        $question = JobApplicationFormQuestionBlockItem::find($block_id);
        $field = $request->input("field");
        $value = $request->input("value");
        $question->$field = $value;
        $question->save();
        return [];
    }

    /**
     * @param int $question_block_id
     * @return array
     */
    public function removeQuestionBlockItem($question_block_id = 0)
    {
        $question = JobApplicationFormQuestionBlockItem::find($question_block_id);
        $question->delete();
        return [];
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showCandidate($id = 0)
    {
        $form = JobApplicationForm::find($id);
        $application = JobApplication::whereUserId(\Auth::user()->id)->first();
        $formProcess = FormProcess::whereUserId(\Auth::user()->id)->whereFormId($form->id)->first();
        $questionBlocks = $form->questionBlocks()->with("items")->get();
        if($formProcess) {
            $appCare = explode(",", $formProcess->app_care);
        } else {
            $appCare = [];
        }
        return response()->view("applicant.form", compact("form", "questionBlocks", "application", "formProcess", "appCare"));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function addCertificate($id)
    {
        $certificate = JobApplicationFormCertificate::create([
            "form_id" => $id
        ]);
        return response()->view("admin.forms.partials.certificateItem", compact("certificate"));
    }

    /**
     * @param Request $request
     * @param int $certificate_id
     * @return array
     */
    public function updateCertificateRow(Request $request, $certificate_id = 0)
    {
        $certificate = JobApplicationFormCertificate::find($certificate_id);
        $field = $request->input("field");
        $value = $request->input("value");
        $certificate->$field = $value;
        $certificate->save();
        return [];
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function addExperience($id)
    {
        $experience = JobApplicationFormExperience::create([
            "form_id" => $id
        ]);
        return response()->view("admin.forms.partials.experienceItem", compact("experience"));
    }

    /**
     * @param Request $request
     * @param int $experience_id
     * @return array
     */
    public function updateExperienceRow(Request $request, $experience_id = 0)
    {
        $experience = JobApplicationFormExperience::find($experience_id);
        $field = $request->input("field");
        $value = $request->input("value");
        $experience->$field = $value;
        $experience->save();
        return [];
    }

    public function parser(Request $request)
    {
        $crawler = new Crawler();

        $client = new Client();
        $res = $client->request('GET', $request->input("url") , []);

        $crawler->addContent((string)$res->getBody());
        $parents = $crawler->filter(".checklist")->each(function (Crawler $node, $i) {
            return $node;
        });

        $form = JobApplicationForm::create([
            "name" => $request->input("name")
        ]);

        $result = [];
        $counter = 0;
        $prev_class = "";
        $last_header_counter = 0;
        $last_first_counter = 0;
        $last_second_counter = 0;
        $last_third_counter = 0;

        $last_header_block = 0;
        $last_first_block = 0;
        $last_second_block = 0;
        $last_third_block = 0;

        $left = 1;
        foreach($parents as $parent) {
            $parents_internal = $parent->filter("tr")->each(function (Crawler $node, $i) {
                return $node;
            });;

            foreach($parents_internal as $parent_internal) {
                $current_item = $parent_internal->filter("td")->first();
                if(!$current_item->text() || $current_item->text() == "&nbsp;" || $current_item->text() == "Back to top") {
                    continue;
                }

                $classes = $current_item->attr("class");
                $first_class = explode(" ", $classes)[0];

                if($first_class != "checklist-section-header") {
                    if($first_class != "description-cell-1") {
                        if($first_class != "description-cell-2") {
                            if($first_class != "description-cell-3") {
                                $result[$last_header_counter]["items"][$last_first_counter]["items"][$last_second_counter]["items"][$last_third_counter]['items'][] = [
                                    "name" => $current_item->text(),
                                    "class" => $first_class,
                                    "l_f_c" => $last_first_counter,
                                    "items" => []
                                ];
                                if($last_header_block) {
                                    JobApplicationFormQuestionBlockItem::create([
                                        "text" => $current_item->text(),
                                        "block_id" => $last_header_block,
                                        "parent_id" => $last_third_block ? $last_third_block : NULL
                                    ]);
                                }
                            } else {
                                $result[$last_header_counter]["items"][$last_first_counter]["items"][$last_second_counter]["items"][$counter] = [
                                    "name" => $current_item->text(),
                                    "class" => $first_class,
                                    "l_f_c" => $last_first_counter,
                                    "items" => []
                                ];
                                $last_third_counter = $counter;
                                $last_third_block= \App\JobApplicationFormQuestionBlockItem::create([
                                    "text" => $current_item->text(),
                                    "block_id" => $last_header_block ? $last_header_block : NULL,
                                    "parent_id" => $last_second_block ? $last_second_block : NULL
                                ]);
                                $last_third_block = $last_third_block->id;
                            }
                        } else {
                            $result[$last_header_counter]["items"][$last_first_counter]["items"][$counter] = [
                                "name" => $current_item->text(),
                                "class" => $first_class,
                                "l_f_c" => $last_first_counter,
                                "items" => []
                            ];
                            $last_second_counter = $counter;
                            $last_second_block= \App\JobApplicationFormQuestionBlockItem::create([
                                "text" => $current_item->text(),
                                "block_id" => $last_header_block,
                                "parent_id" => $last_first_block ? $last_first_block : NULL
                            ]);
                            $last_second_block = $last_second_block->id;
                        }
                    } else {
                        $result[$last_header_counter]["items"][$counter] = [
                            "name" => $current_item->text(),
                            "class" => $first_class,
                            "items" => []
                        ];
                        $last_first_counter = $counter;
                        $last_first_block= \App\JobApplicationFormQuestionBlockItem::create([
                            "text" => $current_item->text(),
                            "block_id" => $last_header_block,
                            "parent_id" => NULL
                        ]);
                        $last_first_block = $last_first_block->id;
                    }
                } else {
                    $result[$counter] = [
                        "name" => $current_item->text(),
                        "left" => $left,
                        "items" => []
                    ];
                    $last_header_counter = $counter;
                    $last_header_block = \App\JobApplicationFormQuestionBlock::create([
                        "name" => $current_item->text(),
                        "form_id" => $form->id,
                        "left" => $left
                    ]);
                    $last_header_block = $last_header_block->id;
                }

                $counter++;
            }
            $left = 0;
        }

        $parents = $crawler->filter(".table-td")->each(function (Crawler $node, $i) {
            return $node;
        });
        foreach($parents as $parent) {
            if($parent->parents()->first()->parents()->first()->attr("class") != "table-age-groups") {
                if($parent->text() != "Other (specify)") {
                    JobApplicationFormExperience::create([
                        "name" => $parent->text(),
                        "form_id" => $form->id
                    ]);
                }
            }
        }

        return redirect()->back()->with("message", "Success!");
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function parserInterface()
    {
        return response()->view("admin.parser");
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeExpBlock($id)
    {
        $exp = JobApplicationFormExperience::find($id);
        $exp->delete();
        return redirect()->back();
    }
}
