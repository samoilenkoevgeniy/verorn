<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

    public function showPage($page = "")
    {
        return redirect("/home");
        if(!$page) {
            $page = "index";
        }
        return response()->view("pages.".$page);
    }

}
