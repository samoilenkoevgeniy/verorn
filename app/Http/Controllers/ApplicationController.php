<?php

namespace App\Http\Controllers;

use App;
use App\FormProcess;
use App\JobApplication;
//use Barryvdh\DomPDF\PDF;
use App\JobApplicationForm;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

//use Knp\Snappy\Pdf;

class ApplicationController extends Controller
{

    /**
     * @param $application
     * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function checkPermission($application)
    {
        if(\Auth::user()->role != "administrator") {
            if(\Auth::user()->role == "applicant") {
                return redirect("/home");
            }
            $applicant = $application->user;
            if($applicant->user_id != \Auth::user()->id) {
                return redirect("/home");
            }
            return true;
        }
        return true;
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = 0)
    {
        $user = \Auth::user();
        $jobApplication = JobApplication::find($id);
        if($user->role == "administrator") {}
        elseif($user->role == "recruiter") {
            $this->checkPermission($jobApplication);
        } else {
            return redirect("/home");
        }
        return response()->view("applications.show", [
            "print" => 1,
            "application" => $jobApplication
        ]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function formShow($id = 0)
    {
        $formProcess = FormProcess::find($id);
        $form = JobApplicationForm::find($formProcess->form_id);
        $application = JobApplication::whereUserId($formProcess->user_id)->first();
        $this->checkPermission($application);
        $questionBlocks = $form->questionBlocks()->with("items")->get();
        $appCare = explode(",", $formProcess->app_care);
        return response()->view("applications.show_form", compact("form", "questionBlocks", "application", "formProcess", "appCare"));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = 0)
    {
        $application = JobApplication::find($id);
        $this->checkPermission($application);
        return response()->view("applicant.application", compact("application"));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function printApp($id = 0)
    {
        //require_once( __DIR__ . '/../../Services/mpdf/mpdf.php');
        //dd($_SERVER['DOCUMENT_ROOT'].'/images/VeroRNLogoFinal.png');
        $application = JobApplication::find($id);
        $image = $img = Image::canvas(800, 65);
        $image->text($application->name_signature, 0, 35, function($font) {
            $font->file(public_path("css/8582.ttf"));
            $font->size(44);
            $font->color('#000000');
        });
        $image->save(public_path("images/user_names/".$application->id.".jpg"));
        //return "<img src='/images/user_names/1.jpg'>";
        $this->checkPermission($application);
        $mpdf = new \mPDF('c','A4','','5' , 5 , 5 , 25 , 15 , 0 , 0);

        //$mpdf->showImageErrors = true;
        $mpdf->SetHTMLHeader('<div style="text-align: right; padding-top: 10px;;padding-right: 20px;"><img src="'.$_SERVER['DOCUMENT_ROOT'].'/images/VeroRNLogoFinal.png" /></div>', 'O');
        $mpdf->SetHTMLFooter('<div style="border-top: 2px solid #888888;text-align: center; font-weight: bold;font-size: 11px; ">4010 Executive Park Drive Suite 102 Cincinnati &#9733; OH  45241 &#9733; Phone: 513.554.2301 &#9733; Fax: 513.672.0568 &#9733; www.VeroRN.com</div>');
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
        $html = view("applications.print", compact("application"))->render();
        $mpdf->WriteHTML($html);
        $orderPdfName = "Application-".$application->id;
        //header('Content-type: application/pdf');
        //header("Content-Disposition: attachment; filename=application.pdf");
        $mpdf->Output("application.pdf",'I');

    }

    /**
     * @param int $id
     * @throws \Exception
     * @throws \Throwable
     */
    public function printAppForm($id = 0)
    {
        $formProcess = FormProcess::find($id);
        $this->checkPermission($formProcess);
        $form = $formProcess->sourceForm;
        $image = $img = Image::canvas(800, 65);
        $image->text($formProcess->first_name. " ".$formProcess->last_name, 0, 35, function($font) {
            $font->file(public_path("css/8582.ttf"));
            $font->size(44);
            $font->color('#000000');
        });
        $image->save(public_path("images/user_names/".$formProcess->id.".jpg"));
        $questionBlocks = $form->questionBlocks()->with("items")->get();
        $appCare = explode(",", $formProcess->app_care);
        //dd($questionBlocks);
        $mpdf=new \mPDF('c','A4','','14' , 5 , 5 , 25 , 15 , 0 , 0);
        //$mpdf->useOddEven = true;
        $mpdf->SetHTMLHeader('<div style="text-align: right; padding-top: 10px;;padding-right: 20px;"><img src="'.$_SERVER['DOCUMENT_ROOT'].'/images/VeroRNLogoFinal.png" /></div>', 'O');
        $mpdf->SetHTMLFooter('<div style="border-top: 2px solid #888888;text-align: center; font-weight: bold;font-size: 11px; ">4010 Executive Park Drive Suite 102 Cincinnati &#9733; OH  45241 &#9733; Phone: 513.554.2301 &#9733; Fax: 513.672.0568 &#9733; www.VeroRN.com</div>');
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
        $html = view("applications.print_forms", compact("formProcess", "form", "questionBlocks", "appCare"))->render();
        $mpdf->WriteHTML($html);
        $orderPdfName = "Application-".$formProcess->id;
        header('Content-type: application/pdf');
        header("Content-Disposition: attachment; filename=application.pdf");
        $mpdf->Output("application.pdf",'I');
    }
}
