<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationEducation extends Model
{
    protected $fillable = [
        "place", "city_state", "month_year_graduated", "type_of_degree", "job_application_id", "state"
    ];

    /**
     * @param $value
     */
    public function setMonthYearGraduatedAttribute($value)
    {
        try {
            $pieces = explode("/", $value);
            $this->attributes['month_year_graduated'] = $pieces[2]."-".$pieces[0]."-".$pieces[1];
        }catch (\Exception $e) {
            $this->attributes['month_year_graduated'] = "00-00-0000";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getMonthYearGraduatedAttribute($value)
    {
        try {
            $pieces = explode("-", $value);
            return $pieces[1]."/".$pieces[2]."/".$pieces[0];
        } catch(\Exception $e) {
            return "00/00/0000";
        }
    }
}
