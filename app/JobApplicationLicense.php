<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationLicense extends Model
{
    protected $fillable = [
         "state","number","expiration_date","is_active", "job_application_id"
    ];

    /**
     * @param $value
     */
    public function setExpirationDateAttribute($value)
    {
        $pieces = explode("/", $value);
        try {
            $this->attributes['expiration_date'] = $pieces[2]."-".$pieces[0]."-".$pieces[1];
        }catch (\Exception $e) {
            $this->attributes['expiration_date'] = "00-00-0000";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getExpirationDateAttribute($value)
    {
        $pieces = explode("-", $value);
        try {
            return $pieces[1]."/".$pieces[2]."/".$pieces[0];
        }catch (\Exception $e) {
            return "00/00/0000";
        }
    }
}
