<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationSpeciality extends Model
{
    protected $fillable = [
        "speciality", "years", "as_of", "job_application_id"
    ];

}
