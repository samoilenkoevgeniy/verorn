<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormProcessQuestion extends Model
{
    protected $fillable = [
        "q_id", "form_process_id", "value"
    ];
}
