<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationEmployment extends Model
{
    protected $fillable = [
        "facility", "city_state",
        "date_start", "date_end",
        "present", "supervisor",
        "phone", "unit", "travel",
        "fulltime", "charge_experience",
        "reason_for_leaving", "may_contact",
        "job_application_id", "clinical_reference",
        "reference_phone_number", "contact_reference",
        "number_of_beds_facility", "facility_state", "no_of_beds_unit",
        "supervisors_title", "avg_pat_ratio_unit", "position_held", "unit_description",
        "reason_of_leaving", "days_description", "employment_type", "agency_name", "facility_type"
    ];

    /**
     * @param $value
     */
    public function setDateStartAttribute($value)
    {
        try{
            $pieces = explode("/", $value);
            $this->attributes['date_start'] = $pieces[2]."-".$pieces[0]."-".$pieces[1];
        }catch (\Exception $e) {
            $this->attributes['date_start'] = "0000-00-00";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getDateStartAttribute($value)
    {
        try {
            $pieces = explode("-", $value);
            return $pieces[1]."/".$pieces[2]."/".$pieces[0];
        } catch (\Exception $e) {
            return "00\00\0000";
        }
    }

    /**
     * @param $value
     */
    public function setDateEndAttribute($value)
    {
        try {
            $pieces = explode("/", $value);
            $this->attributes['date_end'] = $pieces[2]."-".$pieces[0]."-".$pieces[1];
        } catch (\Exception $e) {
            $this->attributes['date_end'] = "0000-00-00";
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getDateEndAttribute($value)
    {
        try {
            $pieces = explode("-", $value);
            return $pieces[1]."/".$pieces[2]."/".$pieces[0];
        } catch (\Exception $e) {
            return "00\00\0000";
        }
    }
}
