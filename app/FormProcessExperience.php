<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormProcessExperience extends Model
{
    protected $fillable = [
        "exp_id", "form_process_id", "years"
    ];
}
