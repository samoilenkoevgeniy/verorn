<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicationEmr extends Model
{
    protected $fillable = [
        "charting_equipment", "years", "other", "job_application_id"
    ];
}
