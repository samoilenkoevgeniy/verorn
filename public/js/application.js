$(window).ready(function(){

    $("input").each(function(){
        $(this).attr("disabled", "disabled");
    });

    $(document).on("click", ".add_yet", function(){

        var that = $(this),
            thing = that.data("thing"),
            result_container = that.data("result-container"),
            counter_element = $("#count_"+result_container),
            val = counter_element.val();
        if(counter_element.val() === "-1") {
            counter_element.val(0);
        } else {
            val = counter_element.val();
        }
        $.ajax({
            url: "/getTemplate/"+thing,
            data: {
                "counter" : val
            },
            success: function(data){
                $("#"+result_container).append(data);
                maskedInit();
                counter_element.val(parseInt(counter_element.val())+1);
            }
        });
        return false;
    });

    $(document).on("click", ".edit_mode", function(){
        $("input").each(function(){
            $(this).removeAttr("disabled");
        });
        $(".add_yet").show();
        return false;
    });

    $("#Declined").click(function(){
        var that = $(this),
            form = $("#EOEForm");
        if(that.prop("checked")){
            form.hide();
        } else {
            form.show();
        }
    });

    $(document).on("click", ".yes_no input", function(){
        var that = $(this),
            value = that.val(),
            container = that.parents(".row").first();
        if(value === "1") {
            container.find(".description_thing").show();
        } else {
            container.find(".description_thing").hide();
        }
    });

    $(document).on("click", ".btn_remove", function(){
        var that = $(this),
            container = that.parents(".row").first(),
            counter = that.parents(".panel-body").find("input[type=hidden]"),
            count_items = counter.val();
        counter.val(parseInt(count_items)-1);
        container.remove();
        return false;
    });

    $(document).on("click", ".btn_remove_panel", function(){
        var that = $(this),
            container = that.parents(".panel").first();
        container.remove();
        return false;
    });

    maskedInit();



});

function maskedInit() {

    $('.date').combodate({
        format : "MM/DD/YYYY",
        template: "MM/DD/YYYY",
        "customClass": "form-control customFormControl",
        yearDescending : false,
        smartDays: true,
        firstItem: "name"
    });

    $(".date_year").mask("9999");
    $(".zip_code").mask("99999");
    $(".phone").mask("(999) 999-9999");
    $(".social_security").mask("999-99-9999");
}
