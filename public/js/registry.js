$(window).ready(function(){
    $("#country").change(function(){
        var that = $(this),
            value = that.val();
        if(value == "US") {
            $("#state_id_canada").attr("disabled", "disabled").hide();
            $("#cap_select").hide();
            $("#state_id").removeAttr("disabled").show();
        } else if(value == "Canada") {
            $("#cap_select").hide();
            $("#state_id_canada").removeAttr("disabled").show();
            $("#state_id").attr("disabled", "disabled").hide();
        } else {
            $("#cap_select").show();
            $("#state_id_canada").attr("disabled", "disabled").hide();
            $("#state_id").attr("disabled", "disabled").hide();
        }
    });

    $("#profession_id").change(function(){
        var that = $(this),
            value = that.val(),
            speciality_container = $("#specialty_id");
        $.ajax({
            url: "/getSpecialities/"+value,
            dataType: "json",
            success: function(data) {
                speciality_container.html("");
                for(var key in data) {
                    speciality_container.append("<option value='"+data[key].id+"'>"+data[key].text+"</option>")
                }
                for(var i = 0; i != data.length; i++) {
                }
            }
        });
    });

    $("#register_form").submit(function(){
        var result = 1;
        $(".contact_us_form_inner_main_upper input, .contact_us_form_inner_main_upper select").each(function(index, item){
            var current_element = $(item),
                value = current_element.val(),
                container = current_element.parent();

            if(value === "" || value === "0") {
                var ok = 0;
                if(current_element.attr("name") === "years") {
                    ok = 1;
                } else {
                    current_element.siblings("select").each(function(index, item){
                        if($(item).val() != "0") {
                            ok = 1;
                        }
                    });
                }
                if(!ok) {
                    container.addClass("error");
                    result = 0;
                }
            } else {
                if(container.hasClass("error")) {
                    container.removeClass("error");
                }
            }
        });
        if(!result) {
            return false;
        }

    })
});