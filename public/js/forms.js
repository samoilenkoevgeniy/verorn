$(window).ready(function(){

    $("#addQuestionBlock").click(function(){
        var that = $(this),
            form_id = that.data("form-id");
        $.ajax({
            url: "/forms/addQuestionBlock/"+form_id,
            success: function(data){
                $("#question_blocks").append(data);
            },
            error: function(){}
        });
    });

    $(document).on("blur", ".ajax_sync", function(){
        var that = $(this),
            field = that.data("field"),
            question_block_id = that.data("question-block-id");
        $.ajax({
            url: "/forms/updateQuestionBlockRow/"+question_block_id,
            data: {
                "field" : field,
                "value" : that.val()
            },
            success: function(data){
                that.parents(".form-group").first().addClass("has-success");
                setTimeout(function(){
                    that.parents(".form-group").first().removeClass("has-success");
                }, 1000);
            },
            error: function(){}
        });
    });

    $(document).on("click", ".ajax_sync_checkbox", function(){
        var that = $(this),
            field = that.data("field"),
            question_block_id = that.data("question-block-id"),
            value = 0;
        if(that.prop("checked")) {
            value = 1;
        }
        $.ajax({
            url: "/forms/updateQuestionBlockRow/"+question_block_id,
            data: {
                "field" : field,
                "value" : value
            },
            success: function(data){
                that.parents(".form-group").first().addClass("has-success");
                setTimeout(function(){
                    that.parents(".form-group").first().removeClass("has-success");
                }, 1000);
            },
            error: function(){}
        });
    });

    $(document).on("click", ".deleteQuestionBlock", function(){
        var that = $(this),
            question_block_id = that.data("question-block-id");
        $.ajax({
            url: "/forms/removeQuestionBlock/"+question_block_id,
            success: function(data){
                that.parents(".panel").first().remove();
            },
            error: function(data){}
        });
    });

    $(document).on("click", ".addQuestionItem", function(){
        var that = $(this),
            question_block_id = that.data("question-block-id"),
            parent_id = that.data("parent-id");
        if(typeof parent_id == "undefined") {
            parent_id = "NULL";
        }
        $.ajax({
            url: "/forms/questionBlock/addQuestionBlockItem/"+question_block_id,
            data: {
                "parent_id" : parent_id
            },
            success: function(data){
                if(parent_id == "NULL") {
                    that.before(data);
                } else {
                    $(".questions_"+parent_id).append(data);
                }
            },
            error: function(){}
        });
    });

    $(document).on("click", ".removeQuestionItem", function(){
        var that = $(this),
            data_question_block_item_id = that.data("question-block-item-id");
        $.ajax({
            url: "/forms/questionBlock/removeQuestionBlockItem/"+data_question_block_item_id,
            success: function(data){
                that.parents(".form-group").first().remove();
            },
            error: function(data){}
        });
    });

    $(document).on("blur", ".ajax_sync_question", function(){
        var that = $(this),
            field = that.data("field"),
            question_block_item_id = that.data("question-block-item-id");
        $.ajax({
            url: "/forms/questionBlock/updateQuestionBlockItemRow/"+question_block_item_id,
            data: {
                "field" : field,
                "value" : that.val()
            },
            success: function(data){
                that.parents(".form-group").first().addClass("has-success");
                setTimeout(function(){
                    that.parents(".form-group").first().removeClass("has-success");
                }, 1000);
            },
            error: function(){}
        });
    });

    $(document).on("click", "#addCertificateBlock", function(){
        var that = $(this),
            form_id = that.data("form-id");

        $.ajax({
            url: "/forms/addCertificate/"+form_id,
            success: function(data){
                that.before(data);
            },
            error: function(){}
        });
    });

    $(document).on("blur", ".ajax_sync_cert", function(){
        var that = $(this),
            field = that.data("field"),
            certificate_id = that.data("certificate-id");
        $.ajax({
            url: "/forms/updateCertificateRow/"+certificate_id,
            data: {
                "field" : field,
                "value" : that.val()
            },
            success: function(data){
                that.parents(".form-group").first().addClass("has-success");
                setTimeout(function(){
                    that.parents(".form-group").first().removeClass("has-success");
                }, 1000);
            },
            error: function(){}
        });
    });

    $(document).on("click", ".ajax_sync_cert_checkbox", function(){
        var that = $(this),
            field = that.data("field"),
            certificate_id = that.data("certificate-id"),
            value = 0;
        if(that.prop("checked")) {
            value = 1;
        }
        $.ajax({
            url: "/forms/updateCertificateRow/"+certificate_id,
            data: {
                "field" : field,
                "value" : value
            },
            success: function(data){
                that.parents(".form-group").first().addClass("has-success");
                setTimeout(function(){
                    that.parents(".form-group").first().removeClass("has-success");
                }, 1000);
            },
            error: function(){}
        });
    });

    $(document).on("click", "#addExperience", function(){
        var that = $(this),
            form_id = that.data("form-id");
        $.ajax({
            url: "/forms/addExperience/"+form_id,
            success: function(data){
                that.before(data);
            },
            error: function(){}
        });
    });

    $(document).on("blur", ".ajax_sync_exp", function(){
        var that = $(this),
            field = that.data("field"),
            experience_id = that.data("experience-id");
        $.ajax({
            url: "/forms/updateExperienceRow/"+experience_id,
            data: {
                "field" : field,
                "value" : that.val()
            },
            success: function(data){
                that.parents(".form-group").first().addClass("has-success");
                setTimeout(function(){
                    that.parents(".form-group").first().removeClass("has-success");
                }, 1000);
            },
            error: function(){}
        });
    });

    $(document).on("click", ".ajax_sync_question_checkbox", function(){
        var that = $(this),
            field = that.data("field"),
            question_block_item_id = that.data("question-block-item-id"),
            value = 0;
        if(that.prop("checked")) {
            value = 1;
        }
        $.ajax({
            url: "/forms/questionBlock/updateQuestionBlockItemRow/"+question_block_item_id,
            data: {
                "field" : field,
                "value" : value
            },
            success: function(data){
                that.parents(".form-group").first().addClass("has-success");
                setTimeout(function(){
                    that.parents(".form-group").first().removeClass("has-success");
                }, 1000);
            },
            error: function(){}
        });
    });

});