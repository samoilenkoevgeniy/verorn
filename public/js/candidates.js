$(window).ready(function(){
    $(document).on("click", ".setRecruiter",function(){
        var that = $(this),
            that_container = that.parent();
        $.ajax({
            url: "/recruiters/getRecruiters",
            type: "GET",
            data: {"user_id" : that.data("user-id")},
            success: function(data){
                that_container.html(data);
            },
            error: function(data){}
        });
        return false;
    });

    $(document).on("change", ".setRecruiterSelect", function(){
        var that = $(this),
            that_container = that.parent(),
            user_id = $(this).val();
        $.ajax({
            url: "/candidates/changeRecruiter",
            type: "GET",
            data: {"recruiter_id" : user_id, "user_id" : that.data("user-id")},
            success: function(data){
                that_container.html("<a href='#' class='setRecruiter' data-user-id='"+that.data("user-id")+"'>"+data+"</a>");
            },
            error: function(data){}
        });
    });

    $("#usersTable").tablesorter();
});