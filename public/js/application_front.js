$(window).ready(function(){

    $(document).on("submit", "#main_form", function(){


    });

    $(document).on("click", ".add_yet", function(){

        var that = $(this),
            thing = that.data("thing"),
            result_container = that.data("result-container"),
            counter_element = $("#count_"+result_container),
            val = counter_element.val();

        if(counter_element.val() === "-1") {
            counter_element.val(0);
        } else {
            val = counter_element.val();
        }

        $.ajax({
            url: "/getTemplate/"+thing,
            data: {
                "counter" : val
            },
            success: function(data){
                $("#"+result_container).append(data);
                maskedInit();
                counter_element.val(parseInt(counter_element.val())+1);
            }
        });
        return false;
    });

    $(document).on("click", ".yes_no input", function(){
        var that = $(this),
            value = that.val(),
            container = that.parents(".row").first();
        if(value === "1") {
            container.find(".description_thing").show();
        } else {
            container.find(".description_thing").hide();
        }
    });

    var url = window.location.href,
        pieces = url.split("#");
    if(pieces[1]) {
        var step = pieces[1].split("step")[1];
    } else {
        step = 1;
    }

    var counter = 1;
    $(".stepBlock").each(function(){
        var that = $(this);
        that.hide();
        counter++;
        if(counter != 10) {
            that.append("<div style='margin: 10px;'><a href='#step"+ counter +"' data-step='"+ counter +"' class='step btn btn-primary btn-sm'>Save and continue</a></div>");
        }
    });
    $(".stepBlock"+step).show();

    $(document).on("click",".step",function(){
        var that = $(this),
            step = that.data("step"),
            form = that.parents("form").first(),
            data = form.serialize(),
            action = form.data("action");

        if(!checkForm(form)) {
            return false;
        }

        if(typeof action != "undefined") {
            $.ajax({
                url: "/job_application/"+form.data("action"),
                data: data,
                type: "get",
                success: function(){},
                error: function(){
                    alert("Internal Error! Please, reload the page.");
                }
            });
        }
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        $(".stepBlock").hide();
        $(".stepBlock"+step).show();
        that.parents("ul").first().hide();
    });

    $("#Declined").click(function(){
        var that = $(this),
            form = $("#EOEForm");
        if(that.prop("checked")){
            form.hide();
        } else {
            form.show();
        }
    });

    $(document).on("click", ".btn_remove", function(){
        var that = $(this),
            container = that.parents(".row").first(),
            counter = that.parents(".panel-body").find("input[type=hidden]"),
            count_items = counter.val();
        counter.val(parseInt(count_items)-1);
        container.remove();
        return false;
    });

    $(document).on("click", ".btn_remove_panel", function(){
        var that = $(this),
            container = that.parents(".panel").first();
        container.remove();
        return false;
    });

    maskedInit();
});

function maskedInit() {
    /*$(".date").Zebra_DatePicker({
        "format" : "m/d/Y"
    });*/
    $('.date').combodate({
        format : "MM/DD/YYYY",
        template: "MM/DD/YYYY",
        "customClass": "form-control customFormControl",
        yearDescending : false,
        smartDays: true,
        firstItem: "name"
    });

    //$(".Zebra_DatePicker_Icon_Wrapper").css("width", "100%");
    //$(".Zebra_DatePicker").css("top", "80px");
    //$(".date").mask("99/99/9999");
    $(".date_year").mask("9999");
    $(".zip_code").mask("99999");
    $(".phone").mask("(999) 999-9999");
    $(".social_security").mask("999-99-9999");
}

function countChar(val) {
    var len = val.value.length,
        current_element = $(val);
    if (len >= 250) {
        current_element.parents("div").first().addClass("has-error");
        val.value = val.value.substring(0, 250);
    } else {
        current_element.parents("div").first().removeClass("has-error");
    }
    current_element.parents("div").first().find(".counter").text(250-len);
}

function checkForm(form) {
    var key = 0;
    form.find(".required").each(function(){
        var that = $(this),
            value = $(this).val();
        if(value == "") {
            that.css("border","2px solid red").addClass("needed");
            key = 1;
        }else {
            that.css("border", "1px solid #ccc").removeClass("needed");
        }
    });

    form.find(".req_radio").each(function(){
        var that = $(this),
            key_internal = 0;
        that.find("input[type=radio]").each(function(){
            var that_radio = $(this);
            if(that_radio.prop("checked")) {
                key_internal = 1;
            }
        });
        if(!key_internal) {
            key = 1;
            if(that[0].tagName == "TR"){
                that.css("color", "red").addClass("needed");
            } else {
                that.css("border","2px solid red").addClass("needed");
            }
        } else {
            if(that[0].tagName == "TR"){
                that.css("color", "#000").removeClass("needed");
            }
            if(!that.hasClass("panel-body")) {
                that.css("border", "none").removeClass("needed");
            } else {
                that.css("border", "1px solid #ccc").removeClass("needed");
            }
        }
    });

    if(key) {
        var item = $(".needed").first();
        item.focus();
        item.parents(".stepBlock").first().show().siblings().hide();
        return false;
    }

    return true;
}