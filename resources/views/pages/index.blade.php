@extends("layouts.app")

@section("content")
    <div class="banner">
        <div class="container">
            <div class="banner_form">
                <h1>Find Your Perfect Job ...
                    <span>Today!</span></h1>
                <div class="form_section">
                    <form>
                        <input type="text" name="adasd" placeholder="First name" />
                        <input type="text" name="adasd" placeholder="Last Name" />
                        <input type="text" name="adasd" placeholder="Phone Number" />
                        <input type="text" name="adasd" placeholder="Select Specialty" />
                        <input type="submit" value="send" />
                    </form>

                </div>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="mid_1">
            <h1>Vero RN Is A Company For Nurses, Founded By Nurses.  </h1>
            <p>Travel Nursing can be one of the most highly compensated, fun, and exciting opportunities available for a nurse with a dream.  At<br /> Vero RN, we are committed to helping you find the best possible travel nursing assignments across the United States.  As former<br /> traveling nurses ourselves, we understand the challenges associated with being a traveling nurse and have built our program to<br /> support the nurse so they can enjoy the benefits of traveling – without the hassle!</p>
            <div class="mid_1_left">
                <img src="/images/video.jpg"  />
                <a class="hvr-bounce-to-right" href="#">Apply Now </a>

            </div>
            <div class="mid_1_right"><img src="/images/mid_side_image.jpg" /></div>

        </div>

    </div>

    <div class="testimonial">
        <div class="container">
            <h1>Testimonials</h1>
            <p>What our customers are saying</p>
            <div class="testi_main_box">

                <div class="testi_main_box_inner_first">
                    <div class="testi_main_box_inner_first_left">
                        <div class="testi_main_box_inner_first_left_image"><img src="/images/jenny.jpg"/></div>
                        <h2>Jenny RN, OR</h2>


                    </div>
                    <div class="testi_main_box_inner_first_right">"I just wanted to say how much I appreciate all that you have done for me and I want to give a "shout out" to Vero RN for being one of the best companies I have every worked for. My recruiter is wonderful to work with; she goes out of her way to make every assignment the best. I would recommend Vero RN to anyone looking for a travel company. My time with this company has been the most rewarding experience and I hope to continue to work for them for many more assignments. So I want to say… "Thanks""I just wanted to say how much I appreciate all that you have done for me and I want to give a "shout out" to Vero RN for being one of the best companies I have every worked for. My recruiter is wonderful to work with; she goes out of her way to make every assignment the best. I would recommend Vero RN to anyone looking for a travel company. My time with this company has been the most rewarding experience and I hope to continue to work for them for many more assignments. So I want to say… "Thanks"</div>

                </div>

                <div class="testi_bottom_left">
                    <div class="testi_bottom_left_left">
                        <div class="testi_bottom_left_left_image"><img src="/images/jhone.jpg"/></div>
                        <h2>Matt, RN, ER</h2>


                    </div>
                    <div class="testi_bottom_left_right">I began traveling with Vero RN because I wanted a flexible and higher-paying job. Vero RN is very reliable and has exceeded my expectations. I work for Vero RN because I can achieve my goals financially while making my own schedule and ensuring that my family is top priority. I like the variety of assignments, and my recruiter has been very helpful in placing me in awesome positions! Thanks so much Vero!</div>

                </div>

                <div class="testi_bottom_left testi_bottom_left_last">
                    <div class="testi_bottom_left_left">
                        <div class="testi_bottom_left_left_image"><img src="/images/kim.jpg"/></div>
                        <h2>Kim RN, ICU</h2>


                    </div>
                    <div class="testi_bottom_left_right">I began traveling with Vero RN because I wanted a flexible and higher-paying job. Vero RN is very reliable and has exceeded my expectations. I work for Vero RN because I can achieve my goals financially while making my own schedule and ensuring that my family is top priority. I like the variety of assignments, and my recruiter has been very helpful in placing me in awesome positions! Thanks so much Vero!</div>

                </div>


                <div class="create_profile"><a class="hvr-bounce-to-right" href="#">Create Profile</a></div>

            </div>
        </div>

    </div>

    <div class="container">
        <div class="mid_2">
            <div class="mid_2_left">
                <h1>Over <span>10,223</span> assignments to choose from today!</h1>
                <p>Create an account and gain access to thousands of available assignments. </p>

                <ul>
                    <li><a class="hvr-bounce-to-right" href="#">Apply now</a></li>
                    <li><a class="hvr-bounce-to-right" href="#">search jobs</a></li>
                </ul>

            </div>

        </div>


    </div>

    <div class="bottom_part">
        <div class="bottom_part_anchor">
            <a class="hvr-bounce-to-right" href="#">SPEAK WITH A RECRUITER NOW</a>

        </div>


    </div>
@endsection