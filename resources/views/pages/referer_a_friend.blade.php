@extends("layouts.app")

@section("content")

    <div class="refer_friend"></div>


    <div class="container">
        <div class="avilable_job_inner">
            <h1>Refer A Friend</h1>
            <h3>Is 10 minutes of your time worth $500 — $1,000?<br />
                We think YES!</h3>
            <p>Refer a friend to Vero RN and receive a bonus of $500 after he or she completes a 13-week assignment. Refer a second <br />one and your bonus will be $750! Have a third healthcare professional friend to refer? The bonus goes to $1,000 — and for<br /> every referral you give us after the third nurse, you will also receive $1,000!</p>
            <p>So many of our best employees have come from referrals and we feel all those who take the time to refer good candidates<br /> to us should be generously rewarded! Our Refer-A-Friend program allows us to do that.</p>
            <p>How do you relay to us information about your referral? Simply fill out the form below. You'll get the proper credit and a<br /> generous payment after your referral has worked a 13-week assignment.</p>
            <p>It pays to have friends when you work with Vero RN!</p>
            <p>Thank you for your referrals.</p>










        </div>


    </div>

    <div class="refer_form_main">
        <div class="container">
            <h1>Refer a Friend Form</h1>
            <form>
                <div class="main_form">
                    <div class="refer_form_main_left">
                        <div class="refer_form_main_left_upper">

                            <label>Name :</label>
                            <input type="text" name="ff" placeholder="Enter your name" />

                        </div>
                        <div class="refer_form_main_left_upper">

                            <label>Address  :</label>
                            <input type="text" name="ff" placeholder="Address" />
                            <div class="input_box">
                                <input class="input" type="text" name="ff" placeholder="City" />
                                <input class="input" type="text" name="ff" placeholder="State" />
                                <input class="input" type="text" name="ff" placeholder="Zip Code" />
                            </div>
                        </div>

                        <div class="refer_form_main_left_upper">

                            <label>Phone  :</label>
                            <input type="text" name="ff" placeholder="Phone Number" />

                        </div>
                        <div class="refer_form_main_left_upper">

                            <label>Email :</label>
                            <input type="text" name="ff" placeholder="Email Address" />

                        </div>
                        <div class="refer_form_main_left_upper">

                            <label>Recruiter's Name :</label>
                            <input type="text" name="ff" placeholder="Name or Email Address" />

                        </div>

                    </div>

                    <div class="refer_form_main_left">
                        <div class="refer_form_main_left_upper">

                            <label>Friend's Name :</label>
                            <input type="text" name="ff" placeholder="Name" />

                        </div>
                        <div class="refer_form_main_left_upper">

                            <label>Address  :</label>
                            <input type="text" name="ff" placeholder="Address" />
                            <div class="input_box">
                                <input class="input" type="text" name="ff" placeholder="City" />
                                <input class="input" type="text" name="ff" placeholder="State" />
                                <input class="input" type="text" name="ff" placeholder="Zip Code" />
                            </div>
                        </div>

                        <div class="refer_form_main_left_upper">

                            <label>Phone  :</label>
                            <input type="text" name="ff" placeholder="Phone Number" />

                        </div>
                        <div class="refer_form_main_left_upper">

                            <label>Email :</label>
                            <input type="text" name="ff" placeholder="Email Address" />

                        </div>
                        <div class="refer_form_main_left_upper">

                            <label>Specialty :</label>
                            <input type="text" name="ff" placeholder="Specialty" />

                        </div>
                        <div class="refer_form_main_left_upper">

                            <label>Best time to contact:</label>
                            <input type="text" name="ff" placeholder="Contact" />

                        </div>
                        <div class="refer_form_main_left_upper">

                            <label>When does the nurse<br />
                                want to travel?</label>
                            <input type="text" name="ff" placeholder="When does the nurse want to travel ?" />

                        </div>

                    </div>
                </div>
                <div class="submit_box">
                    <input type="submit" value="send" />

                </div>


            </form>




        </div>


    </div>



    <div class="bottom_part">
        <div class="bottom_part_anchor">
            <a class="hvr-bounce-to-right" href="#">SPEAK WITH A RECRUITER NOW</a>

        </div>


    </div>
@endsection