@extends("layouts.app")

@section("content")

    <div class="about_us_banner"></div>
    <div class="about_us_container_inner">
        <div class="container">

            <div class="about_us_container_inner_left">
                <h1>Mission:</h1>
                <p><span>Vero RN</span> is committed to being the industry - leading provider of highly qualified healthcare professionals while providing our client facilities with the most cost effective solutions to their staffing demands.  </p>
                <p>At Vero RN, we are proud to support traveling nurses in accomplishing their dreams.  Being a nurse requires a selfless attitude and a deep compassion for others, and as a result, we were inspired to build a nurse staffing program that can truly help nurses enjoy all of the benefits of healthcare traveling - without the headaches.  We have assembled a dream team of industry experts, healthcare staffing leaders, and experienced traveling nurses to build the absolutely ideal system for recruiting, placing, and supporting our partnered nurses.  We are proud to say that we've accomplished our goals.</p>

                <p>How can we be so confident that we are the best at what we do?  Simple: we've been on both sides.  The majority of healthcare staffing companies, although wonderful, have not built their teams with a diversity of both corporate leaders and real-world travel nursing experience.  We are proud to have leaders in our company that have both recruited nurses for assignments – and been on assignments themselves.  As a result, Vero RN has been constructed with the nurse's perspective at its core, with focused attention placed on meeting the needs and demands of the individual nurse. </p>

                <p>Being a traveling nurse is one of the most exciting, adventurous, and financially valuable opportunities in the entire healthcare industry.  Nurses are able to experience a level of freedom and travel that most individuals do not get to enjoy until retirement age, and we at Vero RN are here to help introduce that exciting and rewarding lifestyle to you.  As current and former nurses, we would love to share our experiences with you first hand, and are happy to answer any and all questions you may have about healthcare travelling. </p>

            </div>


        </div>

    </div>

    <div class="who_we_are">
        <div class="container">
            <h1>Who Are We?</h1>
            <div class="video_box"><img src="images/who-we-are-video.jpg"  /></div>

        </div>


    </div>



    <div class="bottom_part">
        <div class="bottom_part_anchor">
            <a class="hvr-bounce-to-right" href="#">SPEAK WITH A RECRUITER NOW</a>

        </div>


    </div>
@endsection