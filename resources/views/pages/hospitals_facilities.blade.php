@extends("layouts.app")

@section("content")
    <div class="hospital_banner"></div>

    <div class="container">

        <div class="hospital_inner_box">
            <h1>As a company we strive to live our MISSION:</h1>

            <div class="hospital_inner_box_upper">
                <ul>
                    <li><strong>Vero RN</strong> is committed to being :</li>
                    <li>The industry leading provider of highly qualified healthcare professionals while providing our client facilities with the most <br /> cost effectivesolutions to their staffing demands.  </li>
                    <li><strong>Vero RN</strong> Core Values include: </li>
                </ul>

            </div>

            <div class="hospital_inner_box_second">

                <div class="hospital_inner_box_second_image"><img src="images/hospital-ico-1.jpg"  /></div>
                <p><strong>Quality- </strong>Excellence in customer service and quality patient care drives our decision-making process.</p>

            </div>

            <div class="hospital_inner_box_second">

                <div class="hospital_inner_box_second_image"><img src="images/hospital-ico-2.jpg"  /></div>
                <p class="p_margin"><strong>Just Experience -</strong> Our staff has been there before - working inside hospitals to transform patient are. We<br />
                    understand your challenges.</p>

            </div>

            <div class="hospital_inner_box_second">

                <div class="hospital_inner_box_second_image"><img src="images/hospital-ico-3.jpg"  /></div>
                <p ><strong>Innovative-</strong> We will remain vigilant in creating unique solutions to service the ever-changing needs within healthcare.</p>

            </div>

            <div class="hospital_inner_box_second">

                <div class="hospital_inner_box_second_image"><img src="images/hospital-ico-4.jpg"  /></div>
                <p class="p_margin" ><strong>Integrity- </strong> We conduct all business matters with integrity. We will lead the industry in professionalism and honesty.</p>

            </div>

            <div class="hospital_inner_box_second">

                <div class="hospital_inner_box_second_image"><img src="images/hospital-ico-5.jpg"  /></div>
                <p class="p_margin" ><strong>Leadership-  </strong> We are dedicated to create a culture of highly motivated and qualified team of professionals who perform <br />
                    the highest level of quality work each and every day.</p>

            </div>


            <div class="hospital_mid_3">
                <div class="hospital_mid_3_left"><img src="images/veron.jpg" /></div>
                <div class="hospital_mid_3_right">
                    <p><strong>Vero RN</strong> is dedicated to being one of the industry- leading providers of supplemental healthcare professionals.  We pride ourselves on staffing qualified and compassionate healthcare professionals across the US.  We are a quality and customer service-driven organization providing pre-qualified nurse and other healthcare traveler candidates and superior customer service nationwide.</p>
                    <p><strong>Vero RN</strong> provides personalized staffing solutions designed to maximize patient care while improving your bottom line.  </p>
                    <p>We partner with our facilities to provide solutions to their staffing demands in a variety of disciplines, including, nursing and allied.   Our goals are simple to staff our client facilities in a timely, cost effective manner with the most qualified healthcare professionals in the industry. </p>
                </div>

            </div>
            <h2>When you rely on Vero RN for all your staffing needs, here are some benefits you can look forward to:</h2>

            <div class="hosiptal_bottom_part">
                <ul>

                    <li>A solid clinical match with each healthcare professional submitted for a hospital interview.</li>
                    <li>Our dedicated RN Clinical Liaisons is a resource for both professionals and facilities to ensure a successful assignment.</li>
                    <li>We have one of the most extensive credentialing processes in the industry, working to ensure that our healthcare     	<br />	professionals are Joint Commission and OSHA compliant.</li>
                    <li>Our Quality Improvement enhances our ability to provide the highest quality healthcare professionals to our client 	<br />	facilities. </li>
                </ul>

                <p>With the partnership and commitment of our client facilities, we will together make a difference in the level of care that patients and their<br /> families expect and deserve.</p>


            </div>

        </div>

    </div>




    <div class="bottom_part">
        <div class="hospital_anchor">
            <a class="hvr-bounce-to-right" href="#">HELP ME FILL A POSITION TODAY!</a>

        </div>


    </div>

@endsection