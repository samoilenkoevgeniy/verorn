@extends("layouts.app")

@section("content")
    <div class="why_veron_banner"></div>
    <div class="container">
        <div class="why_veron_mid">
            <h1>Why VeroRN?</h1>
            <div class="why_veron_mid_first_box">
                <div class="why_veron_mid_first_box_image"><img src="images/ico-1.jpg"  /></div>
                <p> At Vero RN our Nurses comes FIRST!  We pride ourselves in the commitment to our nurses! </p>
            </div>

            <div class="why_veron_mid_first_box why_veron_mid_first_box_2">
                <div class="why_veron_mid_first_box_image"><img src="images/ico-2.jpg"  /></div>
                <p> We have a dedicated, experienced team of<br /> industry leading healthcare recruiters to<br /> support our partnered nurses with 24/7<br /> support.  Our experienced recruiters  get to<br /> know you to ensure the best placements <br />are the best fit for you!   </p>
            </div>

            <div class="why_veron_mid_first_box">
                <div class="why_veron_mid_first_box_image"><img src="images/ico-3.jpg"  /></div>
                <p> We treat you like you're part of our<br /> Vero RN family.
                </p>
                <p>We value your compassion, dedication and knowledge in the healthcare industry</p>
                <p>Your career is our focus; so let Vero RN be the gateway to your future opportunities!</p>
            </div>

        </div>


    </div>

    <div class="benifits">
        <div class="container">
            <h1>Benefits:</h1>
            <div class="text">At Vero RN, we value your hard work and dedication to the healthcare profession.  That's why at <br />
                Vero RN we believe in providing the most comprehensive and competitive benefits in the industry.  <br />Come work with us and enjoy:</div>

            <div class="benifits_left">
                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-1.jpg"  /></div>
                        <h2>1st day healthcare benefits</h2>
                    </div>
                    <p>We want you to rest easy and know that VeroRN has got your covered while on contract. We offer several great plans for you to choose from, including first day benefits!</p>

                </div>

                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-2.jpg"  /></div>
                        <h2>Comfortable private pet friendly housing</h2>
                    </div>
                    <p>Vero RN team is available to assist you with all of your housing needs. Our housing consultants ensure that your transition is as seamless as possible!</p>

                </div>

                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-3.jpg"  /></div>
                        <h2>Housing Stipends
                        </h2>
                    </div>
                    <p>Explore limitless options and opt for the weekly, housing allowance. Enjoy the independence of booking your own housing; spend your time and money on what is most important to you.</p>

                </div>

                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-4.jpg"  /></div>
                        <h2>Travel Reimbursement

                        </h2>
                    </div>
                    <p>If you qualify, Vero RN provides the reimbursement in one lump sum at the beginning of your assignment. Amounts can vary and are based on travel time, distance, etc… but ask your recruiter for details. </p>

                </div>

                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-5.jpg"  /></div>
                        <h2>Licensing Assistance and Reimbursement


                        </h2>
                    </div>
                    <p>We absolutely offer reimbursement. Just make sure you have that good old fashioned conversation with your Recruiter before your assignment begins to make sure this is covered and/or you qualify. Don't forget to save those receipts….you'll need them!</p>

                </div>

            </div>

            <div class="benifits_left">
                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-6.jpg"  /></div>
                        <h2>24/7 Support
                        </h2>
                    </div>
                    <p>If you have any questions or concerns, you can depend on our dedicated team of qualified team members to provide a solution.  </p>

                </div>

                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-7.jpg"  /></div>
                        <h2>Direct Deposit/Weekly Pay/Online Payroll Access
                        </h2>
                    </div>
                    <p>No matter where your travels take you, Vero RN can ensure that don't have to worry about receiving and depositing your hard earned money. We'll conveniently deposit your weekly pay, and you'll have access to your payroll information 24/7 through Paychex.</p>

                </div>

                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-8.jpg"  /></div>
                        <h2>Sign on and Completion Bonuses

                        </h2>
                    </div>
                    <p>You can earn up to an extra $1,000 for select assignments.  In addition, many of our client facilities also offer completion bonuses.  Ask your recruiter for details.</p>

                </div>

                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-9.jpg"  /></div>
                        <h2>Referral Bonuses


                        </h2>
                    </div>
                    <p>When you spread the word about Vero RN to your friends and colleagues and that referral completes a 13-week assignment, you will receive $1000 for each referral!  Unlimited payouts!  </p>

                </div>

                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-10.jpg"  /></div>
                        <h2>Competitive Exceptional Compensation Package



                        </h2>
                    </div>
                    <p>At Vero RN we value your professionalism, hard work, and dedication to the healthcare industry.  That's why Vero RN rewards you with industry-leading pay rates.  </p>

                </div>

                <div class="benifits_left_first">
                    <div class="benifits_left_first_upper">
                        <div class="benifits_left_first_upper_image"><img src="images/beni-ico-11.jpg"  /></div>
                        <h2>Top Assignments




                        </h2>
                    </div>
                    <p>Vero RN has great assignments covering the US. Talk to your recruiter today about a location that best fits your individual needs.  </p>

                </div>

            </div>


        </div>

    </div>



    <div class="bottom_part">
        <div class="bottom_part_anchor">
            <a class="hvr-bounce-to-right" href="#">SPEAK WITH A RECRUITER NOW</a>

        </div>
    </div>
@endsection