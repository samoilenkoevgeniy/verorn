@extends("layouts.app")

@section("content")

    <div class="avilable_job_banner"></div>


    <div class="container">
        <div class="avilable_job_inner">
            <h1>Vero RN Travel Nursing Job Finder</h1>
            <p>Click "Find Available Jobs" to see all jobs or narrow your job search by sorting on the fields below.</p>

            <div class="form_main">

                <div class="form_main_inner">
                    <div class="form_main_inner_box">
                        <form>
                            <div class="form_main_inner_box_upper">
                                <label>Select the state where you want to work:</label>
                                <select>
                                    <option value="volvo">Volvo</option>
                                    <option value="saab">Saab</option>
                                    <option value="mercedes">Mercedes</option>
                                    <option value="audi">Audi</option>
                                </select>
                            </div>

                            <div class="form_main_inner_box_upper">
                                <label>Select the specialty you want to work in:</label>
                                <select>
                                    <option value="volvo">Volvo</option>
                                    <option value="saab">Saab</option>
                                    <option value="mercedes">Mercedes</option>
                                    <option value="audi">Audi</option>
                                </select>
                            </div>

                            <div class="form_main_inner_box_upper new_width">
                                <label>Select the job type you are looking for:</label>
                                <select>
                                    <option value="volvo">Volvo</option>
                                    <option value="saab">Saab</option>
                                    <option value="mercedes">Mercedes</option>
                                    <option value="audi">Audi</option>
                                </select>
                            </div>

                            <div class="jobs_button">
                                <input type="submit" value="Available Jobs" />

                            </div>

                        </form>


                    </div>

                </div>






            </div>



            <div class="job_text"><strong>Jobs constantly come and go all day long!</strong><br />
                Please call us today to get your own personalized search:<br />
                <span>1-877-293-3670</span> Or, email us at jobs@verorn.com
            </div>

            <p class="margin_bottom">Want to be one of the first to know about the hottest jobs in your specialty? Click here to sign up for our Specialty Job Blast emails.</p>


        </div>


    </div>



    <div class="bottom_part">
        <div class="bottom_part_anchor">
            <a class="hvr-bounce-to-right" href="#">SPEAK WITH A RECRUITER NOW</a>

        </div>


    </div>
@endsection