@extends("layouts.app")

@section("content")
    <div class="container">
        <form action="{{ url("/form/process/".$form->id) }}" method="post">
            <div class="panel panel-default" style="margin-top: 160px;">
                <div class="panel-heading">
                    <strong>
                        {{ $form->name }}
                    </strong>
                </div>
                <div class="panel-body">
                    {!! csrf_field() !!}
                    <p>
                        This profile is for use by Cath Lab/Cardiac Cath Lab nurses with more than one year of experience in this specialty area.
                    </p>
                    <p>
                        Please enter your full legal name as it appears on your Social Security Card.
                    </p>
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label for="first_name">First name</label>
                            <input type="text" name="first_name" id="first_name" class="form-control" value="{{ $formProcess->first_name or "" }}" />
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label for="last_name">Last name</label>
                            <input type="text" name="last_name" id="last_name" class="form-control" value="{{ $formProcess->last_name or "" }}"/>
                        </div>
                    </div>
                    <div class="row margin_top_line">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label for="date">Date</label>
                            <input type="text" name="date" id="date" class="form-control date" value="{{ $formProcess->date or "" }}"/>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email " class="form-control" value="{{ $formProcess->email or "" }}"/>
                        </div>
                    </div>
                    <div class="row margin_top_line">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Please check the box/es below for each group in which you have provided age-appropriate care:
                                </div>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>
                                            A. Newborn/Neonate (birth-30days)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="A" />
                                        </td>
                                        <td>
                                            F. Adolescents (12-18 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="F"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            B. Infant (30 days-1 year)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="B" />
                                        </td>
                                        <td>
                                            G. Young adults (18-39 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="G"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            C. Toddler (1-3 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="C" />
                                        </td>
                                        <td>
                                            H. Middle adults (39-64 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="H"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            D. Preschooler (3-5 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="D" />
                                        </td>
                                        <td>
                                            I. Older adults (64-79 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="I"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            E. School age children (5-12 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="E" />
                                        </td>
                                        <td>
                                            J. Elderly adults (80+ years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="J"  />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row margin_top_line">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            Please indicate your level of experience by checking 1 box in each of the category below (1-less experience to 4-more experience):
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div>
                                1.   Theory, or only prior observation
                            </div>
                            <div>
                                3.   One - Two years current experience or need minimal assistance
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div>
                                2.   Less than one-year current experience or any previous experience
                            </div>
                            <div>
                                4.   Two plus years experience or functions independently
                            </div>
                        </div>
                    </div>
                    <div class="row margin_top_line questions_block">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <?
                        $right_ok = 0;
                        ?>
                        @foreach($questionBlocks as $questionBlock)
                            @if(!$questionBlock->left && !$right_ok)
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            @endif
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <strong>{{ $questionBlock->name }}</strong>
                                    </div>
                                    <table class="table table-bordered table-hover">
                                        <thead></thead>
                                        <tbody>
                                        @foreach($questionBlock->items as $question)
                                            @include("applicant.partials.formQuestionBlockItem", [
                                                "question" => $question,
                                                "formProcess" => $formProcess,
                                                "nested" => 0
                                            ])
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                                <?php
                                if($questionBlock->left == 0) {
                                    $right_ok = 1;
                                }
                                ?>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="panel panel-default" @if(!$form->age_specific_block) style="display: none;" @endif>
                <div class="panel-heading">
                    AGE SPECIFIC PRACTICE CRITERIA
                </div>
                <div class="panel-body">
                    <p>
                        Please check the boxes below for each age group for which you have expertise in providing age-appropriate nursing care.
                    </p>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            A. Newborn/Neonate (birth – 30 days)	<br />
                            B. Infant (30 days – 1 year)	<br />
                            C. Toddler (1 – 3 years)<br />
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            D. Preschooler (3 - 5 years)<br />
                            E. School age children (5 – 12 years)<br />
                            F. Adolescents (12 – 18 years)<br />
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            G. Young adults (18 – 39 years)<br />
                            H. Middle adults (39 - 64 years)<br />
                            I.  Older adults (64+ years)<br />
                        </div>
                    </div>

                    <div class="row margin_top_line">
                        <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                            Able to adapt care to incorporate normal growth and development.
                        </div>
                        <div class="col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-offset-sm-2 col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            @for($i = "A"; $i < "J"; $i++)
                                <div class="pull-left">
                                    {{ $i }}<br />
                                    <input type="radio" value="{{ $i }}" name="age_specific_1" @if(isset($formProcess) && $formProcess->age_specific_1 == $i) checked @endif>&nbsp;&nbsp;
                                </div>
                            @endfor
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="row margin_top_line">
                        <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                            Able to adapt method and terminology of patient instructions to their age, comprehension and maturity level.
                        </div>
                        <div class="col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-offset-sm-2 col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            @for($i = "A"; $i < "J"; $i++)
                                <div class="pull-left">
                                    {{ $i }}<br />
                                    <input type="radio" value="{{ $i }}" name="age_specific_2" @if(isset($formProcess) && $formProcess->age_specific_2 == $i) checked @endif>&nbsp;&nbsp;
                                </div>
                            @endfor
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="row margin_top_line">
                        <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                            Can ensure a safe environment reflecting specific needs of various age groups.
                        </div>
                        <div class="col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-offset-sm-2 col-lg-4 col-md-4 col-xs-4 col-sm-4">
                            @for($i = "A"; $i < "J"; $i++)
                                <div class="pull-left">
                                    {{ $i }}<br />
                                    <input type="radio" value="{{ $i }}" name="age_specific_3" @if(isset($formProcess) && $formProcess->age_specific_3 == $i) checked @endif>&nbsp;&nbsp;
                                </div>
                            @endfor
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default" @if(!$form->primary_experience_block) style="display: none;" @endif>
                <div class="panel-heading">
                    MY EXPERIENCE IS PRIMARILY IN (Please indicate number of years)
                </div>
                <div class="panel-body">
                    @foreach($form->experiences as $experience)
                        @if($experience->name)
                            <?php
                            if(isset($formProcess)) {
                                $check = \App\FormProcessExperience::whereFormProcessId($formProcess->id)->whereExpId($experience->id)->first();
                            }else {
                                $check = false;
                            }
                            ?>

                            <input type="hidden" name="experience[{{ $experience->id }}][exp_id]" value="{{ $experience->id }}" />
                            <div class="row margin_top_line">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <input type="hidden" name="experience[{{ $experience->id }}][name]" value="0" />
                                    <input type="checkbox" name="experience[{{ $experience->id }}][name]" value="1" @if(isset($formProcess) && $check) checked @endif/> {{ $experience->name }}
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
                                    <input type="text" name="experience[{{ $experience->id }}][years]" value="{{ $check->years or "" }}" class="form-control"/> &nbsp;&nbsp; year(s)
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="panel panel-default" @if(!$form->certification_block) style="display: none;" @endif>
                <div class="panel-heading">
                    CERTIFICATION
                </div>
                <div class="panel-body">
                    <p>
                        Please check the boxes below and indicate the expiration date for each certificate that you have.  If you do not know the exact date, please use the last date of the specific month (e.g., 05/31/2004).
                    </p>
                    @foreach($form->certificates as $certificate)
                        <input type="hidden" name="certificate[{{ $certificate->id }}][cert_id]" value="{{ $certificate->id }}" />
                        <?php
                        if(isset($formProcess)) {
                            $check = \App\FormProcessCertificate::whereFormProcessId($formProcess->id)->whereCertId($certificate->id)->first();
                        }else {
                            $check = false;
                        }
                        ?>
                        <div class="row margin_top_line">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <input type="hidden" name="certificate[{{ $certificate->id }}][name]" value="0" />
                                <input type="checkbox" name="certificate[{{ $certificate->id }}][name]" value="1" @if(isset($formProcess) && $check) checked @endif /> {{ $certificate->name }}
                            </div>
                            @if($certificate->text)
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <input type="text" name="certificate[{{ $certificate->id }}][text]" class="form-control" value="{{ $check->value or "" }}"/>
                                </div>
                            @endif
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
                                Exp. Date:&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="certificate[{{ $certificate->id }}][date]" value="{{ $check->date or "" }}" class="form-control date"/>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Confirmation
                        </div>
                        <div class="panel-body">
                            <p>
                                The information I have given is true and accurate to the best of my knowledge. By clicking the Submit button I hereby authorize Vero RN to release this Cath Lab/Cardiac Cath Lab skills checklist to Vero RN-related facilities in consideration of my employment.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <input type="submit" value="Submit application" class="btn btn-primary" />
            </div>
            <br />
        </form>
    </div>
@endsection

@section("scripts")
    <script src="{{ url("/js/libs/masonry.pkgd.min.js") }}"></script>
    <script src="{{ url("/js/libs/jquery.maskedinput.js") }}"></script>
    <script src="{{ url("/js/application_front.js") }}"></script>
    <script>
        //$('.questions_block').masonry()
    </script>
@endsection