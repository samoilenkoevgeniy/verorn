@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-xs-6 col-sm-6">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url("/home") }}"><i class="fa fa-btn fa-home"></i>Dashboard</a></li>
                    <li class="list-group-item"><a href="{{ url("/edit") }}"><i class="fa fa-btn fa-user"></i> Edit Profile</a></li>
                    <li class="list-group-item"><a href="{{ url("/applicant/application") }}"><i class="fa fa-btn fa-tasks"></i>Job Application {{ \App\JobApplication::isFilled(Auth::user()->id) ? " (Completed)" : "" }}</a></li>
                    <li class="list-group-item active"><a style="color: #fff;"  href="{{ url("/forms") }}"><i class="fa fa-btn fa-align-left"></i>Skills Checklists</a></li>
                    <li class="list-group-item">{!! Cache::get("who_is_my_recruiter_".Auth::user()->id) !!}</li>
                    <li class="list-group-item"><a href="{{ url("/logout") }}">Log Out</a></li>
                </ul>
            </div>
            <div class="col-md-8">
                @foreach($forms as $form)
                    <li class="list-group-item"><a href="{{ url("/applicant/form/".$form->id) }}">{{ $form->name }} {{ \App\FormProcess::isFilled(Auth::user()->id, $form->id) ? " (Completed)" : "" }}</a></li>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section("scripts")

@endsection