@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1>Hello, {{ $user->name }}!</h1>
            <form action="{{ url("/first_login") }}" method="POST">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label for="user_id">Please, select a recruiter:</label>
                    <br />
                    <select name="user_id" id="user_id">
                        <option value="NULL">I do not have a recruiter!</option>
                        @foreach(\App\User::where("role", "recruiter")->get() as $recruiter)
                            <option value="{{ $recruiter->id }}">{{ $recruiter->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" value="Submit" class="btn_custom" />
                </div>
            </form>
        </div>
    </div>
</div>
@endsection


@section("scripts")
    <script src="{{ url("/js/global.js") }}"></script>
    <script src="{{ url("/js/jquery.tablesorter.pager.js") }}"></script>
    <script src="{{ url("/js/candidates.js") }}"></script>
@endsection
