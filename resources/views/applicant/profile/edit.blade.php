@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                @if(\Session("message"))
                    <div class="alert alert-success">
                        {{ \Session("message") }}
                    </div>
                @endif
            </div>
            <div class="col-lg-4 col-md-4 col-xs-6 col-sm-6">
                @if(Auth::user()->role == "applicant")
                    <ul class="list-group">
                        <li class="list-group-item "><a href="{{ url("/home") }}"><i class="fa fa-btn fa-home"></i>Dashboard</a></li>
                        <li class="list-group-item active"><a style="color: #fff;"  href="{{ url("/edit") }}"><i class="fa fa-btn fa-user"></i> Edit Profile</a></li>
                        <li class="list-group-item"><a href="{{ url("/applicant/application") }}"><i class="fa fa-btn fa-tasks"></i>Job Application {{ \App\JobApplication::isFilled(Auth::user()->id) ? " (Completed)" : "" }}</a></li>
                        <li class="list-group-item"><a href="{{ url("/forms") }}"><i class="fa fa-btn fa-align-left"></i>Skills Checklists</a></li>
                        <li class="list-group-item">{!! Cache::get("who_is_my_recruiter_".Auth::user()->id) !!}</li>
                        <li class="list-group-item"><a href="{{ url("/logout") }}">Log Out</a></li>
                    </ul>
                    @elseif(Auth::user()->role == "administrator")
                    <ul class="list-group">
                        <li class="list-group-item "><a href="{{ url("/home") }}">Dashboard</a></li>
                        <li class="list-group-item active"><a style="color: #fff;"  href="{{ url("/edit") }}">Edit Profile</a></li>
                        <li class="list-group-item"><a href="{{ url("/candidates") }}">Сandidates</a></li>
                        <li class="list-group-item"><a href="{{ url("/recruiters") }}">Recruiters</a></li>
                        <li class="list-group-item"><a href="{{ url("/logout") }}">Log Out</a></li>
                    </ul>
                    @elseif(Auth::user()->role == "recruiter")
                    <ul class="list-group">
                        <li class="list-group-item"><a href="{{ url("/home") }}">Dashboard</a></li>
                        <li class="list-group-item active"><a style="color: #fff;"  href="{{ url("/edit") }}">Edit Profile</a></li>
                        <li class="list-group-item"><a href="{{ url("/logout") }}">Log Out</a></li>
                    </ul>
                @endif
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Profile @if(isset($recruiter)) of recruiter @endif</div>
                    <div class="panel-body">
                        @if(\Session("error_referral"))
                            <div class="alert alert-danger">
                                {{ \Session("error_referral") }}
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/edit') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" name="redirect" value="/recruiters" />
                            <input type="hidden" name="user_id" value="{{ $user->id }}"/>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ $user->name }}" />

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}" />

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}" />

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Phone</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="phone" value="{{ $user->phone }}" />

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            @if(Auth::user()->role == "applicant")
                                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Country :</label>
                                    <div class="col-md-6">
                                        <select name="country" id="country" class="form-control">
                                            <option value="0">Select a country</option>
                                            <option value="US" @if(Auth::user()->country == "US") selected @endif>US</option>
                                            <option value="Canada" @if(Auth::user()->country == "Canada") selected @endif>Canada</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">State:</label>
                                    <div class="col-md-6">
                                        <select disabled id="cap_select" class="form-control" @if(!empty(Auth::user()->country)) style="display: none;" @endif>
                                            <option value="0">First Please select your country</option>
                                        </select>
                                        <select name="country_state_id" id="state_id" class="form-control" @if(Auth::user()->country != "US") style="display: none;" disabled="disabled" @endif>
                                            <option value="0">---</option>
                                            @foreach(\App\CountryState::whereCountry("US")->get() as $item)
                                                <option value="{{ $item->id }}" @if(Auth::user()->country_state_id == $item->id) selected @endif>{{ $item->text }}</option>
                                            @endforeach
                                        </select>

                                        <select name="country_state_id" id="state_id_canada" class="form-control" @if(Auth::user()->country != "Canada") style="display: none;" disabled="disabled" @endif>
                                            <option value="0">---</option>
                                            @foreach(\App\CountryState::whereCountry("Canada")->get() as $item)
                                                <option value="{{ $item->id }}" @if(Auth::user()->country_state_id == $item->id) selected @endif>{{ $item->text }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Profession:</label>
                                    <div class="col-md-6">
                                        <select name="profession_id" id="profession_id" class="form-control">
                                            <option value="0">Select a profession</option>
                                            @foreach(\App\Profession::all() as $item)
                                                <option value="{{ $item->id }}" @if(Auth::user()->profession_id == $item->id) selected @endif>{{ $item->text }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('speciality_id') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Specialty:</label>
                                    <div class="col-md-6">
                                        <select name="speciality_id" id="specialty_id" class="form-control">
                                            @if(!Auth::user()->profession_id)
                                                <option value="0">---</option>
                                            @else
                                                @foreach(\App\ProfessionSpeciality::whereProfessionId(Auth::user()->profession_id)->get() as $item)
                                                    <option value="{{ $item->speciality->id }}" @if(Auth::user()->speciality_id == $item->speciality->id) selected @endif>{{ $item->speciality->text }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('years') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">Years:</label>
                                    <div class="col-md-6">
                                        <select name="years" id="years" class="form-control">
                                            @for($i = 0; $i < 26; $i++)
                                                <option value="{{ $i }}" @if(Auth::user()->years == $i) selected @endif>{{ $i }} years</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="registry_edit">
                                        <i class="fa fa-btn fa-user"></i>Update Profile
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script src="/js/registry.js"></script>
@endsection