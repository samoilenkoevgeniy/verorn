<div class="row margin_top_line_sm">
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <label for="type">Cert type</label>
            <input type="text" name="certificates[type][{{ $counter or 0 }}]" id="type" class="required form-control" value="{{ $item['type'] or "" }}"/>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <label for="number">Number</label>
            <input type="text" name="certificates[number][{{ $counter or 0 }}]" id="number" class="required form-control" value="{{ $item['number'] or "" }}"/>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
            <label for="date">Cert Date</label><br />
            <input type="text" name="certificates[date][{{ $counter or 0 }}]" id="date" class="required form-control date" value="{{ $item['date'] or "" }}"/>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
            <label for="expiration_date">Expiration Date</label><br />
            <input type="text" name="certificates[expiration_date][{{ $counter or 0 }}]" id="expiration_date" class="required form-control date" value="{{ $item['expiration_date'] or "" }}"/>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <a href="#" class="btn btn-danger btn_remove">Remove This</a>
    </div>
</div>