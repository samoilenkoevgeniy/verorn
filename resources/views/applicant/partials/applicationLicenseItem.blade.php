                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         <div class="row margin_top_line_sm">
    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <label for="licensies_state_{{ $counter or 0 }}">State</label>
            <!--<input type="text" name="licensies[state][{{ $counter or 0 }}]"  class="form-control" value="{{ $item['state'] or "" }}"/>-->
            <select name="licensies[state][{{ $counter or 0 }}]" id="licensies_state_{{ $counter or 0 }}" class="required form-control">
                @include("partials.states", [
                    "application" => isset($item) ? $item : ["state" => ""],
                    "field" => "state"
                ])
            </select>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <label for="number">License #</label>
            <input type="text" name="licensies[number][{{ $counter or 0 }}]" id="number" class="required form-control" value="{{ $item['number'] or "" }}"/>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
            <label for="expiration_date">Expiration Date</label>
            <br />
            <input type="text" name="licensies[expiration_date][{{ $counter or 0 }}]" id="expiration_date" class="required form-control date" value="{{ $item['expiration_date'] or "" }}"/>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 req_radio">
            <br />

            <input type="hidden" name="licensies[is_active][{{ $counter or 0 }}]" value="0" />
            <input type="radio" name="licensies[is_active][{{ $counter or 0 }}]" value="active" @if(isset($item) && $item['is_active'] == "active") checked @endif/>
            <label for="is_active">Active</label>
            &nbsp;&nbsp;
            <input type="radio" name="licensies[is_active][{{ $counter or 0 }}]" value="inactive" @if(isset($item) && $item['is_active'] == "inactive") checked @endif/>
            <label for="is_active">Inactive</label>
            &nbsp;&nbsp;
            <input type="radio" name="licensies[is_active][{{ $counter or 0 }}]" value="compact" @if(isset($item) && $item['is_active'] == "compact") checked @endif/>
            <label for="is_active">Compact</label>
        </div>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
        <a href="#" class="btn btn-danger btn_remove">Remove This</a>
    </div>
</div>