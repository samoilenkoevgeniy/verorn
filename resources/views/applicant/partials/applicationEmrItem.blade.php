<div class="row margin_top_line_sm">
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <label for="speciality">EMR Charting Software</label>
            <!--<input type="text" name="emrs[charting_equipment][{{ $counter or 0 }}]" class="form-control" value="{{ $item['charting_equipment'] or "" }}"/>-->
            <select name="emrs[charting_equipment][{{ $counter or 0 }}]" id="charting_equipment_{{ $counter or 0 }}" class="form-control">
                <option value='' @if(isset($item) && $item['charting_equipment'] == "") selected @endif></option><option value='McKesson' @if(isset($item) && $item['charting_equipment'] == "McKesson") selected @endif>McKesson</option><option value='EPIC' @if(isset($item) && $item['charting_equipment'] == "EPIC") selected @endif>EPIC</option><option value='Meditech' @if(isset($item) && $item['charting_equipment'] == "Meditech") selected @endif>Meditech</option><option value='Cerner' @if(isset($item) && $item['charting_equipment'] == "Cerner") selected @endif>Cerner</option><option value='Eclipsys' @if(isset($item) && $item['charting_equipment'] == "Eclipsys") selected @endif>Eclipsys</option><option value='Siemens' @if(isset($item) && $item['charting_equipment'] == "Siemens") selected @endif>Siemens</option>
            </select>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <label for="years">Years Experience (with equipment)</label>
            <input type="number" name="emrs[years][{{ $counter or 0 }}]" class="form-control" value="{{ $item['years'] or "" }}"/>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <a href="#" class="btn btn-danger btn_remove">Remove This</a>
    </div>
</div>