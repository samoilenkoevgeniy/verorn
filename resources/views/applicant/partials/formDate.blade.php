<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <select name="" id="" class="form-control">
            @for($i = 1; $i < 13; $i++)
                <option value="{{ $i }}">{{ $i }}</option>
            @endfor
        </select>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <select name="" id="" class="form-control">
            @for($i = 1; $i < 31; $i++)
                <option value="{{ $i }}">{{ $i }}</option>
            @endfor
        </select>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
</div>