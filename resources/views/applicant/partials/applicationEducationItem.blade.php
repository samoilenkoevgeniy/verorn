<div class="row margin_top_line_sm">
    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <label for="place">College/University</label>
            <input type="text" name="educations[place][{{ $counter or 0 }}]" id="place" class="required form-control" value="{{ $item['place'] or "" }}"/>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <label for="city_state">City</label>
            <input type="text" name="educations[city_state][{{ $counter or 0 }}]" id="city_state" class="required form-control" value="{{ $item['city_state'] or "" }}"/>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
            <label for="city_state">State</label>
            <!--<input type="text" name="educations[state][{{ $counter or 0 }}]" id="state" class="form-control" value="{{ $item['state'] or "" }}"/>-->
            <select name="educations[state][{{ $counter or 0 }}]" id="state_{{ $counter or 0}}" class="required form-control">
                @include("partials.states", [
                    "application" => isset($item) ? $item : ["state" => ""],
                    "field" => "state"
                ])
            </select>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <label for="educations_type_of_degree_{{ $counter or 0 }}">Type of Degree/Program</label>
            <select class="required form-control" name="educations[type_of_degree][{{ $counter or 0 }}]" id="educations_type_of_degree_{{ $counter or 0 }}">
                <option value='None' @if(isset($item) && $item['type_of_degree'] == "None") selected @endif>None</option><option value='Diploma' @if(isset($item) && $item['type_of_degree'] == "Diploma") selected @endif>Diploma</option><option value='ADN' @if(isset($item) && $item['type_of_degree'] == "ADN") selected @endif>ADN</option><option value='BSN' @if(isset($item) && $item['type_of_degree'] == "BSN") selected @endif>BSN</option><option value='MSN' @if(isset($item) && $item['type_of_degree'] == "MSN") selected @endif>MSN</option><option value='NP' @if(isset($item) && $item['type_of_degree'] == "NP") selected @endif>NP</option><option value='ST/ORT' @if(isset($item) && $item['type_of_degree'] == "ST/ORT") selected @endif>ST/ORT</option><option value='Bachelors' @if(isset($item) && $item['type_of_degree'] == "Bachelors") selected @endif>Bachelors</option><option value='CNA' @if(isset($item) && $item['type_of_degree'] == "CNA") selected @endif>CNA</option><option value='CRNA' @if(isset($item) && $item['type_of_degree'] == "CRNA") selected @endif>CRNA</option><option value='Diag Tech' @if(isset($item) && $item['type_of_degree'] == "Diag Tech") selected @endif>Diag Tech</option><option value='Dietitian' @if(isset($item) && $item['type_of_degree'] == "Dietitian") selected @endif>Dietitian</option><option value='Doctorate' @if(isset($item) && $item['type_of_degree'] == "Doctorate") selected @endif>Doctorate</option><option value='EMT' @if(isset($item) && $item['type_of_degree'] == "EMT") selected @endif>EMT</option><option value='LPN/LVN' @if(isset($item) && $item['type_of_degree'] == "LPN/LVN") selected @endif>LPN/LVN</option><option value='Massage Therapist' @if(isset($item) && $item['type_of_degree'] == "Massage Therapist") selected @endif>Massage Therapist</option><option value='Masters' @if(isset($item) && $item['type_of_degree'] == "Masters") selected @endif>Masters</option><option value='Monitor Tech' @if(isset($item) && $item['type_of_degree'] == "Monitor Tech") selected @endif>Monitor Tech</option><option value='Ophthalmic Tech' @if(isset($item) && $item['type_of_degree'] == "Ophthalmic Tech") selected @endif>Ophthalmic Tech</option><option value='OT' @if(isset($item) && $item['type_of_degree'] == "OT") selected @endif>OT</option><option value='COTA' @if(isset($item) && $item['type_of_degree'] == "COTA") selected @endif>COTA</option><option value='PT' @if(isset($item) && $item['type_of_degree'] == "PT") selected @endif>PT</option><option value='PTA' @if(isset($item) && $item['type_of_degree'] == "PTA") selected @endif>PTA</option><option value='Paramedic' @if(isset($item) && $item['type_of_degree'] == "Paramedic") selected @endif>Paramedic</option><option value='Rad Tech' @if(isset($item) && $item['type_of_degree'] == "Rad Tech") selected @endif>Rad Tech</option><option value='Rec Therapist' @if(isset($item) && $item['type_of_degree'] == "Rec Therapist") selected @endif>Rec Therapist</option><option value='SLP' @if(isset($item) && $item['type_of_degree'] == "SLP") selected @endif>SLP</option><option value='Ultrasound' @if(isset($item) && $item['type_of_degree'] == "Ultrasound") selected @endif>Ultrasound</option><option value='Xray' @if(isset($item) && $item['type_of_degree'] == "Xray") selected @endif>Xray</option><option value='Other' @if(isset($item) && $item['type_of_degree'] == "Other") selected @endif>Other</option>
            </select>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
            <label for="month_year_graduated">Grad. Date</label>
            <br />
            <input type="text" name="educations[month_year_graduated][{{ $counter or 0 }}]" id="month_year_graduated" class="required form-control date" value="{{ $item['month_year_graduated'] or "" }}"/>
        </div>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
        <a href="#" class="btn btn-danger btn_remove">Remove This</a>
    </div>
</div>