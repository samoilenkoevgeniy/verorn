<div class="panel panel-default">
    <div class="panel-heading">
        <div class="pull-left">
            Employment #{{ isset($counter) ? $counter+1 : 1 }}
        </div>
        <div class="pull-right">
            <a href="#" class="btn btn-danger btn_remove_panel">Remove This</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
        <div class="row margin_top_line_sm">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <label for="facility">Facility/Employer</label>
                <input type="text" name="employments[facility][{{ $counter or 0 }}]" id="facility" class="required form-control" value="{{ $item['facility'] or "" }}"/>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <label for="city_state"># of Beds/Facility</label>
                <input type="text" name="employments[number_of_beds_facility][{{ $counter or 0 }}]" id="number_of_beds_facility" class="form-control" value="{{ $item['number_of_beds_facility'] or "" }}"/>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <label for="city_state">Facility City</label>
                <input type="text" name="employments[city_state][{{ $counter or 0 }}]" id="city_state" class="required form-control" value="{{ $item['city_state'] or "" }}"/>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <label for="employments_facility_state_{{ $counter or 0 }}">Facility state</label>
                <!--
                <input type="text" name="employments[facility_state][{{ $counter or 0 }}]" id="facility_state" class="form-control" value="{{ $item['facility_state'] or "" }}"/>
                -->
                <select name="employments[facility_state][{{ $counter or 0 }}]" id="employments_facility_state_{{ $counter or 0 }}" class="required form-control">
                    @include("partials.states", [
                        "application" => isset($item) ? $item : ["facility_state" => ""],
                        "field" => "facility_state"
                    ])
                </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <label for="employments_unit_{{ $counter or 0 }}">Unit</label>
                <!--
                <input type="text" name="employments[unit][{{ $counter or 0 }}]" id="unit" class="form-control" value="{{ $item['unit'] or "" }}"/>
                -->
                <select class="required form-control" name="employments[unit][{{ $counter or 0 }}]" id="employments_unit_{{ $counter or 0 }}">
                    <option value='' @if(isset($item) && $item['unit'] == "") selected @endif></option><option value='ANTEPARTUM' @if(isset($item) && $item['unit'] == "ANTEPARTUM") selected @endif>ANTEPARTUM</option><option value='BMT' @if(isset($item) && $item['unit'] == "BMT") selected @endif>BMT</option><option value='CARDIAC' @if(isset($item) && $item['unit'] == "CARDIAC") selected @endif>CARDIAC</option><option value='CASE MANAGER' @if(isset($item) && $item['unit'] == "CASE MANAGER") selected @endif>CASE MANAGER</option><option value='CATH LAB' @if(isset($item) && $item['unit'] == "CATH LAB") selected @endif>CATH LAB</option><option value='CCU' @if(isset($item) && $item['unit'] == "CCU") selected @endif>CCU</option><option value='CST' @if(isset($item) && $item['unit'] == "CST") selected @endif>CST</option><option value='CVICU' @if(isset($item) && $item['unit'] == "CVICU") selected @endif>CVICU</option><option value='CVOR' @if(isset($item) && $item['unit'] == "CVOR") selected @endif>CVOR</option><option value='DIALYSIS' @if(isset($item) && $item['unit'] == "DIALYSIS") selected @endif>DIALYSIS</option><option value='ENDOSCOPY' @if(isset($item) && $item['unit'] == "ENDOSCOPY") selected @endif>ENDOSCOPY</option><option value='ER' @if(isset($item) && $item['unit'] == "ER") selected @endif>ER</option><option value='HEM/ONC' @if(isset($item) && $item['unit'] == "HEM/ONC") selected @endif>HEM/ONC</option><option value='HOME HEALTH' @if(isset($item) && $item['unit'] == "HOME HEALTH") selected @endif>HOME HEALTH</option><option value='HOSPICE' @if(isset($item) && $item['unit'] == "HOSPICE") selected @endif>HOSPICE</option><option value='ICU' @if(isset($item) && $item['unit'] == "ICU") selected @endif>ICU</option><option value='IMC' @if(isset($item) && $item['unit'] == "IMC") selected @endif>IMC</option><option value='LD' @if(isset($item) && $item['unit'] == "LD") selected @endif>LD</option><option value='MICU' @if(isset($item) && $item['unit'] == "MICU") selected @endif>MICU</option><option value='MS' @if(isset($item) && $item['unit'] == "MS") selected @endif>MS</option><option value='MS TELE' @if(isset($item) && $item['unit'] == "MS TELE") selected @endif>MS TELE</option><option value='NEURO - ICU' @if(isset($item) && $item['unit'] == "NEURO - ICU") selected @endif>NEURO - ICU</option><option value='NICU' @if(isset($item) && $item['unit'] == "NICU") selected @endif>NICU</option><option value='OB' @if(isset($item) && $item['unit'] == "OB") selected @endif>OB</option><option value='ONCOLOGY' @if(isset($item) && $item['unit'] == "ONCOLOGY") selected @endif>ONCOLOGY</option><option value='OR' @if(isset($item) && $item['unit'] == "OR") selected @endif>OR</option><option value='OT' @if(isset($item) && $item['unit'] == "OT") selected @endif>OT</option><option value='OTA' @if(isset($item) && $item['unit'] == "OTA") selected @endif>OTA</option><option value='OTR' @if(isset($item) && $item['unit'] == "OTR") selected @endif>OTR</option><option value='PACU' @if(isset($item) && $item['unit'] == "PACU") selected @endif>PACU</option><option value='PCU' @if(isset($item) && $item['unit'] == "PCU") selected @endif>PCU</option><option value='PEDS' @if(isset($item) && $item['unit'] == "PEDS") selected @endif>PEDS</option><option value='PICU' @if(isset($item) && $item['unit'] == "PICU") selected @endif>PICU</option><option value='PP' @if(isset($item) && $item['unit'] == "PP") selected @endif>PP</option><option value='PSYCH' @if(isset($item) && $item['unit'] == "PSYCH") selected @endif>PSYCH</option><option value='PT' @if(isset($item) && $item['unit'] == "PT") selected @endif>PT</option><option value='PTA' @if(isset($item) && $item['unit'] == "PTA") selected @endif>PTA</option><option value='RAD Tech' @if(isset($item) && $item['unit'] == "RAD Tech") selected @endif>RAD Tech</option><option value='RAD Therapist' @if(isset($item) && $item['unit'] == "RAD Therapist") selected @endif>RAD Therapist</option><option value='RADIOLOGY' @if(isset($item) && $item['unit'] == "RADIOLOGY") selected @endif>RADIOLOGY</option><option value='REHAB' @if(isset($item) && $item['unit'] == "REHAB") selected @endif>REHAB</option><option value='RT' @if(isset($item) && $item['unit'] == "RT") selected @endif>RT</option><option value='SICU' @if(isset($item) && $item['unit'] == "SICU") selected @endif>SICU</option><option value='SLP' @if(isset($item) && $item['unit'] == "SLP") selected @endif>SLP</option><option value='STEPDOWN' @if(isset($item) && $item['unit'] == "STEPDOWN") selected @endif>STEPDOWN</option><option value='SUB ACUTE' @if(isset($item) && $item['unit'] == "SUB ACUTE") selected @endif>SUB ACUTE</option><option value='TELE' @if(isset($item) && $item['unit'] == "TELE") selected @endif>TELE</option>
                </select>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <label for="facility">No. of Beds/Unit</label>
                <input type="text" name="employments[no_of_beds_unit][{{ $counter or 0 }}]" id="no_of_beds_unit" class="form-control" value="{{ $item['no_of_beds_unit'] or "" }}"/>
            </div>
        </div>
        <div class="row margin_top_line_sm">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <label for="supervisor">Supervisor</label>
                <input type="text" name="employments[supervisor][{{ $counter or 0 }}]" id="supervisor" class="form-control" value="{{ $item['supervisor'] or "" }}"/>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <label for="supervisor">Supervisor's Title</label>
                <input type="text" name="employments[supervisors_title][{{ $counter or 0 }}]" id="supervisors_title" class="form-control" value="{{ $item['supervisors_title'] or "" }}"/>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <label for="phone">Supervisor's Phone</label>
                <input type="text" name="employments[phone][{{ $counter or 0 }}]" id="phone" class="form-control phone" value="{{ $item['phone'] or "" }}"/>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <label for="number_of_beds">Avg. Pat. Ratio/Unit</label>
                <input type="text" name="employments[avg_pat_ratio_unit][{{ $counter or 0 }}]" id="avg_pat_ratio_unit" class="form-control" value="{{ $item['avg_pat_ratio_unit'] or "" }}"/>
            </div>
        </div>
        <div class="row margin_top_line_sm">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <label for="supervisor">Position Held</label>
                <input type="text" name="employments[position_held][{{ $counter or 0 }}]" id="position_held" class="required form-control" value="{{ $item['position_held'] or "" }}"/>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <label for="supervisor">Unit Description</label>
                <input type="text" name="employments[unit_description][{{ $counter or 0 }}]" id="unit_description" class="form-control" value="{{ $item['unit_description'] or "" }}"/>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
                <label for="date_start">Date Start</label><br />
                <input type="text" name="employments[date_start][{{ $counter or 0 }}]" id="date_start" class="required form-control date" value="{{ $item['date_start'] or "" }}"/>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 form-inline">
                        <label for="date_end">Date End</label><br />
                        <input type="text" name="employments[date_end][{{ $counter or 0 }}]" id="date_end" class="form-control date" value="{{ $item['date_end'] or "" }}"/>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <label for="persent">Present</label><br />
                        <input type="hidden" name="employments[present][{{ $counter or 0 }}]" value="0" />
                        <input type="checkbox" name="employments[present][{{ $counter or 0 }}]" value="1" @if(isset($item) && $item['present']) checked @endif/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin_top_line_sm">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <label for="employments_facility_type_{{ $counter or 0 }}">Facility Type</label>
                <!--
                <input type="text" name="employments[facility_type][{{ $counter or 0 }}]" id="facility_type" class="form-control" value="{{ $item['facility_type'] or "" }}"/>
                -->
                <select class="required form-control" name="employments[facility_type][{{ $counter or 0 }}]" id="employments_facility_type_{{ $counter or 0 }}">
                    <option value='' @if(isset($item) && $item['facility_type'] == "") selected @endif></option><option value='Teaching Facility' @if(isset($item) && $item['facility_type'] == "Teaching Facility") selected @endif>Teaching Facility</option><option value='Trauma Center' @if(isset($item) && $item['facility_type'] == "Trauma Center") selected @endif>Trauma Center</option><option value='Teaching/Trauma' @if(isset($item) && $item['facility_type'] == "Teaching/Trauma") selected @endif>Teaching/Trauma</option><option value='Level 1 Trauma' @if(isset($item) && $item['facility_type'] == "Level 1 Trauma") selected @endif>Level 1 Trauma</option><option value='Level 2 Trauma' @if(isset($item) && $item['facility_type'] == "Level 2 Trauma") selected @endif>Level 2 Trauma</option><option value='Level 3 Trauma' @if(isset($item) && $item['facility_type'] == "Level 3 Trauma") selected @endif>Level 3 Trauma</option><option value='Community' @if(isset($item) && $item['facility_type'] == "Community") selected @endif>Community</option><option value='Regional Med. Ctr.' @if(isset($item) && $item['facility_type'] == "Regional Med. Ctr.") selected @endif>Regional Med. Ctr.</option><option value='LTAC' @if(isset($item) && $item['facility_type'] == "LTAC") selected @endif>LTAC</option><option value='Home Health' @if(isset($item) && $item['facility_type'] == "Home Health") selected @endif>Home Health</option><option value='Hospice' @if(isset($item) && $item['facility_type'] == "Hospice") selected @endif>Hospice</option><option value='Outpatient Surgical Center' @if(isset($item) && $item['facility_type'] == "Outpatient Surgical Center") selected @endif>Outpatient Surgical Center</option>
                </select>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <label for="supervisor">Agency Name</label>
                <input type="text" name="employments[agency_name][{{ $counter or 0 }}]" id="agency_name" class="form-control" value="{{ $item['agency_name'] or "" }}"/>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <label for="employments_employment_type_{{ $counter or 0 }}">Employment Type</label>
                <select class="required form-control" name="employments[employment_type][{{ $counter or 0 }}]" id="employments_employment_type_{{ $counter or 0 }}">
                    <option value='' @if(isset($item) && $item['employment_type'] == "") selected @endif></option><option value='Full Time' @if(isset($item) && $item['employment_type'] == "Full Time") selected @endif>Full Time</option><option value='Travel' @if(isset($item) && $item['employment_type'] == "Travel") selected @endif>Travel</option><option value='Per Diem' @if(isset($item) && $item['employment_type'] == "Per Diem") selected @endif>Per Diem</option>
                </select>
            </div>
        </div>
        <div class="row margin_top_line_sm">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <label for="reason_of_leeaving">Reason for leaving:</label>
                <p >
                    You have <span style="font-weight: bold;" class="counter">250</span> characters remaining
                </p>
                <textarea onkeyup="countChar(this)" name="employments[reason_of_leaving][{{ $counter or 0 }}]" cols="30" rows="4" class="form-control">{{ $item['reason_of_leaving'] or "" }}</textarea>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <label for="days_description">Please also provide details if more than 30 days elapsed between employers:</label>
                <p >
                    You have <span style="font-weight: bold;" class="counter">250</span> characters remaining
                </p>
                <textarea onkeyup="countChar(this)" name="employments[days_description][{{ $counter or 0 }}]" cols="30" rows="4" class="form-control">{{ $item['days_description'] or "" }}</textarea>
            </div>
        </div>
        <div class="row margin_top_line">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!--<div class="margin_top_line_sm">
                    <label for="">Contact Reference</label>&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="employments[contact_reference][{{ $counter or 0 }}]" value="0" />
                    <input type="radio" name="employments[contact_reference][{{ $counter or 0 }}]" value="1" @if(isset($item) && $item['contact_reference']) checked="checked" @endif /> Yes <input type="radio" name="employments[contact_reference][{{ $counter or 0 }}]" value="0" @if(isset($item) && !$item['contact_reference']) checked="checked" @endif  /> No
                </div>
                <div class="margin_top_line_sm">
                    <label for="">Travel Assignment</label>&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="employments[travel][{{ $counter or 0 }}]" value="0" />
                    <input type="radio" name="employments[travel][{{ $counter or 0 }}]" value="1" @if(isset($item) && $item['travel']) checked="checked" @endif /> Yes <input type="radio" name="employments[travel][{{ $counter or 0 }}]" value="0" @if(isset($item) && !$item['travel']) checked="checked" @endif /> No
                </div>-->
                <div class="margin_top_line_sm">
                    <label for="">Charge Experience</label>&nbsp;&nbsp;&nbsp;
                    <input type="hidden" name="employments[charge_experience][{{ $counter or 0 }}]" value="0" />
                    <input type="radio" name="employments[charge_experience][{{ $counter or 0 }}]" value="1" @if(isset($item) && $item['charge_experience']) checked="checked" @endif /> Yes <input type="radio" name="employments[charge_experience][{{ $counter or 0 }}]" value="0" @if(isset($item) && !$item['charge_experience']) checked="checked" @endif /> No
                </div>
            </div>
        </div>
    </div>
</div>