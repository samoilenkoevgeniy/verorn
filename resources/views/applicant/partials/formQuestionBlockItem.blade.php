<?php
$count = $question->children()->count();
?>

<table style="padding:1%;">
    <tr @if($count == 0) class="req_radio" @endif>
        <td @if($count != 0) colspan="5" @endif style="width: {{ $count ? "98%" : 58-$count."%" }};    padding: 1%; {{ (isset($nested) && $nested != 0) ? "padding-left: ".$nested."%;" : "" }}">
            {{ $question->text }}
        </td>
        @if($count == 0)
            @if(!$question->yes_no && !$question->type)
                @for($i = "A"; $i < "E"; $i++)
                    <td style="width: 8%; padding: 1%;">
                        {{ $i }}&nbsp;&nbsp;<input type="radio" value="{{ $i }}" name="question_{{ $question->id }}" @if(isset($formProcess) && \App\FormProcessQuestion::whereFormProcessId($formProcess->id)->where("q_id", $question->id)->whereValue($i)->first()) checked @endif>&nbsp;&nbsp;
                    </td>
                @endfor
            @elseif($question->yes_no)
                <td style="width: 25%;" colspan="2">
                    Yes <input type="radio" value="yes" name="question_{{ $question->id }}" @if(isset($formProcess) && \App\FormProcessQuestion::whereFormProcessId($formProcess->id)->where("q_id", $question->id)->whereValue("yes")->first()) checked @endif />
                </td>
                <td style="width: 25%;" colspan="2">
                    No <input type="radio" value="no" name="question_{{ $question->id }}" @if(isset($formProcess) && \App\FormProcessQuestion::whereFormProcessId($formProcess->id)->where("q_id", $question->id)->whereValue("no")->first()) checked @endif />
                </td>
            @elseif($question->type)

            @endif
        @endif
    </tr>
    <tr>
        @foreach($question->children as $child)
            @include("applicant.partials.formQuestionBlockItem", [
                "question" => $child,
                "formProcess" => $formProcess,
                "nested" => $nested+2
            ])
        @endforeach
    </tr>
</table>