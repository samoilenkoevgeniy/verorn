@extends("layouts.app")

@section("content")
    <style>
        table{
            margin: 0;
            width: 100%;
        }
        .questions_block{
            padding:1%;
        }
    </style>
    <div class="container">
        <form action="{{ url("/form/process/".$form->id) }}" method="post" id="main_form">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        {{ $form->name }}
                    </strong>
                </div>
                <div class="panel-body">
                    {!! csrf_field() !!}
                    <p>
                        This profile is for use by <strong>{{ $form->name }}</strong> nurses with more than one year of experience in this specialty area.
                    </p>
                    <br />
                    <p>
                        Please enter your full legal name as it appears on your Social Security Card.
                    </p>
                    <br />
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label for="first_name">First name</label>
                            <input type="text" name="first_name" id="first_name" class="required form-control" value="{{ $formProcess->first_name or "" }}" />
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label for="last_name">Last name</label>
                            <input type="text" name="last_name" id="last_name" class="required form-control" value="{{ $formProcess->last_name or "" }}"/>
                        </div>
                    </div>
                    <div class="row margin_top_line">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
                            <label for="date">Date</label><br />
                            <input type="text" name="date" id="date" class="required form-control date" value="{{ $formProcess->date or "" }}"/>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email " class="required form-control" value="{{ $formProcess->email or "" }}"/>
                        </div>
                    </div>
                    <div class="row margin_top_line">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Please check the box/es below for each group in which you have provided age-appropriate care:
                                </div>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>
                                            A. Newborn/Neonate (birth-30days)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="A" @if(in_array("A", $appCare)) checked="checked" @endif />
                                        </td>
                                        <td>
                                            F. Adolescents (12-18 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="F" @if(in_array("F", $appCare)) checked="checked" @endif  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            B. Infant (30 days-1 year)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="B" @if(in_array("B", $appCare)) checked="checked" @endif  />
                                        </td>
                                        <td>
                                            G. Young adults (18-39 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="G" @if(in_array("G", $appCare)) checked="checked" @endif  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            C. Toddler (1-3 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="C" @if(in_array("C", $appCare)) checked="checked" @endif />
                                        </td>
                                        <td>
                                            H. Middle adults (39-64 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="H" @if(in_array("H", $appCare)) checked="checked" @endif  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            D. Preschooler (3-5 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="D" @if(in_array("D", $appCare)) checked="checked" @endif  />
                                        </td>
                                        <td>
                                            I. Older adults (64-79 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="I" @if(in_array("I", $appCare)) checked="checked" @endif  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            E. School age children (5-12 years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="E" @if(in_array("E", $appCare)) checked="checked" @endif  />
                                        </td>
                                        <td>
                                            J. Elderly adults (80+ years)
                                        </td>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="app_care[]" value="J" @if(in_array("J", $appCare)) checked="checked" @endif  />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row margin_top_line">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            Please indicate your level of experience by checking 1 box in each of the category below (1-less experience to 4-more experience):
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div>
                                A.   Limited or no recent experience
                            </div>
                            <div>
                                B.   Intermittent experience and may need review
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div>
                                C.   One-two years current experience
                            </div>
                            <div>
                                D.   Two-plus years of current experience, can function independently
                            </div>
                        </div>
                    </div>
                    <div class="row margin_top_line questions_block">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <?php
                        $right_ok = 0;
                        ?>
                        @foreach($questionBlocks as $questionBlock)
                            @if(!$questionBlock->left && $right_ok)
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            @endif
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <strong>{{ $questionBlock->name }}</strong>
                                    </div>
                                    <table class="table table-bordered table-hover" >
                                        <thead></thead>
                                        <tbody>
                                        @foreach($questionBlock->items as $question)
                                            @include("applicant.partials.formQuestionBlockItem", [
                                                "question" => $question,
                                                "formProcess" => $formProcess,
                                                "nested" => 0
                                            ])
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                                <?php
                                if($questionBlock->left == 0) {
                                    $right_ok = 1;
                                }
                                ?>
                        @endforeach
                    </div>
                </div>
            </div>

            </div>
            <div class="row" @if($form->experiences->count() == 0) style="display: none;" @endif>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            MY EXPERIENCE IS PRIMARILY IN (Please indicate number of years)
                        </div>
                        <div class="panel-body">
                            @foreach($form->experiences as $experience)
                                @if($experience->name)
                                    <?php
                                    if(isset($formProcess)) {
                                        $check = \App\FormProcessExperience::whereFormProcessId($formProcess->id)->whereExpId($experience->id)->first();
                                    }else {
                                        $check = false;
                                    }
                                    ?>

                                    <input type="hidden" name="experience[{{ $experience->id }}][exp_id]" value="{{ $experience->id }}" />
                                    <div class="row margin_top_line">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <input type="hidden" name="experience[{{ $experience->id }}][name]" value="0" />
                                            <input type="checkbox" name="experience[{{ $experience->id }}][name]" value="1" @if(isset($formProcess) && $check) checked @endif/> {{ $experience->name }}
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
                                            <input type="text" name="experience[{{ $experience->id }}][years]" value="{{ $check->years or "" }}" class="form-control"/> &nbsp;&nbsp; year(s)
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Confirmation
                        </div>
                        <div class="panel-body">
                            <p>
                                The information I have given is true and accurate to the best of my knowledge. By clicking the Submit button I hereby authorize Vero RN to release this <strong>{{ $form->name }}</strong> skills checklist to Vero RN-related facilities in consideration of my employment.
                            </p>
                        </div>
                    </div>
                    <div>
                        <input type="submit" value="Save & Submit" class="btn btn-primary" />
                    </div>
                    <br />
                </div>
            </div>

        </form>
    </div>
@endsection

@section("top_nav")
    <li>
        <a class="hvr-bounce-to-right" href="/forms">< Back To Skills Checklists</a>
    </li>
@endsection

@section("top_nav_style")
    style="position: fixed; z-index: 10000;"
@endsection

@section("scripts")
    <script src="{{ url("/js/libs/masonry.pkgd.min.js") }}"></script>
    <script src="{{ url("/js/libs/jquery.maskedinput.js") }}"></script>
    <script src="{{ url("/js/libs/moment.js") }}"></script>
    <script src="{{ url("/js/libs/combodate.js") }}"></script>
    <script src="{{ url("/js/application_front.js") }}"></script>
    <script>
        //$('.questions_block').masonry()
    </script>
@endsection