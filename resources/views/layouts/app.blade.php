<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        VeroRN
    </title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    {{--
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> --}}
    <!--<link href="{{ asset("/css/bootstrap.min.css") }}" rel="stylesheet">
    -->
    <link href="{{ asset("/css/default.css") }}" rel="stylesheet">
    <link href="{{ asset("/css/bootstrap.min.css") }}" rel="stylesheet">
    <link href="{{ asset("/css/style.css") }}" rel="stylesheet">
    <link href="{{ asset("/css/style_custom.css") }}" rel="stylesheet">
    <link href="{{ asset("/css/mobile.css") }}" rel="stylesheet">
    <link href="{{ asset("/css/font.css") }}" rel="stylesheet">
    <link href="{{ asset("/css/hover.css") }}" rel="stylesheet">
    <link href="{{ asset("/css/style_sorter.css") }}" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <!--<nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image
                <a class="navbar-brand" href="{{ url('/') }}">
                    VeroRN
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Home</a></li>
                    @if(!Auth::guest() && (Auth::user()->role == "administrator" || Auth::user()->role == "recruiter"))
                        <li><a href="{{ url('/candidates') }}">Candidates</a></li>
                    @endif
                    @if(!Auth::guest() && Auth::user()->role == "administrator" )
                        <li><a href="{{ url('/recruiters') }}">Recruiters</a></li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar --
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links --
                    @if(ENV("SHOW_NAV", true))
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    @if(!Auth::guest() && Auth::user()->role == "applicant" )
                                        <li><a href="{{ url('/edit') }}">Edit profile</a></li>
                                    @endif
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                </ul>
                            </li>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
    </nav>-->
    <div class="header_top" @yield("top_nav_style")>
        <div class="container">
            <div class="header_top_left">
                <div class="header_top_left_nav">
                    <ul>
                        <li>
                            <a href="/" class="logo"><img src="/images/VeroRNLogoFinal_logo.png"></a>
                        </li>
                        @yield("top_nav")
                    </ul>
                </div>
            </div>
            <div class="header_top_right">

                <div class="header_top_right_nav">
                    <ul>
                        <li>
                            @if(\Auth::guest())
                                <a class="hvr-bounce-to-right" href="/login">Login</a>
                            @else
                                <a class="hvr-bounce-to-right" href="/home">Hello, {{ Auth::user()->name }}!</a>
                            @endif
                        </li>
                        @if(\Auth::guest())
                            <li><a class="hvr-bounce-to-right" href="{{ url("/register") }}">Create Profile</a></li>
                        @endif

                    </ul>


                </div>
                <p>Ready to Travel? APPLY NOW!<span> 877-293-3670</span></p>



            </div>


        </div>


    </div>
    <!--
    <div class="nav_full">
        <div class="container">
            <a class="logo" href="#"><img src="/images/logo.jpg" /></a>
            <div class="nav">
                <label for="show-menu" class="show-menu"> Menu</label>
                <input type="checkbox" id="show-menu" role="button">
                <ul id="menu">
                    <li><a href="{{ url("/about") }}">About Us</a></li>
                    <li><a href="{{ url("/why_vero_rn") }}">Why Vero RN</a></li>
                    <li><a href="#">Why Travel  </a></li>
                    <li><a href="{{ url("/avilable_job") }}">Available Jobs </a></li>
                    <li><a href="{{ url("/hospitals_facilities") }}"> For Hospitals/Facilities</a></li>
                    <li><a href="{{ url("/referer_a_friend") }}">  Refer a Friend </a></li>
                    <li><a href="{{ url("/contact_us") }}">  Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>-->

    <div style="padding-top: 70px;"></div>
    @yield('content')


    <div class="footer">
        <div class="container">

            <div class="footer_left">
                <h1>QUICK LINKS</h1>
                <div class="footer_left_left">
                    <ul>
                        <li><a href="http://verorn.com/terms-condition.html">Terms of Use</a></li>
                        <li><a href="http://verorn.com/privecy-policy.html">Privacy Policy</a></li>
                        <li><a href="http://verorn.com/hospitals-facilities.html">For Employers </a></li>
                        <li><a href="http://verornportal.com/createprofile">Login Job Seeker</a></li>

                    </ul>



                </div>
                <div class="footer_left_left">
                    <ul>
                        <li><a href="mailto:careers@verorn.com">Careers</a></li>
                        <li><a href="#">Testimonial & Reviews</a></li>


                    </ul>



                </div>
            </div>

            <div class="footer_second">
                <h1>Referral Bonus</h1>
                <p>Make Money By Telling Your Friends</p>
                <a class="hvr-bounce-to-right" href="http://verorn.com/refer-friend.html">GET PAID TO REFER TODAY</a>

            </div>
            <div class="folow_us">
                <h1>Follow Us </h1>
                <ul>
                    <li><a target="_blank" href="https://www.facebook.com/Vero-RN-1712075832396761/?fref=ts"><img src="/images/fb.jpg"  /></a></li>
                    <li><a target="_blank" href="https://twitter.com/VeroRNTravel"><img src="/images/twitter.jpg"  /></a></li>
                    <li><a target="_blank" href="https://www.youtube.com/channel/UC4Lhb3HbAJk1ZIwiznZdyLA"><img src="/images/you-tube.jpg"  /></a></li>

                </ul>
            </div>
            <div class="footer_second_new">
                <p>© Copyright 2016 Vero RN, LLC – Equal Opportunity Employer</p>
            </div>
        </div>
    </div>

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script>
        $(window).ready(function(){
            $(".dropdown-toggle").click(function(){
                var that = $(this),
                        next = that.next();
                if(next.css("display") == "none") {
                    next.show();
                } else {
                    next.hide();
                }
            });
        })
    </script>
    @yield("scripts")

    <div class="modal fade" id="chooseRecruiter" tabindex="-1" role="dialog" aria-labelledby="chooseRecruiterLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Choose a recruiter</h4>
                </div>
                <form action="{{ url("/addRecruiterFromModal") }}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="user_id"></label>
                            <select name="user_id" id="user_id" class="form-control">
                                <option value="NULL">---</option>
                                @foreach(\App\User::whereRole("recruiter")->get() as $recruiter)
                                    <option value="{{ $recruiter->id }}">{{ $recruiter->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
