@extends("layouts.app")

@section("content")
    <div class="container" style="height: 250px;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1>Page Not Found!</h1>
            <p>
                Go to <a href="{{ url("/home") }}">Home page</a>
            </p>
        </div>
    </div>
@endsection