<h3>Hello!</h3>

<div>
    <p>
        Your candidate {{ $user->name . " " .$user->last_name }} has submitted the Job Application.
    </p>
    <p>
        <a href="{{ url(ENV("APP_BASE_HREF")."app/show/".$app->id) }}" target="_blank">Click here to view the form.</a>
    </p>
    <p>
        Keep up the great work,
    </p>
    <p>
        Vero RN Team
    </p>
</div>