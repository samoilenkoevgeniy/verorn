<h3>Hello!</h3>

<p>
    You have a new candidate!
</p>

<p>
    Email: {{ $candidate['email'] }}
</p>

<p>
    Name: {{ $candidate['name'] . " " . $candidate['last_name'] }}
</p>
<p>
    Country: {{ $candidate['country'] }}
</p>
<p>
    State: {{ \App\CountryState::find($candidate['country_state_id'])->text }}
</p>
<p>
    Profession: {{ \App\Profession::find($candidate['profession_id'])->text}}
</p>
<p>
    Speciality: {{ \App\Speciality::find($candidate['speciality_id'])->text}}
</p>

<p>
    Keep up the great work,
</p>

<p>
    Vero RN Team
</p>