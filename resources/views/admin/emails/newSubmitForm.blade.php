<h3>Hello!</h3>

<div>
    <p>
        Your candidate {{ $user->name . " " .$user->last_name }} has submitted the {{ $form->sourceForm->name }}.
    </p>
    <p>
        <a href="{{ url(ENV("APP_BASE_HREF")."app/form/show/".$form->id) }}" target="_blank">Click here to view the form.</a>
    </p>
    <p>
        Keep up the great work,
    </p>
    <p>
        Vero RN Team
    </p>
</div>