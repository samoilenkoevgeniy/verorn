@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url("/home") }}">Dashboard</a></li>
                    <li class="list-group-item active"><a style="color: #fff;" href="{{ url("/candidates") }}">Сandidates</a></li>
                    <li class="list-group-item"><a href="{{ url("/recruiters") }}">Recruiters</a></li>
                    <li class="list-group-item"><a href="{{ url("/logout") }}">Log out</a></li>
                </ul>
            </div>
            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-8">
                @if(\Session("message"))
                    <div class="alert alert-success">
                        {{ \Session("message") }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        User applications
                    </div>
                    <div class="panel-body">
                        @if($jobApplication)
                            <li class="list-group-item"><a href="{{ url("/app/show/".$jobApplication->id) }}">Application</a></li>
                        @endif
                        @if($forms)
                            @foreach($forms as $form)
                                <li class="list-group-item"><a href="{{ url("/app/form/show/".$form->id) }}">{{ $form->sourceForm->name }}</a></li>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection