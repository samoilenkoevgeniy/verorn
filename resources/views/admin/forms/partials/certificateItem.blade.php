<div class="form-group">
    <label>Cert # {{ $certificate->id }}</label>
    <input data-field="name" data-certificate-id="{{ $certificate->id }}" type="text" class="form-control ajax_sync_cert" value="{{ $certificate->name }}" />
    long text <input data-certificate-id="{{ $certificate->id }}"  data-field="text" type="checkbox" class="ajax_sync_cert_checkbox" @if($certificate->text) checked @endif>
</div>