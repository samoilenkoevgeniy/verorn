<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                <label>question #{{ $question->id }}</label>
                <input data-field="text" data-question-block-item-id="{{ $question->id }}" type="text" class="form-control ajax_sync_question" value="{{ $question->text }}" />
            </div>
            <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        Yes/No
                        <input type="checkbox" data-question-block-item-id="{{ $question->id }}" data-field="yes_no" class="ajax_sync_question_checkbox" @if($question->yes_no) checked @endif/>
                        Type
                        <input type="checkbox" data-question-block-item-id="{{ $question->id }}" data-field="type" class="ajax_sync_question_checkbox"  @if($question->type) checked @endif>
                        Other
                        <input type="checkbox" data-question-block-item-id="{{ $question->id }}" data-field="other" class="ajax_sync_question_checkbox"  @if($question->other) checked @endif>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div  data-question-block-id="{{ $question->block_id }}" data-parent-id="{{ $question->id }}" style="margin-top: 25px;" class="btn btn-success btn-sm addQuestionItem">Add parent</div>
                    </div>
                    <div class="col-lg-5 col-md-5">
                        <div  data-question-block-item-id="{{ $question->id }}" data-parent-id="{{ $question->id }}" style="margin-top: 25px;" class="btn btn-danger btn-sm removeQuestionItem">Remove parent</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-offset-1 col-md-offset-1 col-lg-11 col-md-11 questions_{{ $question->id }}">
                    @foreach($question->children as $child)
                        @include("admin.forms.partials.questionBlockItemQuestion", [
                            "question" => $child
                        ])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>