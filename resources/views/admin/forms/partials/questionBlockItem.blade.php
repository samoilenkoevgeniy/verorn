<div class="panel panel-default">
    <div class="panel-heading">
        <div class="pull-left">
            Question block #{{ $questionBlock->id }}
        </div>
        <div class="pull-right">
            <div class="btn btn-danger deleteQuestionBlock" data-question-block-id="{{ $questionBlock->id }}">delete this block</div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label>Name</label>
            <input data-field="name" data-question-block-id="{{ $questionBlock->id }}" type="text" class="form-control ajax_sync" value="{{ $questionBlock->name }}" />
        </div>
        <div class="form-group">
            <label>Initials</label>
            <input data-field="initials" data-question-block-id="{{ $questionBlock->id }}" type="checkbox" class="ajax_sync_checkbox" value="1" {{ $questionBlock->initials ? "checked" : "" }}/>
        </div>
        @foreach($questionBlock->items as $question)
            @include("admin.forms.partials.questionBlockItemQuestion", compact("question"))
        @endforeach
        <div data-question-block-id="{{ $questionBlock->id }}" class="btn btn-warning addQuestionItem">Add question</div>
    </div>
</div>