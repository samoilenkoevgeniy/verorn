@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                @if(\Session("message"))
                    <div class="alert alert-success">
                        {{ \Session("message") }}
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit form {{ $form->name }}
                    </div>
                    <div class="panel-body">
                        <form action="{{ url("/forms/update/".$form->id) }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" value="{{ $form->name }}"/>
                            </div>
                            <div class="form-group">
                                <label for="header">Header</label>
                                <textarea class="form-control" name="header" id="header" cols="30" rows="10">{{ $form->header }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="age_specific_block">age_specific_block</label>
                                <input type="checkbox" name="age_specific_block" id="age_specific_block" value="1" {{ $form->age_specific_block ? "checked" : "" }}/>
                            </div>
                            <div class="form-group">
                                <label for="primary_experience_block">primary_experience_block</label>
                                <input type="checkbox" name="primary_experience_block" id="primary_experience_block" value="1" {{ $form->primary_experience_block ? "checked" : "" }}/>
                            </div>
                            <div class="form-group">
                                <label for="certification_block">certification_block</label>
                                <input type="checkbox" name="certification_block" id="certification_block" value="1" {{ $form->certification_block ? "checked" : "" }}/>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Question Blocks
                                </div>
                                <div class="panel-body">
                                    <div id="question_blocks">
                                        @foreach($form->questionBlocks as $questionBlock)
                                            @include("admin.forms.partials.questionBlockItem", compact("questionBlock"))
                                        @endforeach
                                    </div>
                                    <div class="btn btn-primary" id="addQuestionBlock" data-form-id="{{ $form->id }}">Add Question Block</div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Certifications blocks
                                </div>
                                <div class="panel-body">
                                    @foreach($form->certificates as $certificate)
                                        @include("admin.forms.partials.certificateItem", compact("certificate"))
                                    @endforeach
                                    <div class="btn btn-primary" id="addCertificateBlock" data-form-id="{{ $form->id }}">Add Certificate</div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Experiences
                                </div>
                                <div class="panel-body">
                                    @foreach($form->experiences as $experience)
                                        @include("admin.forms.partials.experienceItem", compact("experience"))
                                    @endforeach
                                    <div class="btn btn-primary" id="addExperience" data-form-id="{{ $form->id }}">Add Experience</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Update form" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script src="{{ url("/js/forms.js") }}"></script>
@endsection