@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                @if(\Session("message"))
                    <div class="alert alert-success">
                        {{ \Session("message") }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Forms
                    </div>
                    <div class="panel-body">
                        <form action="{{ url("/forms/update/0") }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" />
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="add form" />
                            </div>
                        </form>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Forms
                    </div>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Form</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($forms as $form)
                            <tr>
                                <td>{{ $form->id }}</td>
                                <td>{{ $form->name }}</td>
                                <td><a href="{{ url("/forms/edit/".$form->id) }}">edit</a></td>
                                <td><a href="{{ url("/forms/delete/".$form->id) }}">Delete!</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script src="{{ url("/js/candidates.js") }}"></script>
@endsection