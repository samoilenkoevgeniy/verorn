@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                @if(\Session("message"))
                    <div class="alert alert-success">
                        {{ \Session("message") }}
                    </div>
                @endif
            </div>

            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url("/home") }}">Dashboard</a></li>
                    <li class="list-group-item"><a href="{{ url("/edit") }}">Edit Profile</a></li>
                    <li class="list-group-item active"><a style="color: #fff;" href="{{ url("/candidates") }}">Сandidates</a></li>
                    <li class="list-group-item"><a href="{{ url("/recruiters") }}">Recruiters</a></li>
                    <li class="list-group-item"><a href="{{ url("/logout") }}">Log out</a></li>
                </ul>
            </div>
            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Candidates
                    </div>
                    <div class="panel-body">
                        <form action="{{ url("/candidatesSearch") }}" method="post">
                            {!! csrf_field() !!}
                            <h4>Search Bar</h4>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="searchName">Name</label>
                                    <input type="text" class="form-control" id="searchName" name="searchName" placeholder="Jane Doe" value="{{ $searchName or "" }}" />
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="searchEmail">Email</label>
                                    <input type="text" class="form-control" id="searchEmail" name="searchEmail" placeholder="JaneDoe@gmail.com" value="{{ $searchEmail or "" }}" >
                                </div>
                            </div>
                            @if(\Auth::user()->role == "administrator")
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <label for="recruiter_id">Recruiter</label>
                                        <select name="recruiter_id" id="recruiter_id" class="form-control">
                                            <option value="0">---</option>
                                            <option value="no_recruiter" @if(isset($recruiter_id) && $recruiter_id=="no_recruiter") selected @endif>No Recruiter</option>
                                            @foreach(App\User::where("role", "recruiter")->get() as $user)
                                                <option value="{{ $user->id }}" @if(isset($recruiter_id) && $recruiter_id==$user->id) selected @endif>{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="application">Form complete</label>
                                    <div>
                                        <input type="checkbox" name="application" value="1" @if(isset($application) && $application) checked @endif/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="submit" value="Apply filters" class="btn btn-primary btn-sm">
                            </div>
                        </form>
                    </div>
                    <table class="table table-hover tablesorter" id="usersTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th style="cursor:pointer;">Created on:</th>
                            <th>Recruiter</th>
                            <th>Forms</th>
                            <th>Reset Password</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{!! $user->recruiter ? "<a href='#' data-user-id='".$user->id."' class='setRecruiter'>". $user->recruiter->name ."</a>" : "<a href='#' data-user-id='".$user->id."' class='setRecruiter'>No recruiter</a>" !!}</td>
                                <td>
                                    <?php
                                    $app = App\JobApplication::where("user_id", $user->id)->get()->last();
                                    $forms = \App\FormProcess::whereUserId($user->id)->get()->last();
                                    ?>
                                    {!! ($app || $forms) ? "<a href='/candidates/show/".$user->id."'>Completed Forms</a>" : "Candidate has not completed forms" !!}
                                </td>
                                <td>
                                    <form action="{{ url("/candidates/resetPassword/".$user->id) }}" method="post" >
                                        <div class="fields" style="display: none;">
                                            {!! csrf_field() !!}
                                            <input type="text" name="password" class="form-control" style="margin-bottom: 10px;" />
                                            <input type="submit" value="Reset" class="btn btn-sm btn-success">
                                        </div>
                                        <div class="button">
                                            <a href="#" class="btn btn-primary btn-sm reset_password">reset password</a>
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <a href="{{ url("/user/delete/".$user->id) }}" class="btn btn-danger btn-sm">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="panel-body">
                        {!! $users->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script src="{{ url("/js/global.js") }}"></script>
<script src="{{ url("/js/jquery.tablesorter.pager.js") }}"></script>
<script src="{{ url("/js/candidates.js") }}"></script>
@endsection