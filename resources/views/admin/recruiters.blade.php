@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                @if(\Session("message"))
                    <div class="alert alert-success">
                        {{ \Session("message") }}
                    </div>
                @endif
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url("/home") }}">Dashboard</a></li>
                    <li class="list-group-item"><a href="{{ url("/edit") }}">Edit Profile</a></li>
                    <li class="list-group-item"><a href="{{ url("/candidates") }}">Сandidates</a></li>
                    <li class="list-group-item active"><a style="color: #fff;" href="{{ url("/recruiters") }}">Recruiters</a></li>
                    <li class="list-group-item"><a href="{{ url("/logout") }}">Log out</a></li>
                </ul>
            </div>
            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Recruiters
                    </div>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Created on:</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>
                                    <a href="{{ url("/recruiters/edit/".$user->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ url("/candidates/resetPassword/".$user->id) }}" method="post" >
                                        <div class="fields" style="display: none;">
                                            {!! csrf_field() !!}
                                            <input type="text" name="password" class="form-control" style="margin-bottom: 10px;" />
                                            <input type="submit" value="Reset" class="btn btn-sm btn-success">
                                        </div>
                                        <div class="button">
                                            <a href="#" class="btn btn-primary btn-sm reset_password">reset password</a>
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <a href="{{ url("/user/delete/".$user->id) }}" class="btn btn-danger btn-sm">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="panel-body">
                        <h3>Add a recruiter</h3>
                        <form class="form-horizontal" role="form" action="{{ url("/addRecruiter") }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i>Add Recruiter
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script src="{{ url("/js/global.js") }}"></script>
@endsection