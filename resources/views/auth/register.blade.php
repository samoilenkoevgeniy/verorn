@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                @if(\Session("error_referral"))
                    <div class="alert alert-danger">
                        {{ \Session("error_referral") }}
                    </div>
                @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
            </div>
        </div>
        <div class="avilable_job_inner">
            <h1>Create your Profile. </h1>
            <p>
                All applicants are asked to create their own login so the information on your application can be updated easily.<br />Please note: your password should be 8+ characters long.
            </p>
            <div class="contact_us_form">
                <div class="contact_us_form_inner">
                    <div class="contact_us_form_inner_main">
                        <form method="POST" action="{{ url('/register') }}" id="register_form">
                            {!! csrf_field() !!}
                            <div class="contact_us_form_inner_main_upper">
                                <label>Email Address :</label>
                                <input type="text" name="email" placeholder="Enter your Email Address" />
                            </div>

                            <div class="contact_us_form_inner_main_upper">
                                <label>Password:</label>
                                <input type="password" name="password" placeholder="Enter your password" />
                            </div>

                            <div class="contact_us_form_inner_main_upper">
                                <label>Password Confirm:</label>
                                <input type="password" name="password_confirmation" placeholder="Enter your password again" />
                            </div>

                            <div class="contact_us_form_inner_main_upper">
                                <label>First Name:</label>
                                <input type="text" name="name" placeholder="Enter your first name" />
                            </div>

                            <div class="contact_us_form_inner_main_upper">
                                <label>Last Name:</label>
                                <input type="text" name="last_name" placeholder="Enter your last name" />
                            </div>

                            <div class="contact_us_form_inner_main_upper">
                                <label>Phone :</label>
                                <input type="text" name="phone" placeholder="Enter your phone" />
                            </div>

                            <div class="contact_us_form_inner_main_upper">
                                <label>Country :</label>
                                <select name="country" id="country">
                                    <option value="0">Select a country</option>
                                    <option value="US">US</option>
                                    <option value="Canada">Canada</option>
                                </select>
                            </div>

                            <div class="contact_us_form_inner_main_upper">
                                <label>State:</label>
                                <select disabled id="cap_select">
                                    <option value="0">First Please select your country</option>
                                </select>
                                <select name="country_state_id" id="state_id" style="display: none">
                                    <option value="0">---</option>
                                    @foreach(\App\CountryState::whereCountry("US")->get() as $item)
                                        <option value="{{ $item->id }}">{{ $item->text }}</option>
                                    @endforeach
                                </select>

                                <select name="country_state_id" id="state_id_canada" style="display: none">
                                    <option value="0">---</option>
                                    @foreach(\App\CountryState::whereCountry("Canada")->get() as $item)
                                        <option value="{{ $item->id }}">{{ $item->text }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="contact_us_form_inner_main_upper">
                                <label>Profession:</label>
                                <select name="profession_id" id="profession_id">
                                    <option value="0">Select a profession</option>
                                    @foreach(\App\Profession::all() as $item)
                                        <option value="{{ $item->id }}">{{ $item->text }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="contact_us_form_inner_main_upper">
                                <label>Specialty:</label>
                                <select name="speciality_id" id="specialty_id">
                                    <option value="0">---</option>
                                </select>
                            </div>

                            <div class="contact_us_form_inner_main_upper">
                                <label>Years:</label>
                                <select name="years" id="years">
                                    @for($i = 0; $i < 26; $i++)
                                        <option value="{{ $i }}">{{ $i }} years</option>
                                    @endfor
                                </select>
                            </div>

                            <input type="submit" value="submit" />

                        </form>
                </div>



            </div>


        </div>

        <div class="add"><img src="images/add.png" /></div>

    </div>



</div>
    <!--
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    @if(\Session("error_referral"))
                        <div class="alert alert-danger">
                            {{ \Session("error_referral") }}
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('speciality') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Speciality</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="speciality">

                                @if ($errors->has('speciality'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('speciality') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('referral_email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Who is my Recruiter</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="referral_email">

                                @if ($errors->has('referral_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('referral_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
@endsection

@section("scripts")
    <script src="/js/registry.js"></script>
@endsection
