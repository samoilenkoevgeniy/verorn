<h3>
    Hello, {{ $user->name }}!
</h3>
<p>
    Your password has been reset by Corporate.
</p>
<p>
    Password: {{ $password }}
</p>

<p>
    Keep up the great work,
</p>
<p>
    Vero RN Team
</p>