@extends('layouts.app')

@section('content')
<div class="container">
    @if(Auth::user()->role == "administrator")
        <div class="row">
            <div class="col-lg-4 col-md-4 col-xs-4 col-sm-4">
                <ul class="list-group">
                    <li class="list-group-item active"><a style="color: #fff;" href="{{ url("/home") }}">Dashboard</a></li>
                    <li class="list-group-item"><a href="{{ url("/edit") }}">Edit Profile</a></li>
                    <li class="list-group-item"><a href="{{ url("/candidates") }}">Сandidates</a></li>
                    <li class="list-group-item"><a href="{{ url("/recruiters") }}">Recruiters</a></li>
                    <li class="list-group-item"><a href="{{ url("/logout") }}">Log out</a></li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <h2>Recruiters Statistics</h2>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Recruiter</th>
                                <th>Candidates Registered</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(App\User::where("role", "recruiter")->get() as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->candidates->count() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <h2>Candidates Statistics</h2>
                        <h4>{{ App\User::where("role", "applicant")->get()->count() }} total candidates</h4>
                        <h4>{{App\JobApplication::all()->count() }} Total applications</h4>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(Auth::user()->role == "recruiter")
        <div class="row">
            <div class="col-lg-4 col-md-4 col-xs-6 col-sm-6">
                <ul class="list-group">
                    <li class="list-group-item active"><a style="color: #ffffff;" href="{{ url("/Home") }}">Dashboard</a></li>
                    <li class="list-group-item"><a href="{{ url("/edit") }}">Edit Profile</a></li>
                    <li class="list-group-item"><a href="{{ url("/logout") }}">Log out</a></li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <form action="{{ url("/candidatesSearch") }}" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="redirect" value="{{ url("/home") }}" />
                            <h4>Search Bar</h4>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-6">
                                <div class="form-group">
                                    <label for="searchName">Name</label>
                                    <input type="text" class="form-control" id="searchName" name="searchName" placeholder="Jane Doe" value="{{ $searchName or "" }}" />
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-6">
                                <div class="form-group">
                                    <label for="searchEmail">Email</label>
                                    <input type="text" class="form-control" id="searchEmail" name="searchEmail" placeholder="JaneDoe@gmail.com" value="{{ $searchEmail or "" }}" >
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="submit" value="Apply filters" class="btn btn-primary btn-sm">
                            </div>
                        </form>
                    </div>
                        <h3>List of your candidates</h3>
                        <table class="table table-hover tablesorterrecruiters" id="usersTable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Account Creation Date</th>
                                <th>Forms</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>
                                        <?php
                                        $app = App\JobApplication::where("user_id", $user->id)->get()->last();
                                        $forms = \App\FormProcess::whereUserId($user->id)->get()->last();
                                        ?>
                                        {!! ($app || $forms) ? "<a href='/candidates/show/".$user->id."'>Completed Forms</a>" : "Candidate has not completed forms" !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    <div class="panel-body">
                        {!! $users->render() !!}
                    </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(Auth::user()->role == "applicant")
        <div class="row">
            <div class="col lg-12 col-md-12 col-xs-12 col-sm-12">
                @if(\Session("message"))
                    <div class="alert alert-success">
                        {{ \Session("message") }}
                    </div>
                @endif
            </div>
            <div class="col-lg-4 col-md-4 col-xs-6 col-sm-6">
                <ul class="list-group">
                    <li class="list-group-item active"><a style="color: #fff;" href="{{ url("/home") }}"><i class="fa fa-btn fa-home"></i>Dashboard</a></li>
                    <li class="list-group-item"><a href="{{ url("/edit") }}"><i class="fa fa-btn fa-user"></i> Edit Profile</a></li>
                    <li class="list-group-item"><a href="{{ url("/applicant/application") }}"><i class="fa fa-btn fa-tasks"></i>Job Application {{ \App\JobApplication::isFilled(Auth::user()->id) ? " (Completed)" : "" }}</a></li>
                    <li class="list-group-item"><a href="{{ url("/forms") }}"><i class="fa fa-btn fa-align-left"></i>Skills Checklists</a></li>
                    <li class="list-group-item">{!! Cache::get("who_is_my_recruiter_".Auth::user()->id) !!}</li>
                    <li class="list-group-item"><a href="{{ url("/logout") }}">Log Out</a></li>
                </ul>
            </div>
        </div>
    @endif
</div>
@endsection


@section("scripts")
    <script src="{{ url("/js/global.js") }}"></script>
    <script src="{{ url("/js/jquery.tablesorter.pager.js") }}"></script>
    <script src="{{ url("/js/candidates.js") }}"></script>
@endsection
