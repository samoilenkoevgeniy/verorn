@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <h2>
                    Edit Candidate <strong>{{ $jobApplication->user->name }}</strong>
                </h2>
                <div>
                    <h3>Question 1:</h3>
                    <p>{{ $jobApplication->question_1 }}</p>
                </div>
                <div>
                    <h3>Question 2:</h3>
                    <p>{{ $jobApplication->question_2 }}</p>
                </div>
                <div>
                    <h3>Question 3:</h3>
                    <p>{{ $jobApplication->question_3 }}</p>
                </div>
                <div>
                    <h4>Application created at {{ $jobApplication->created_at }}</h4>
                    <h4>Application updates at {{ $jobApplication->updated_at }}</h4>
                </div>
                <div>
                    @if(Auth::user()->role == "administrator")
                        <a href="{{ url("/app/edit/".$jobApplication->id) }}" class="btn btn-success">save app</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection