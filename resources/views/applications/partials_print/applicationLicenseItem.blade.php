<tr class="first line_sm">
    <td>
        {{ $item->state }}
    </td>
    <td>
        {{ $item->number }}
    </td>
    <td>
        {{ $item->expiration_date }}
    </td>
    <td>
        {{ $item->is_active ? "Active" : "No Active" }}
    </td>
</tr>
<tr class="second">
    <td>State</td>
    <td>Number</td>
    <td>Expiration Date</td>
    <td></td>
</tr>