<?php
$count = $question->children()->count();
?>

<tr>
    <td @if($count != 0) colspan="5" @endif style="width: {{ $count ? "80%" : "80%" }}; {{ (isset($nested) && $nested != 0) ? "padding-left: ".$nested."0px;" : "" }}">
        {{ $question->text }}
    </td>
    @if($count == 0)
        @if(!$question->yes_no && !$question->type)
            @for($i = "A"; $i < "E"; $i++)
                <td style="width: 1%; text-align: center;">
                    <input type="radio" value="{{ $i }}" name="question_{{ $question->id }}" @if(\App\FormProcessQuestion::whereFormProcessId($formProcess->id)->where("q_id", $question->id)->whereValue($i)->first()) checked="checked" @endif >
                </td>
            @endfor
        {{--@elseif($question->yes_no)
            <td style="width: 25%;">
                Yes <input type="radio" value="yes" name="question_{{ $question->id }}" @if(isset($formProcess) && \App\FormProcessQuestion::whereFormProcessId($formProcess->id)->where("q_id", $question->id)->whereValue("yes")->first()) checked="checked" @endif />
            </td>
            <td style="width: 25%;">
                No <input type="radio" value="no" name="question_{{ $question->id }}" @if(isset($formProcess) && \App\FormProcessQuestion::whereFormProcessId($formProcess->id)->where("q_id", $question->id)->whereValue("no")->first()) checked="checked" @endif />
            </td>
        @elseif($question->type)--}}

        @endif
    @endif
</tr>
<tr>
    @foreach($question->children as $child)
        @include("applications.partials_print.formQuestionBlockItem", [
            "question" => $child,
            "formProcess" => $formProcess,
            "nested" => $nested+2
        ])
    @endforeach
</tr>