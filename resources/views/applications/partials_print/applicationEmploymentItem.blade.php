<table class="item_employment" cellpadding="0" cellspacing="0" @if($key != 0) style="width: 100%; border-top: 4px solid #000000; margin-top: 30px;padding-top: 15px;" @endif>
    <tr>
        <th colspan="4"  style="text-align: left"><h3>Employment #{{ $key+1 }}</h3></th>
    </tr>
    <tr class="first line_sm">
        <td colspan="4">
            {{ $item->facility }}
        </td>
    </tr>
    <tr class="second">
        <td colspan="4">Facility/Employer</td>
    </tr>
    <tr class="first line_sm">
        <td>{{ $item->number_of_beds_facility }}</td>
        <td >{{ $item->city_state }} {{ $item->facility_state }}</td>
        <td >{{ $item->unit }}</td>
        <td >{{ $item->no_of_beds_unit }}</td>
    </tr>
    <tr class="second">
        <td># of Beds/Facility</td>
        <td>City\State</td>
        <td>Unit</td>
        <td>No. of Beds/Unit</td>
    </tr>
    <tr class="first line_sm">
        <td>{{ $item->supervisor }}</td>
        <td>{{ $item->supervisors_title }}</td>
        <td>{{ $item->phone }}</td>
        <td>{{ $item->avg_pat_ratio_unit }}</td>
    </tr>
    <tr class="second">
        <td>Supervisor</td>
        <td>Supervisor's Title</td>
        <td>Supervisor's Phone</td>
        <td>Avg. Pat. Ratio/Unit</td>
    </tr>
    <tr class="first line_sm">
        <td >{{ $item->position_held }}</td >
        <td>{{ $item->unit_description }}</td>
        <td >{{ $item->date_start }}</td>
        <td>{{ $item->present ? "Present" : $item->date_end }}</td>
    </tr>
    <tr class="second">
        <td >Position Held</td>
        <td >Unit Description</td>
        <td >Date Start</td>
        <td>Date End</td>
    </tr>
    <tr class="first line_sm">
        <td>{{ $item->facility_type }}</td>
        <td>{{ $item->agency_name }}</td>
        <td colspan="2">{{ $item->employment_type }}</td>
    </tr>
    <tr class="second">
        <td>Facility Type</td>
        <td>Agency Name</td>
        <td colspan="2">Employment Type</td>
    </tr>
    <tr class="first line_sm">
        <!--<td>{{ $item->contact_reference ? "Yes" : "No" }}</td>
        <td>{{ $item->travel ? "Yes" : "No" }}</td>-->
        <td colspan="4">{{ $item->charge_experience ? "Yes" : "No" }}</td>
    </tr>
    <tr class="second">
        <!--<td>Contact Reference</td>
        <td>Travel Assignment</td>-->
        <td colspan="4">Charge Experience</td>
    </tr>
    <tr class="first line_sm">
        <td colspan="4">{{ $item->reason_of_leaving }}</td>
    </tr>
    <tr class="second">
        <td colspan="4">Reason for leaving</td>
    </tr>
    <tr class="first line_sm">
        <td colspan="4">{{ $item->days_description }}</td>
    </tr>
    <tr class="second">
        <td colspan="4">Please also provide details if more than 30 days elapsed between employers</td>
    </tr>
</table>