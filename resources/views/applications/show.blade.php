@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                @if(\Session("message"))
                    <div class="alert alert-success">
                        {{ \Session("message") }}
                    </div>
                @endif
                <h3>Application <strong>#{{ $application->id }}</strong> from <strong>{{ $application->user->name }}</strong></h3>
                <form method="post" action="{{ url("/job_application/process") }}" style="position:relative;">
                    <div class="wrapper_for_buttons" style="position:relative;">
                        <div class="wrapper_for_button">
                            <input type="submit" value="apply changes" class="btn btn-success" />
                        </div>

                        <div class="wrapper_for_button" style="margin-top: 50px;">
                            <a class="btn btn-warning" target="_blank" href="{{ url("/app/print/".$application->id) }}">Download</a>
                        </div>
                    </div>
                    {!! csrf_field() !!}
                    <input type="hidden" name="job_application_id" value="{{ $application->id or 0 }}">
                    <input type="hidden" name="redirect" value="/app/show/{{ $application->id }}">
                    <div class="panel panel-default stepBlock stepBlock1">
                        <div class="panel-heading">
                            <div class="pull-left">
                                Personal information
                            </div>
                            <div class="pull-right">
                                <a href="#" class="btn btn-primary edit_mode">Edit</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            {!! csrf_field() !!}
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                            <input type="hidden" name="job_application_id" value="{{ $application->id or 0 }}">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <label for="first_name">First name</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" value="{{ $application['first_name'] or "" }}" />
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <label for="mi">Middle Name</label>
                                    <input type="text" name="mi" id="mi" class="form-control" value="{{ $application['mi'] or "" }}"/>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <label for="last_name">Last name</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control" value="{{ $application['last_name'] or "" }}"/>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <label for="maiden">Former Name</label>
                                    <input type="text" name="maiden" id="maiden" class="form-control" value="{{ $application['maiden'] or "" }}"/>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <label for="current_address">Current Address</label>
                                    <input type="text" name="current_address" id="current_address" class="form-control" value="{{ $application['current_address'] or "" }}"/>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                    <label for="city_state">City</label>
                                    <input type="text" name="city" id="city" class="form-control" value="{{ $application['city'] or "" }}"/>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-6">
                                    <label for="city_state">State</label>
                                    <select name="city_state" id="city_state" class="form-control">
                                        @include("partials.states", [
                                            "application" => isset($application) ? $application : ["city_state" => ""],
                                            "field" => "city_state"
                                        ])
                                    </select>
                                    <!--<input type="text" name="city_state" id="city_state" class="form-control" value="{{ $application['city_state'] or "" }}"/>-->
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <label for="zip_code">Zip Code</label>
                                    <input type="text" name="zip_code" id="zip_code" class="form-control zip_code" value="{{ $application['zip_code'] or "" }}"/>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 form-inline">
                                    <label for="good_until">Good Until Address</label><br />
                                    <input type="text" name="good_until" id="good_until" class="form-control date" value="{{ $application['good_until'] or "" }}"/>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-3 col-md-3">
                                    <label for="permanent_address">Permanent Address</label>
                                    <input type="text" name="permanent_address" id="permanent_address" class="form-control" value="{{ $application['permanent_address'] or "" }}"/>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <label for="permanent_city_state">Permanent City</label>
                                    <input type="text" name="permanent_city" id="permanent_city" class="form-control" placeholder="City" value="{{ $application['permanent_city'] or "" }}"/>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <label for="permanent_city_state">Permanent State</label>
                                    <select name="permanent_city_state" id="permanent_city_state" class="form-control">
                                        @include("partials.states", [
                                            "application" => isset($application) ? $application : ["permanent_city_state" => ""],
                                            "field" => "permanent_city_state"
                                        ])
                                    </select>
                                    <!--<input type="text" name="permanent_city_state" id="permanent_city_state" class="form-control" placeholder="City\State" value="{{ $application['permanent_city_state'] or "" }}"/>-->
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <label for="permanent_zip_code">Permanent Zip Code</label>
                                    <input type="text" name="permanent_zip_code" id="permanent_zip_code" class="form-control zip_code" placeholder="Zip code" value="{{ $application['permanent_zip_code'] or "" }}"/>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <label for="home_phone">Home Phone</label>
                                    <input type="text" name="home_phone" id="home_phone" class="form-control phone" value="{{ $application['home_phone'] or "" }}"/>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <label for="work_phone">Work Phone</label>
                                    <input type="text" name="work_phone" id="work_phone" class="form-control phone" value="{{ $application['work_phone'] or "" }}"/>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <label for="cellular_phone">Cellular Phone</label>
                                    <input type="text" name="cellular_phone" id="cellular_phone" class="form-control phone" value="{{ $application['cellular_phone'] or "" }}"/>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <label for="social_security">Social Security #</label>
                                    <input type="text" name="social_security" id="social_security" class="form-control social_security" value="{{ $application['social_security'] or "" }}"/>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
                                    <label for="birth_date">Date of Birth</label><br />
                                    <input type="text" name="birth_date" id="birth_date" class="form-control date" value="{{ $application['birth_date'] or "" }}"/>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <label for="email_address">Email Address</label>
                                    <input type="text" name="email_address" id="email_address" class="form-control" value="{{ $application['email_address'] or "" }}"/>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <label for="how_did_hear">
                                        Are you eligible to work in the United States?
                                    </label>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <input type="radio" name="united_states_citizen" value="1" @if(isset($application) && $application['united_states_citizen'] == 1) checked @endif/> Yes
                                    <input type="radio" name="united_states_citizen" value="0" @if(isset($application) && $application['united_states_citizen'] == 0) checked @endif/> No
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <label for="emergency_contact">Emergency Contact</label>
                                    <input type="text" name="emergency_contact" id="emergency_contact" class="form-control" value="{{ $application['emergency_contact'] or "" }}"/>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <label for="relationship">Relationship</label>
                                    <input type="text" name="relationship" id="relationship" class="form-control" value="{{ $application['relationship'] or "" }}"/>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <label for="contact_phone">Contact Phone</label>
                                    <input type="text" name="contact_phone" id="contact_phone" class="form-control phone" value="{{ $application['contact_phone'] or "" }}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default stepBlock stepBlock3">
                        <div class="panel-heading">
                            Experience / Expertise
                        </div>
                        <div class="panel-body">
                            @if(!isset($application))
                                <input type="hidden" id="count_specialities" value="0" />
                                @include("applicant.partials.applicationSpecialityItem")
                            @else
                                <input type="hidden" id="count_specialities" value="{{ $application->specialities ? $application->specialities->count()-1 : 0 }}" />
                                <?php $counter = 0;?>
                                @foreach($application->specialities as $item)
                                    @include("applicant.partials.applicationSpecialityItem", compact("item", "counter"))
                                    <?php $counter++;?>
                                @endforeach
                            @endif
                            <br />
                            <div id="specialities"></div>
                            <br />
                            <div>
                                <a href="#" class="btn btn-primary btn-sm add_yet" data-thing="Speciality" data-result-container="specialities">Add Another</a>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-inline">
                                    <label for="">Total years of experience</label>
                                    <input type="number" name="total_years_of_exp" class="form-control" value="{{(isset($application)) ? $application->total_years_of_exp : "" }}" />
                                </div>
                            </div>
                            <h3>EMR Charting Software</h3>
                            @if(!isset($application))
                                <input type="hidden" id="count_emrs" value="0" />
                                @include("applicant.partials.applicationEmrItem")
                            @else
                                <input type="hidden" id="count_emrs" value="{{ $application->specialities ? $application->emrs->count()-1 : 0 }}" />
                                <?php $counter = 0;?>
                                @foreach($application->emrs as $item)
                                    @include("applicant.partials.applicationEmrItem", compact("item", "counter"))
                                    <?php $counter++;?>
                                @endforeach
                            @endif
                            <br />
                            <div id="emrs"></div>
                            <br />
                            <div>
                                <a href="#" class="btn btn-primary btn-sm add_yet" data-thing="Emr" data-result-container="emrs">Add Another</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default stepBlock stepBlock6">
                        <div class="panel-heading">
                            Education
                        </div>
                        <div class="panel-body">
                            Please check here if you do not possess a Registered Nurse license. <input type="checkbox" name="not_rn" value="1" {{ (isset($application) && $application->not_rn) ? "checked" : "" }}>
                            <br />
                            <br />
                            <div class="form-inline">
                                <label for="">What year did you pass your nursing boards?</label>
                                <input type="text" class="form-control date_year" name="edu_pass_board_year" value="{{ $application->edu_pass_board_year or "" }}" />
                            </div>
                            @if(!isset($application))
                                <input type="hidden" id="count_educations" value="0" />
                                @include("applicant.partials.applicationEducationItem")
                            @else
                                <input type="hidden" id="count_educations" value="{{ $application->educations ? $application->educations->count()-1 : 0 }}" />
                                <?php $counter = 0;?>
                                @foreach($application->educations as $item)
                                    @include("applicant.partials.applicationEducationItem", compact("item", "counter"))
                                    <?php $counter++;?>
                                @endforeach
                            @endif
                            <br />
                            <div id="educations"></div>
                            <br />
                            <div>
                                <a href="#" class="btn btn-primary btn-sm add_yet" data-thing="Education" data-result-container="educations">Add Another</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default stepBlock stepBlock2">
                        <div class="panel-heading">
                            Legal Questions
                        </div>
                        <div class="panel-body yes_no">
                            <div class="row margin_top_line">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>Have you ever been convicted of a crime that would prevent employment at a healthcare facility?</label>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="crime" value="1" @if(isset($application) && $application['crime'] == 1) checked @endif/> Yes
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="crime" value="0" @if(isset($application) && $application['crime'] == 0) checked @endif/> No
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-9">
                                    If yes, please detail the circumstances, dates, and final outcome.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description_thing" style="display: {{ (isset($application) && $application['crime']) ? "block" : "none" }}; margin-top: 10px;">
                                    <textarea name="crime_description" id="" cols="30" rows="5" class="form-control">{{ (isset($application) && $application['crime']) ? $application["crime_description"] : "" }}</textarea>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>Have you ever had a license or certification investigated, revoked or suspended?</label>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="license" value="1" @if(isset($application) && $application['license'] == 1) checked @endif/> Yes
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="license" value="0" @if(isset($application) && $application['license'] == 0) checked @endif/> No
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-9">
                                    If yes, please detail the circumstances, dates, and final outcome.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description_thing" style="display: {{ (isset($application) && $application['license']) ? "block" : "none" }}; margin-top: 10px;">
                                    <textarea name="license_description" id="" cols="30" rows="5" class="form-control">{{ (isset($application) && $application['license']) ? $application["license_description"] : "" }}</textarea>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>Do you have any limitations that would restrict you from performing the functions of the position you are applying for?</label>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="limitations" value="1" @if(isset($application) && $application['limitations'] == 1) checked @endif/> Yes
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="limitations" value="0" @if(isset($application) && $application['limitations'] == 0) checked @endif/> No
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-9">
                                    If yes, please provide an explanation.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description_thing" style="display: {{ (isset($application) && $application['limitations']) ? "block" : "none" }}; margin-top: 10px;">
                                    <textarea name="limitations_description" id="" cols="30" rows="5" class="form-control">{{ (isset($application) && $application['limitations']) ? $application["limitations_description"] : "" }}</textarea>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>Are your driving privileges suspended or revoked in any state?</label>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="privileges" value="1" @if(isset($application) && $application['privileges'] == 1) checked @endif/> Yes
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="privileges" value="0" @if(isset($application) && $application['privileges'] == 0) checked @endif/> No
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-9">
                                    If yes, please detail the circumstances, dates, and final outcome.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description_thing" style="display: {{ (isset($application) && $application['privileges']) ? "block" : "none" }}; margin-top: 10px;">
                                    <textarea name="privileges_description" id="" cols="30" rows="5" class="form-control">{{ (isset($application) && $application['privileges']) ? $application["privileges_description"] : "" }}</textarea>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>Has any malpractice claim or suit ever been brought against you?</label>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="malpractice_claim" value="1" @if(isset($application) && $application['malpractice_claim'] == 1) checked @endif/> Yes
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="malpractice_claim" value="0" @if(isset($application) && $application['malpractice_claim'] == 0) checked @endif/> No
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-9">
                                    If yes, please give details of the suit and its current status.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description_thing" style="display: {{ (isset($application) && $application['malpractice_claim']) ? "block" : "none" }}; margin-top: 10px;">
                                    <textarea name="malpractice_claim_description" id="" cols="30" rows="5" class="form-control">{{ (isset($application) && $application['malpractice_claim']) ? $application["malpractice_claim_description"] : "" }}</textarea>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>Are you willing to submit to a criminal background check?</label>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="submit_criminal_background" value="1" @if(isset($application) && $application['submit_criminal_background'] == 1) checked @endif/> Yes
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="radio" name="submit_criminal_background" value="0" @if(isset($application) && $application['submit_criminal_background'] == 0) checked @endif/> No
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default stepBlock stepBlock4">
                        <div class="panel-heading">
                            RN and Therapists Professional Licensure
                        </div>
                        <div class="panel-body">
                            <h3>Original Licensure</h3>
                            @if(!isset($application))
                                <input type="hidden" id="count_licensies" value="0" />
                                @include("applicant.partials.applicationLicenseItem")
                            @else
                                @include("applicant.partials.applicationLicenseItem", [
                                    "item" => $application->licensies->first(),
                                    "counter" => 0
                                ])
                            @endif
                            <br/>
                            <div class="form-inline">
                                <label for="">What date did you pass your original licensure exams?</label>
                                <input type="text" name="date_pass_exams" class="form-control date" value="{{(isset($application)) ? $application->date_pass_exams : "" }}" />
                            </div>

                            <h3>Additional Licensure</h3>
                            <div id="licensies">
                                @if(isset($application))
                                    <input type="hidden" id="count_licensies" value="{{ $application->licensies ? $application->licensies->count()-1 : 0 }}" />

                                    <?php
                                    foreach($application->licensies as $key=>$item) {
                                    if($key == 0) {
                                        continue ;
                                    }
                                    ?>

                                    @include("applicant.partials.applicationLicenseItem",  [
                                        "item" => $item,
                                        "counter" => $key
                                    ])
                                    <?php } ?>
                                @endif
                            </div>
                            <br />
                            <div>
                                <a href="#" class="btn btn-primary btn-sm add_yet" data-thing="License" data-result-container="licensies">Add Another</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default stepBlock stepBlock5">
                        <div class="panel-heading">
                            Professional Certifications
                        </div>
                        <div class="panel-body">
                            @if(!isset($application))
                                <input type="hidden" id="count_certificates" value="0"/>
                                @include("applicant.partials.applicationCertificateItem")
                            @else
                                <input type="hidden" id="count_certificates" value="{{ $application->certificates ? $application->certificates->count()-1 : 0 }}" />
                                <?php $counter = 0;?>
                                @foreach($application->certificates as $item)
                                    @include("applicant.partials.applicationCertificateItem", compact("item", "counter"))
                                    <?php $counter++; ?>
                                @endforeach
                            @endif
                            <br />
                            <div id="certificates"></div>
                            <br />
                            <div>
                                <a href="#" class="btn btn-primary btn-sm add_yet" data-thing="Certificate" data-result-container="certificates">Add Another</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default stepBlock stepBlock7">
                        <div class="panel-heading">
                            Employment History (Most Recent First)
                        </div>
                        <div class="panel-body">
                            @if(!isset($application))
                                <input type="hidden" id="count_employments" value="0" />
                                @include("applicant.partials.applicationEmploymentItem")
                            @else
                                <input type="hidden" id="count_employments" value="{{ $application->employments ? $application->employments->count()-1 : 0 }}" />
                                <?php $counter = 0;?>
                                @foreach($application->employments as $item)
                                    @include("applicant.partials.applicationEmploymentItem", compact("item", "counter"))
                                    <?php $counter++;?>
                                @endforeach
                            @endif
                            <br />
                            <div id="employments"></div>
                            <br />
                            <div>
                                <a href="#" class="btn btn-primary btn-sm add_yet" data-thing="Employment" data-result-container="employments">Add Another</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default stepBlock stepBlock8">
                        <div class="panel-heading">
                            Affirmative Action Frequently Asked Questions
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <h2 style="margin-top:20px; text-align:center">Affirmative Action Frequently Asked Questions</h2>
                                <div class="col-sm-10 col-sm-offset-1" style="margin-top:50px; ">
                                    <b>
                                        What does affirmative action mean?
                                    </b>
                                    <p>
                                        Affirmative action is best understood in terms of the goal: equal employment opportunity for everyone.
                                        Equal Employment Opportunity is the condition where all personnel decisions such as hiring, promotions,
                                        etc., are made without any consideration of race, color, national origin, religion, sex, gender preference,
                                        physical or mental disability, medical condition, ancestry, marital status, age, sexual orientation,
                                        citizenship, or status as a covered veteran. Affirmative action, therefore, is a vehicle by which we seek to
                                        reach the goal of equal employment opportunity. Affirmative Action may take the form of outreach
                                        recruitment efforts, educational opportunities for career advancement, and other training and skill
                                        enhancement programs for all employees, including women, minorities, persons with disabilities and
                                        covered veterans.
                                    </p>
                                    <b>
                                        What is an affirmative action program?
                                    </b>
                                    <p>
                                        An affirmative action program is a tool designed to ensure equal employment opportunity. It includes those
                                        policies, practices and procedures that Vero RN, LLC will implement to ensure that all qualified
                                        applicants and employees are receiving an equal opportunity for recruitment, selection, advancement,
                                        training, development and every other term, condition and privilege of employment.

                                    </p>
                                    <b>
                                        Who benefits from the affirmative action policies and programs?
                                    </b>
                                    <p>
                                        All employees benefit from the affirmative action policies and programs as they help to ensure a fair work
                                        environment for everyone.

                                    </p>
                                    <b>
                                        Do I have to provide the information request on my race, gender, disability and veteran status?
                                    </b>
                                    <p>
                                        Yes. Vero RN, LLC is a federal contractor and therefore is required to report information about its
                                        workforce, including race and sex data, to the federal government. You therefore must provide this
                                        information after you are hired. This information is determined by self-identification. That is, you decide
                                        which of the five racial categories best describes you.
                                        <br>
                                        <br>
                                        This information is used for statistical reporting to the federal government. It has no relation to your
                                        performance and/or your future career with Vero RN, LLC

                                    </p>
                                    <b>
                                        I have a disability but I do not want to disclose this information. What are my rights?
                                    </b>
                                    <p>
                                        Vero RN, LLC will not discriminate or treat you differently in any way after you disclose this
                                        information. We do not need to know your medical history or the specific details regarding your disability
                                        or condition; we only need to know if you require us as your employer to accommodate you in any way.
                                        Our goal is to make your employment a comfortable environment.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <h3>Explanation of Veterans’ Categories</h3>
                                    <p>
                                        For all federal contracts signed on or after December 1, 2003, the categories of protected veterans are
                                        different. However, the government has not yet informed employers exactly when and how they must begin
                                        tracking the new categories. Until we receive further guidance, we are temporarily asking employees to
                                        provide us with the information under both categories.
                                        <b>
                                            You can be covered under more than one
                                            category, so please check all that apply on the “Invitation to Self-Identify” form.
                                        </b>
                                    </p>

                                    <b>
                                        Current Categories
                                    </b>
                                    <ul>
                                        <li>Special Disabled Veteran—Veteran of the U.S. military, ground, naval or air service: <br>

                                            <ul>
                                                <li>
                                                    Who is entitled to compensation (or would be if the person were not receiving military retired pay)
                                                    under laws administered by the U.S. Department of Veterans Affairs for a disability rated at 30
                                                    percent or more or rated at 10 percent or 20 percent in the case of a veteran who has been
                                                    determined by the U.S. Department of Veterans Affairs to have a serious employment handicap; or
                                                </li>
                                                <li>
                                                    Who was discharged or released from active duty because of a service-connected disability.
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            Vietnam-Era Veteran—Either of the following:<br>
                                            <ul>
                                                <li>
                                                    Veteran who served on active duty in the U.S. military, ground, naval or air service for more than
                                                    180 days and was discharged or released with other than a dishonorable discharge, if any part of
                                                    the active duty occurred in the Republic of Vietnam between February 28, 1961, and May 7, 1975,
                                                    or occurred elsewhere between August 5, 1964, and May 7, 1975; or
                                                </li>
                                                <li>
                                                    Veteran who was discharged or released from active duty in the U.S. military ground, naval or air
                                                    service for a service-connected disability if any part of the active duty occurred in the Republic of
                                                    Vietnam between February 28, 1961, and May 7, 1975, or occurred elsewhere between August 5,
                                                    1964, and May 7, 1975.
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            Other veteran who served on active duty in the U.S. military, ground, naval or air service during a war
                                            or in a campaign or expedition for which a campaign badge has been authorized, other than special
                                            disabled veterans or Vietnam-era veterans. (Example: Gulf War veterans).
                                        </li>

                                        <li>
                                            Recently separated veteran: Any veteran during the one-year period beginning on the date of the
                                            veteran’s discharge or release from active duty in the U.S. military, ground, naval or air service.
                                        </li>
                                    </ul>

                                    <b>
                                        New Categories
                                    </b>
                                    <ul>
                                        <li>
                                            Disabled veteran. A veteran who is entitled to compensation (or would be if the person were not
                                            receiving military retired pay) for a service-connected disability under laws administered by the U.S.
                                            Department of Veterans Affairs or a person who was discharged or released from active duty because
                                            of a service-connected disability.
                                        </li>
                                        <li>
                                            Veteran of war, campaign or expedition. Veteran who served on active duty in the U.S. Armed Forces
                                            during a war or in a campaign or expedition for which a campaign badge has been authorized. A list of
                                            these wars, campaigns and expeditions can be found at
                                            http://www.opm.gov/veterans/html/vgmedal2.asp.
                                        </li>
                                        <li>
                                            Noncombat veteran who earned Armed Forces Service Medal. Veteran who, while serving on active
                                            duty in the Armed Forces, participated in a United States military operation for which an Armed
                                            Forces Service Medal was awarded pursuant to Executive Order 12985. This service medal is a
                                            noncombat medal that covers significant U.S. military operations that don’t encounter foreign armed
                                            opposition or imminent hostile action. An explanation and list of operations that qualify for the Armed
                                            Forces Service Medal can be found at http://foxfall.com/csm-common-afsm.htm
                                        </li>
                                    </ul>

                                    <p>
                                        <i>
                                            Recently separated veteran:
                                        </i>
                                        Any veteran during the three-year period beginning on the date of the
                                        veteran’s discharge or release from active duty in the U.S. military, ground, naval or air service.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <h2 style="margin-top:100px; text-align:center">FEDERAL EQUAL OPPORTUNITY QUESTIONS</h2>
                                    <div class="bs-callout bs-callout-info" style="margin-top:50px">
                                        <h4>Invitation to Self-Identify</h4>
                                        <p>
                                            In order to help us comply with Federal Equal Opportunity record keeping and legal requirements, we encourage you to answer the questions below.
                                        </p>
                                        <p>
                                            Please note that Vero RN, LLC. adheres to and believes in equal employment opportunity for all applicants and employees without regard to race, color, sex, age, disability, national origin, religion or veteran status.  This information is strictly confidential and kept separate from employee files.
                                        </p>
                                    </div>

                                    <div class="form-group">
                                        <input name="Declined" type="hidden" value="0">
                                        <input class="check-box" id="Declined" name="Declined" type="checkbox" value="1" @if(isset($application) && $application->Declined) checked="checked" @endif />

                                        <label class="control-label">
                                            I decline to answer these federal equal opportunity questions at this time.
                                        </label>
                                    </div>

                                    <div id="EOEForm" @if(isset($application) && $application->Declined) style="display: none;" @endif>
                                        <div class="form-group" style=" margin-top:40px">
                                            <h4 style="font-weight:bold">1.)	What is your gender?</h4>

                                            <div class="radio">
                                                <label>
                                                    <input id="GenderCode" name="GenderCode" type="radio" value="M" @if(isset($application) && $application->GenderCode == "M") checked="checked" @endif />
                                                    <b>Male</b>
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label>
                                                    <input id="GenderCode" name="GenderCode" type="radio" value="F" @if(isset($application) && $application->GenderCode == "F") checked="checked" @endif>
                                                    <b>Female</b>
                                                </label>
                                            </div>
                                        </div>



                                        <div class="form-group" style=" margin-top:40px">
                                            <h4 style="font-weight:bold">2.)	Are you Hispanic?</h4>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="IsHispanic" name="IsHispanic" type="radio" value="Y" @if(isset($application) && $application->IsHispanic == "Y") checked="checked" @endif />
                                                    <b>Yes</b>
                                                </label>
                                                <p class="help-block">
                                                    Hispanic means a person of Cuban, Mexican, Puerto Rican, South or Central American or other Spanish culture or origin, regardless of race. If you check this box, skip questions 3 and 4.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label>
                                                    <input id="IsHispanic" name="IsHispanic" type="radio" value="N" @if(isset($application) && $application->IsHispanic == "N") checked="checked" @endif>
                                                    <b>No</b>
                                                </label>
                                                <p class="help-block">
                                                    Continue to the next question.
                                                </p>
                                            </div>
                                        </div>



                                        <div class="form-group" style=" margin-top:40px">
                                            <h4 style="font-weight:bold">3.)	What is your race?</h4>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input data-val="true" data-val-number="The field PrimaryRaceID must be a number." id="PrimaryRaceID" name="PrimaryRaceID" type="radio" value="1" @if(isset($application) && $application->PrimaryRaceID == "1") checked="checked" @endif />
                                                    <b>White</b>
                                                </label>
                                                <p class="help-block">
                                                    A person having origins in any of the original peoples of Europe, the Middle East or North Africa.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="PrimaryRaceID" name="PrimaryRaceID" type="radio" value="2" @if(isset($application) && $application->PrimaryRaceID == "2") checked="checked" @endif>
                                                    <b>Black or African American</b>
                                                </label>
                                                <p class="help-block">
                                                    A person having origins in any of the black racial groups of Africa.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="PrimaryRaceID" name="PrimaryRaceID" type="radio" value="3" @if(isset($application) && $application->PrimaryRaceID == "3") checked="checked" @endif>
                                                    <b>Native Hawaiian or Other Pacific Islander</b>
                                                </label>
                                                <p class="help-block">
                                                    A person having origins in any of the peoples of Hawaii, Guam, Samoa or other Pacific Islands.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="PrimaryRaceID" name="PrimaryRaceID" type="radio" value="4" @if(isset($application) && $application->PrimaryRaceID == "4") checked="checked" @endif>
                                                    <b>Asian</b>
                                                </label>
                                                <p class="help-block">
                                                    A person having origins in any of the original peoples of the Far East, Southeast Asia, or the Indian subcontinent, including, for example, Cambodia, China, India, Japan, Korea, Malaysia, Pakistan, the Philippine Islands, Thailand and Vietnam.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="PrimaryRaceID" name="PrimaryRaceID" type="radio" value="5" @if(isset($application) && $application->PrimaryRaceID == "5") checked="checked" @endif>
                                                    <b>American Indian or Alaskan Native</b>
                                                </label>
                                                <p class="help-block">
                                                    A person having origins in any of the original peoples of North and South America (including Central America) and who maintains cultural identification through tribal affiliation or community recognition.
                                                </p>
                                            </div>
                                        </div>

                                        <div class="form-group" style=" margin-top:40px">
                                            <h4 style="font-weight:bold">
                                                4.)	In addition to the race above, do you identify as belonging to any other race?
                                            </h4>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input checked="checked" data-val="true" data-val-number="The field SecondaryRaceID must be a number." id="SecondaryRaceID" name="SecondaryRaceID" type="radio" value="0" @if(isset($application) && $application->SecondaryRaceID == "0") checked="checked" @endif>
                                                    <b>No</b>
                                                </label>
                                                <p class="help-block">
                                                    I do not identify as belonging to any other race.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="SecondaryRaceID" name="SecondaryRaceID" type="radio" value="1" @if(isset($application) && $application->SecondaryRaceID == "1") checked="checked" @endif>
                                                    <b>White</b>
                                                </label>
                                                <p class="help-block">
                                                    A person having origins in any of the original peoples of Europe, the Middle East or North Africa.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="SecondaryRaceID" name="SecondaryRaceID" type="radio" value="2" @if(isset($application) && $application->SecondaryRaceID == "2") checked="checked" @endif>
                                                    <b>Black or African American</b>
                                                </label>
                                                <p class="help-block">
                                                    A person having origins in any of the black racial groups of Africa.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="SecondaryRaceID" name="SecondaryRaceID" type="radio" value="3" @if(isset($application) && $application->SecondaryRaceID == "3") checked="checked" @endif>
                                                    <b>Native Hawaiian or Other Pacific Islander</b>
                                                </label>
                                                <p class="help-block">
                                                    A person having origins in any of the peoples of Hawaii, Guam, Samoa or other Pacific Islands.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="SecondaryRaceID" name="SecondaryRaceID" type="radio" value="4" @if(isset($application) && $application->SecondaryRaceID == "4") checked="checked" @endif>
                                                    <b>Asian</b>
                                                </label>
                                                <p class="help-block">
                                                    A person having origins in any of the original peoples of the Far East, Southeast Asia, or the Indian subcontinent, including, for example, Cambodia, China, India, Japan, Korea, Malaysia, Pakistan, the Philippine Islands, Thailand and Vietnam.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="SecondaryRaceID" name="SecondaryRaceID" type="radio" value="5" @if(isset($application) && $application->SecondaryRaceID == "5") checked="checked" @endif>
                                                    <b>American Indian or Alaskan Native</b>
                                                </label>
                                                <p class="help-block">
                                                    A person having origins in any of the original peoples of North and South America (including Central America) and who maintains cultural identification through tribal affiliation or community recognition.
                                                </p>
                                            </div>
                                        </div>




                                        <div class="form-group" style=" margin-top:40px">
                                            <h4 style="font-weight:bold">
                                                5.)	Veteran Status
                                            </h4>
                                            <p class="help-block">
                                                (Please check all that apply in both vet categories)
                                            </p>

                                            <label class="control-label">Current Categories</label>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input name="IsSpecialDisabled" type="hidden" value="0">
                                                    <input class="check-box" id="IsSpecialDisabled" name="IsSpecialDisabled" type="checkbox" value="1" @if(isset($application) && $application->IsSpecialDisabled) checked="checked" @endif />
                                                    <b>Special disabled veteran</b>
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input name="IsVietnamVeteran" type="hidden" value="0">
                                                    <input class="check-box" id="IsVietnamVeteran" name="IsVietnamVeteran" type="checkbox" value="1" @if(isset($application) && $application->IsVietnamVeteran) checked="checked" @endif />
                                                    <b>Vietnam-Era veteran</b>
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input name="IsOther" type="hidden" value="0">
                                                    <input class="check-box" id="IsOther" name="IsOther" type="checkbox" value="1" @if(isset($application) && $application->IsOther) checked="checked" @endif >
                                                    <b>Other</b>
                                                </label>
                                                <p class="help-block">
                                                    Veteran who served on active duty in the U.S. military, ground, naval or air service during a war or in a campaign or expedition for which a campaign badge has been authorized, other than special disabled veterans or Vietnam-era veterans.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input name="IsRecentlySeparated" type="hidden" value="0">
                                                    <input class="check-box" id="IsRecentlySeparated" name="IsRecentlySeparated" type="checkbox" value="1" @if(isset($application) && $application->IsRecentlySeparated) checked="checked" @endif/>
                                                    <b>Recently separated veteran</b>
                                                </label>
                                                <p class="help-block">
                                                    Any veteran during the one-year period beginning on the date of the veteran’s discharge or release from active duty in the U.S. military, ground, naval or air service
                                                </p>
                                            </div>




                                            <label class="control-label" style="margin-top:20px">New Categories</label>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input name="IsDisabledVeteran" type="hidden" value="0">
                                                    <input class="check-box" id="IsDisabledVeteran" name="IsDisabledVeteran" type="checkbox" value="1" @if(isset($application) && $application->IsDisabledVeteran) checked="checked" @endif />
                                                    <b>Special disabled veteran</b>
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input name="IsWarVeteran" type="hidden" value="0" />
                                                    <input class="check-box" id="IsWarVeteran" name="IsWarVeteran" type="checkbox" value="1" @if(isset($application) && $application->IsWarVeteran) checked="checked" @endif />
                                                    <b>Veteran of war, campaign or expedition</b>
                                                </label>
                                                <p class="help-block">
                                                    Veteran who served on active duty in the U.S. Armed Forces during a war or in a campaign or expedition for which a campaign badge has been authorized.
                                                </p>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input name="IsNonCombat" type="hidden" value="0">
                                                    <input class="check-box" id="IsNonCombat" name="IsNonCombat" type="checkbox" value="1" @if(isset($application) && $application->IsNonCombat) checked="checked" @endif >
                                                    <b>Noncombat veteran who earned an Armed Forces Service Medal</b>
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input name="IsRecentlySeparatedNew" type="hidden" value="0">
                                                    <input class="check-box" id="IsRecentlySeparatedNew" name="IsRecentlySeparatedNew" type="checkbox" value="1" @if(isset($application) && $application->IsRecentlySeparatedNew) checked="checked" @endif >
                                                    <b>Recently separated veteran</b>
                                                </label>
                                                <p class="help-block">
                                                    Any veteran during the three-year period beginning on the date of the veteran’s discharge or release from active duty in the U.S. military, ground, naval or air service.
                                                </p>
                                            </div>


                                        </div>

                                        <div class="form-group" style=" margin-top:40px">
                                            <h4 style="font-weight:bold">6.)	Disabled Status</h4>
                                            <p class="help-block">
                                                (Check one)
                                            </p>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="IsDisabled" name="IsDisabled" type="radio" value="0" @if(isset($application) && !$application->IsDisabled) checked="checked" @endif />
                                                    NonDisabled
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label class="control-label">
                                                    <input id="IsDisabled" name="IsDisabled" type="radio" value="1" @if(isset($application) && $application->IsDisabled) checked="checked" @endif />
                                                    Person with a disability
                                                </label>
                                                <p class="help-block">
                                                    A person who has a physical or mental impairment which substantially limits one or more of the person’s major life activities or who has a record of such an impairment or is regarded as having such an impairment.
                                                </p>
                                            </div>

                                            <label class=" control-label">
                                                Disability Description
                                            </label>
                                            <div class="">
                                                <textarea class="form-control" cols="20" id="DisabilityDescription" name="DisabilityDescription" rows="2">@if(isset($application) && $application->IsDisabled) {{ $application->DisabilityDescription or "" }} @endif</textarea>
                                                <span class="field-validation-valid" data-valmsg-for="DisabilityDescription" data-valmsg-replace="true"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default stepBlock stepBlock9">
                        <div class="panel-heading">
                            Certification
                        </div>
                        <div class="panel-body">
                            <div class="row margin_top_line">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label>Applicant Name</label>
                                    <input type="text" name="name_signature" id="name_signature" class="form-control" value="{{ $application['name_signature'] or "" }}" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label>Date</label>
                                    <input type="text" name="date_signature" id="date_signature" class="form-control date" value="{{ $application['date_signature'] or "" }}" />
                                </div>
                            </div>
                            <p class="margin_top_line">
                                I agree, and it is my intent, to sign this Application by typing my name in the box above and by electronically submitting this Application to Vero RN by selecting the “Submit” button. I understand that signing and submitting this Application in this manner is the legal equivalent of me having placed my handwritten signature on the submitted Application, including this affirmation. I agree that no certification authority or other third party verification is necessary to validate my electronic signature. I understand and agree that by electronically signing and submitting this Application in this manner I am affirming to the truth of the information contained in the Application.
                            </p>
                            <br />
                            <p>
                                Vero RN requires its employment applicants to complete and electronically submit employment applications online. As explained in the online application’s attestation or affirmation statement, signing and submitting the application in this manner is legally equivalent to a handwritten signature by the applicant and the applicant’s affirmation that the contents of the application are true.
                            </p>
                            <br />
                            <br />
                            <div>
                                <input type="submit" value="Submit Application" class="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                    <br />
                </form>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script src="{{ url("/js/libs/jquery.maskedinput.js") }}"></script>
    <script src="{{ url("/js/libs/moment.js") }}"></script>
    <script src="{{ url("/js/libs/combodate.js") }}"></script>
    <script src="{{ url("/js/application.js") }}"></script>
@endsection