<html>
<head>
    <meta charset="utf-8" />

    <style>

        h1{
            font-size: 22px;
        }

        table {
            page-break-inside: avoid;
            width: 100%;
            table-layout: fixed;
        }

        .item_employment td{
            width: 25%;
        }

        table.margin_bottom_40 {
            margin-bottom: 30px;
        }

        tr.first td{
            width: 25%;border-bottom: 1px solid #000;padding-bottom: 5px; font-size: 16px; font-weight: bold;
        }

        tr.one td{
            border-bottom: 1px solid #000;padding-bottom: 5px; font-size: 20px; font-weight: bold;
        }

        tr.line td{
            padding-top: 20px;
        }
        tr.line_sm td{
            padding-top: 10px;
        }


        tr.second td{
            padding-top: 6px;
        }

        tr.first_3 td{
            width: 25%;border-bottom: 1px solid #000;padding-bottom: 5px; font-size: 20px; font-weight: bold;
        }

        tr.question td{}
        tr.padding td{
            padding-top: 10px;
        }
        tr.more_padding td{
            padding-top: 20px;
        }
        tr.answer td{
            font-size: 20px; font-weight: bold;
        }

        .signature {
            font-family: "sundaneseunicode";
        }
    </style>
</head>
<body>
<h1 class="signature">
    APPLICANT PROFILE
</h1>
<table class="margin_bottom_40" cellpadding="0" cellspacing="0">
    <tr class="first">
        <td>{{ $application->first_name }}</td>
        <td>{{ $application->mi }}</td>
        <td>{{ $application->last_name }}</td>
        <td>{{ $application->maiden }}</td>
    </tr>
    <tr class="second">
        <td>First Name</td>
        <td>Middle Name</td>
        <td>Last Name</td>
        <td>Madien</td>
    </tr>

    <tr class="first line">
        <td>{{ $application->current_address }}</td>
        <td>{{ $application->city }} {{ $application->city_state }}</td>
        <td>{{ $application->zip_code }}</td>
        <td>{{ $application->good_until }}</td>
    </tr>
    <tr class="second">
        <td>Current Address</td>
        <td>City/State</td>
        <td>Zip Code</td>
        <td>Good Until</td>
    </tr>
    <tr class="one line">
        <td colspan="4">{{ $application->permanent_address . " " . $application->permanent_city . " ". $application->permanent_city_state . " " .$application->permanent_zip_code }}</td>
    </tr>
    <tr class="second">
        <td colspan="4">Permanent address</td>
    </tr>

    <tr class="first_3 line">
        <td>{{ $application->home_phone }}</td>
        <td>{{ $application->work_phone }}</td>
        <td colspan="2">{{ $application->cellular_phone }}</td>
    </tr>
    <tr class="second">
        <td >Home Phone</td>
        <td >Work Phone</td>
        <td colspan="2" >Cellular Phone</td>
    </tr>

    <tr class="first_3 line">
        <td>{{ $application->social_security }}</td>
        <td>{{ $application->birth_date }}</td>
        <td colspan="2">{{ $application->email_address }}</td>
    </tr>
    <tr class="second">
        <td >Social Security #</td>
        <td >Birth date</td>
        <td colspan="2" >Email Address</td>
    </tr>

    <tr class="first line">
        <td colspan="4">{{ $application->united_states_citizen ? "Yes" : "No" }}</td>
    </tr>
    <tr class="second">
        <td colspan="4">Are you eligible to work in the United States?</td>
    </tr>

    <tr class="first line">
        <td>{{ $application->emergency_contact }}</td>
        <td>{{ $application->relationship }}</td>
        <td colspan="2">{{ $application->contact_phone }}</td>
    </tr>
    <tr class="second">
        <td >Emergency Contact</td>
        <td >Relationship</td>
        <td colspan="2">Contact Phone</td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0" class="margin_bottom_40">
    <tr>
        <td colspan="3"  style="text-align: left"><h1 style="text-align: left">Experience / Expertise</h1></td>
    </tr>
    @foreach($application->specialities as $item)
        @include("applications.partials_print.applicationSpecialityItem", compact("item", "counter"))
    @endforeach
    <tr class="first_3 line">
        <td colspan="3">{{ $application->total_years_of_exp }}</td>
    </tr>
    <tr class="second">
        <td colspan="3">
            Total Years Of Experience
        </td>
    </tr>

    @foreach($application->emrs as $item)
        @include("applications.partials_print.applicationEmrItem", compact("item"))
    @endforeach
</table>

<table cellpadding="0" cellspacing="0" class="margin_bottom_40">
    <tr>
        <th  style="text-align: left"><h1 style="text-align: left">Education</h1></th>
    </tr>
    @foreach($application->educations as $item)
        @include("applications.partials_print.applicationEducationItem", compact("item"))
    @endforeach
</table>

<table class="margin_bottom_40">
    <tr>
        <th  style="text-align: left"><h1 style="text-align: left">Legal Questions</h1></th>
    </tr>
    <tr class="question">
        <td>
            Have you ever been convicted of a crime that would prevent employment at a healthcare facility?
        </td>
    </tr>
    <tr class="answer">
        <td>
            {{ $application->crime ? "Yes" : "No" }}
            {!! $application->crime ? "<br /><br />".$application->crime_description : "" !!}
        </td>
    </tr>
    <tr class="question padding">
        <td>
            Have you ever had a license or certification investigated, revoked or suspended?
        </td>
    </tr>
    <tr class="answer">
        <td>
            {{ $application->license ? "Yes" : "No" }}
            {!! $application->license ? "<br /><br />".$application->license_description : "" !!}
        </td>
    </tr>
    <tr class="question padding">
        <td>
            Do you have any limitations that would restrict you from performing the functions of the position you are applying for?
        </td>
    </tr>
    <tr class="answer">
        <td>
            {{ $application->limitations ? "Yes" : "No" }}
            {!! $application->limitations ? "<br /><br />".$application->limitations_description : "" !!}
        </td>
    </tr>
    <tr class="question more_padding">
        <td>
            Are your driving privileges suspended or revoked in any state?
        </td>
    </tr>
    <tr class="answer">
        <td>
            {{ $application->privileges ? "Yes" : "No" }}
            {!! $application->privileges ? "<br /><br />".$application->privileges_description : "" !!}
        </td>
    </tr>
    <tr class="question padding">
        <td>
            Has any malpractice claim or suit ever been brought against you?
        </td>
    </tr>
    <tr class="answer">
        <td>
            {{ $application->malpractice_claim ? "Yes" : "No" }}
            {!! $application->malpractice_claim ? "<br /><br />".$application->malpractice_claim_description : "" !!}
        </td>
    </tr>
    <tr class="question padding">
        <td>
            Are you willing to submit to a criminal background check
        </td>
    </tr>
    <tr class="answer">
        <td>
            {{ $application->submit_criminal_background ? "Yes" : "No" }}
        </td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0">
    <tr>
        <th colspan="4"  style="text-align: left">
            <h1 style="text-align: left;">RN and Therapists Professional Licensure</h1>
            <br />
            <h3 style="text-align: left;">Original Licensure</h3>
        </th>
    </tr>
    @include("applications.partials_print.applicationLicenseItem", [
        "item" => $application->licensies->first(),
        "counter" => 0
    ])
</table>
<table cellpadding="0" cellspacing="0" >
    <tr class="first line">
        <td>
            {{ $application->date_pass_exams }}
        </td>
    </tr>
    <tr class="second">
        <td>
            What date did you pass your original licensure exams?
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" class="margin_bottom_40">
    <tr>
        <th  style="text-align: left">
            <br />
            <h3 style="text-align: left">Additional Licensure</h3></th>
    </tr>
    <?php foreach($application->licensies as $key=>$item) { ?>
    <?php
    if($key == 0) {
        continue ;
    }
    ?>
    @include("applications.partials_print.applicationLicenseItem", compact("item", "counter"))
    <?php } ?>
</table>

<table cellpadding="0" cellspacing="0" class="margin_bottom_40">
    <tr>
        <th colspan="4" style="text-align: left"><h1 style="text-align: left">Professional Certifications</h1></th>
    </tr>
    @foreach($application->certificates as $item)
        @include("applications.partials_print.applicationCertificateItem", compact("item"))
    @endforeach
</table>

<table class="margin_bottom_40">
    <tr>
        <th  style="text-align: left"><h1>Employment History</h1></th>
    </tr>
</table>
@foreach($application->employments as $key=>$item)
    @include("applications.partials_print.applicationEmploymentItem", compact("item", "key"))
@endforeach

<pagebreak />
<table cellspacing="0" cellpadding="0" style="width: 100%">
    <tr>
        <th colspan="2" style="text-align: left;">
            <h1  style="text-align: left;">
                Certification
            </h1>
        </th>
    </tr>
    <tr class="first line">
        <td style="width: 50%" class="signature">{{ $application->name_signature }}</td>
        <td style="width: 50%">{{ $application->date_signature }}</td>
    </tr>
    <tr class="second">
        <td>Applicant Name</td>
        <td>Date</td>
    </tr>
    <tr class="first line">
        <td colspan="2"><img src="/images/user_names/{{ $application->id }}.jpg"></td>
    </tr>
    <tr class="second">
        <td colspan="2">Applicant Signature</td>
    </tr>
</table>

<p>
    I agree, and it is my intent, to sign this Application by typing my name in the box below and by electronically
    submitting this Application to VeroRN by selecting the "Submit" button. I understand that signing and
    submitting this Application in this manner is the legal equivalent of me having placed my handwritten signature on
    the submitted Application, including this affirmation. I agree that no certification authority or other third party
    verification is necessary to validate my electronic signature. I understand and agree that by electronically signing
    and submitting this Application in this manner I am affirming to the truth of the information contained in the
    Application. VeroRN requires its employment applicants to complete and electronically submit employment
    applications online. As explained in the online application's attestation or affirmation statement, signing and
    submitting the application in this manner is legally equivalent to a handwritten signature by the applicant and the
    applicant's affirmation that the contents of the application are true.
</p>
</body>
</html>