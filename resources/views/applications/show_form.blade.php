@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                @if(\Session("message"))
                    <div class="alert alert-success">
                        {{ \Session("message") }}
                    </div>
                @endif
            </div>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <form action="{{ url("/form/process/".$form->id) }}" method="post" style="position:relative;">
                    <div class="wrapper_for_buttons" style="position:relative;">
                        <div class="wrapper_for_button">
                            <input type="submit" value="apply changes" class="btn btn-success" />
                        </div>

                        <div class="wrapper_for_button" style="margin-top: 50px;">
                            <a class="btn btn-warning" target="_blank" href="{{ url("/app/form/print/".$formProcess->id) }}">Download </a>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <strong>
                                    {{ $form->name }}
                                </strong>
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-primary edit_mode btn-sm">Edit</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            {!! csrf_field() !!}
                            <p>
                                {!! $form->header !!}
                            </p>
                            <input type="hidden" name="redirect" value="/app/form/show/{{ $formProcess->id }}" />
                            <input type="hidden" name="user_id" value="{{ $formProcess->user_id }}" />
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label for="first_name">First name</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" value="{{ $formProcess->first_name or "" }}" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label for="last_name">Last name</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control" value="{{ $formProcess->last_name or "" }}"/>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <label for="date">Date</label>
                                    <input type="text" name="date" id="date" class="form-control date" value="{{ $formProcess->date or "" }}"/>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" id="email " class="form-control" value="{{ $formProcess->email or "" }}"/>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            Please check the box/es below for each group in which you have provided age-appropriate care:
                                        </div>
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>
                                                    A. Newborn/Neonate (birth-30days)
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="checkbox" name="app_care[]" value="A" @if(in_array("A", $appCare)) checked="checked" @endif />
                                                </td>
                                                <td>
                                                    F. Adolescents (12-18 years)
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="checkbox" name="app_care[]" value="F" @if(in_array("F", $appCare)) checked="checked" @endif  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    B. Infant (30 days-1 year)
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="checkbox" name="app_care[]" value="B" @if(in_array("B", $appCare)) checked="checked" @endif  />
                                                </td>
                                                <td>
                                                    G. Young adults (18-39 years)
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="checkbox" name="app_care[]" value="G" @if(in_array("G", $appCare)) checked="checked" @endif  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    C. Toddler (1-3 years)
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="checkbox" name="app_care[]" value="C" @if(in_array("C", $appCare)) checked="checked" @endif />
                                                </td>
                                                <td>
                                                    H. Middle adults (39-64 years)
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="checkbox" name="app_care[]" value="H" @if(in_array("H", $appCare)) checked="checked" @endif  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    D. Preschooler (3-5 years)
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="checkbox" name="app_care[]" value="D" @if(in_array("D", $appCare)) checked="checked" @endif  />
                                                </td>
                                                <td>
                                                    I. Older adults (64-79 years)
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="checkbox" name="app_care[]" value="I" @if(in_array("I", $appCare)) checked="checked" @endif  />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    E. School age children (5-12 years)
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="checkbox" name="app_care[]" value="E" @if(in_array("E", $appCare)) checked="checked" @endif  />
                                                </td>
                                                <td>
                                                    J. Elderly adults (80+ years)
                                                </td>
                                                <td style="text-align: center">
                                                    <input type="checkbox" name="app_care[]" value="J" @if(in_array("J", $appCare)) checked="checked" @endif  />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin_top_line">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    Please indicate your level of experience by checking 1 box in each of the category below (1-less experience to 4-more experience):
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div>
                                        A.   Theory, or only prior observation
                                    </div>
                                    <div>
                                        B.   One - Two years current experience or need minimal assistance
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div>
                                        C.   Less than one-year current experience or any previous experience
                                    </div>
                                    <div>
                                        D.   Two plus years experience or functions independently
                                    </div>
                                </div>
                            </div>
                            <div class="row margin_top_line questions_block">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <?php
                                    $right_ok = 0;
                                    ?>
                                    @foreach($questionBlocks as $questionBlock)
                                        @if(!$questionBlock->left && !$right_ok)
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    @endif
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <strong>{{ $questionBlock->name }}</strong>
                                            </div>
                                            <table class="table table-bordered table-hover">
                                                <thead></thead>
                                                <tbody>
                                                @foreach($questionBlock->items as $question)
                                                    @include("applicant.partials.formQuestionBlockItem", [
                                                        "question" => $question,
                                                        "formProcess" => $formProcess,
                                                        "nested" => 0
                                                    ])
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                    if($questionBlock->left == 0) {
                                        $right_ok = 1;
                                    }
                                    ?>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-default" >
                                <div class="panel-heading">
                                    MY EXPERIENCE IS PRIMARILY IN (Please indicate number of years)
                                </div>
                                <div class="panel-body">
                                    @foreach($form->experiences as $experience)
                                        @if($experience->name)
                                            <?php
                                            if(isset($formProcess)) {
                                                $check = \App\FormProcessExperience::whereFormProcessId($formProcess->id)->whereExpId($experience->id)->first();
                                            }else {
                                                $check = false;
                                            }
                                            ?>

                                            <input type="hidden" name="experience[{{ $experience->id }}][exp_id]" value="{{ $experience->id }}" />
                                            <div class="row margin_top_line">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <input type="hidden" name="experience[{{ $experience->id }}][name]" value="0" />
                                                    <input type="checkbox" name="experience[{{ $experience->id }}][name]" value="1" @if(isset($formProcess) && $check) checked @endif/> {{ $experience->name }}
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-inline">
                                                    <input type="text" name="experience[{{ $experience->id }}][years]" value="{{ $check->years or "" }}" class="form-control"/> &nbsp;&nbsp; year(s)
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Confirmation
                                </div>
                                <div class="panel-body">
                                    <p>
                                        The information I have given is true and accurate to the best of my knowledge. By clicking the Submit button I hereby authorize Vero RN to release this Cath Lab/Cardiac Cath Lab skills checklist to Vero RN-related facilities in consideration of my employment.
                                    </p>
                                </div>
                            </div>
                            <div>
                                <input type="submit" value="Submit application" class="btn btn-primary" />
                            </div>
                            <br />
                        </div>
                    </div>

                </form>
            </div>
            @endsection

@section("scripts")
    <script src="{{ url("/js/application.js") }}"></script>
@endsection