<style>

    table {
        /*
        page-break-inside: avoid;
        width: 100%;
        table-layout: fixed;
        */
    }

    tr.first td{
        width: 25%;border-bottom: 1px solid #000;padding-bottom: 5px; font-size: 20px; font-weight: bold;
    }

    tr.one td{
        border-bottom: 1px solid #000;padding-bottom: 5px; font-size: 20px; font-weight: bold;
    }

    tr.line td{
        padding-top: 30px;
    }
    tr.line_sm td{
        padding-top: 14px;
    }


    tr.second td{
        padding-top: 10px;
    }

    tr.first_3 td{
        width: 25%;border-bottom: 1px solid #000;padding-bottom: 5px; font-size: 20px; font-weight: bold;
    }

    tr.question td{}
    tr.padding td{
        padding-top: 15px;
    }
    tr.more_padding td{
        padding-top: 30px;
    }
    tr.answer td{
        font-size: 20px; font-weight: bold;
    }
    .appCare{
        margin-top: 20px;
        width: 100%;
    }
    .appCare td{
        padding: 5px;
    }

    .questionBlockItem{
        margin-top: 10px;
        width: 100%;;
        /*page-break-inside: avoid;*/
    }
    .questionBlockItem td{
        padding: 0;
        margin: 0;
    }

    .experiences td{
        padding: 5px;
    }
    .experiences th{
        padding: 5px;
    }

    .experiences{
        margin-top: 20px;
        page-break-inside: avoid;
    }

    .border{
        border: 1px solid #000;
    }

</style>

<h1>
    {{ $form->name }} Skills Checklist </h1>
<table style="width: 100%" cellpadding="0" cellspacing="0">
    <tr class="first">
        <td>{{ $formProcess->first_name }}</td>
        <td>{{ $formProcess->last_name }}</td>
    </tr>
    <tr class="second">
        <td>First Name</td>
        <td>Last Name</td>
    </tr>
    <tr class="first line">
        <td>{{ $formProcess->date }}</td>
        <td>{{ $formProcess->email }}</td>
    </tr>
    <tr class="second">
        <td>Date</td>
        <td>Email</td>
    </tr>
</table>

<h3 style="margin-bottom: 0; padding-bottom: 0;">
    Age-appropriate Care Experience
</h3>
<table class="table appCare" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            A. Newborn/Neonate (birth-30days)
        </td>
        <td style="text-align: center">
            <input type="checkbox" name="app_care[]" value="A" @if(in_array("A", $appCare)) checked="checked" @endif />
        </td>
        <td>
            F. Adolescents (12-18 years)
        </td>
        <td style="text-align: center">
            <input type="checkbox" name="app_care[]" value="F" @if(in_array("F", $appCare)) checked="checked" @endif  />
        </td>
    </tr>
    <tr>
        <td>
            B. Infant (30 days-1 year)
        </td>
        <td style="text-align: center">
            <input type="checkbox" name="app_care[]" value="B" @if(in_array("B", $appCare)) checked="checked" @endif  />
        </td>
        <td>
            G. Young adults (18-39 years)
        </td>
        <td style="text-align: center">
            <input type="checkbox" name="app_care[]" value="G" @if(in_array("G", $appCare)) checked="checked" @endif  />
        </td>
    </tr>
    <tr>
        <td>
            C. Toddler (1-3 years)
        </td>
        <td style="text-align: center">
            <input type="checkbox" name="app_care[]" value="C" @if(in_array("C", $appCare)) checked="checked" @endif />
        </td>
        <td>
            H. Middle adults (39-64 years)
        </td>
        <td style="text-align: center">
            <input type="checkbox" name="app_care[]" value="H" @if(in_array("H", $appCare)) checked="checked" @endif  />
        </td>
    </tr>
    <tr>
        <td>
            D. Preschooler (3-5 years)
        </td>
        <td style="text-align: center">
            <input type="checkbox" name="app_care[]" value="D" @if(in_array("D", $appCare)) checked="checked" @endif  />
        </td>
        <td>
            I. Older adults (64-79 years)
        </td>
        <td style="text-align: center">
            <input type="checkbox" name="app_care[]" value="I" @if(in_array("I", $appCare)) checked="checked" @endif  />
        </td>
    </tr>
    <tr>
        <td>
            E. School age children (5-12 years)
        </td>
        <td style="text-align: center">
            <input type="checkbox" name="app_care[]" value="E" @if(in_array("E", $appCare)) checked="checked" @endif  />
        </td>
        <td>
            J. Elderly adults (80+ years)
        </td>
        <td style="text-align: center">
            <input type="checkbox" name="app_care[]" value="J" @if(in_array("J", $appCare)) checked="checked" @endif  />
        </td>
    </tr>
</table>

<p style="font-size: 16px;">
    <strong>A:</strong> Theory, or only prior observation;<br />
    <strong>B :</strong> Less than one-year current experience or any previous experience;<br />
    <strong> C :</strong> One - Two years current experience or need minimal assistanc;<br />
    <strong>D :</strong> Two plus years experience or functions independently;
</p>

<h3 style="margin-bottom: 0; padding-bottom: 0;">Skill checklist</h3>
<?php
$right_ok = 0;
?>
<div style="width:47%;float: left; padding-right: 2%;border-right: 2px solid #000;">
@foreach($questionBlocks as $questionBlock)

        @if(!$questionBlock->left && !$right_ok)
            </div>
            <div style="width: 47%;float: right;page-break-inside: auto;">
        @endif

    <table class="questionBlockItem" cellspacing="0" cellpadding="0">
        <tr>
            <th style="text-align: left; margin-bottom: 2px; width: 96%" ><strong>{{ $questionBlock->name }}</strong></th>
            <th style="width: 1%; text-align: center;">A</th>
            <th style="width: 1%; text-align: center;">B</th>
            <th style="width: 1%; text-align: center;">C</th>
            <th style="width: 1%; text-align: center;">D</th>
        </tr>
        @foreach($questionBlock->items as $question)
            @include("applications.partials_print.formQuestionBlockItem", [
                "question" => $question,
                "formProcess" => $formProcess,
                "nested" => 0
            ])
        @endforeach
    </table>
    <?php
    if($questionBlock->left == 0) {
        $right_ok = 1;
    }
    ?>
@endforeach
</div>
<div style="clear: both;"></div>
@if($form->experiences()->count() > 0)

    <table class="experiences" cellpadding="0" cellspacing="0">
        <tr>
            <th style="float: left;" colspan="2"><h2 class="experiences">My years of experience are in the following area/s:</h2></th>
        </tr>
        <tr>
            <th style="text-align: left;" class="border">
                Area
            </th>
            <th style="text-align: left;" class="border">
                Years
            </th>
        </tr>
        @foreach($form->experiences as $experience)
            @if($experience->name)
                <?php
                if(isset($formProcess)) {
                    $check = \App\FormProcessExperience::whereFormProcessId($formProcess->id)->whereExpId($experience->id)->first();
                }else {
                    $check = false;
                }
                ?>
                <tr>
                    <td class="border">
                        {{ $experience->name }}
                    </td>
                    <td class="border">
                        {{ $check->years or "-" }}
                    </td>
                </tr>
            @endif
        @endforeach
    </table>
@endif

<h2 class="experiences">Confirmation</h2>
<p>
    The information I have given is true and accurate to the best of my knowledge. By clicking the Submit button I hereby authorize Vero RN to release this {{ $form->name }} Skills Checklist to Vero RN-related facilities in consideration of my employment.
</p>

<table cellspacing="0" cellpadding="0" style="width: 100%">
    <tr class="first line">
        <td style="width: 50%">{{ $formProcess->first_name. " ".$formProcess->last_name }}</td>
        <td style="width: 50%">{{ $formProcess->date }}</td>
    </tr>
    <tr class="second">
        <td>Applicant name</td>
        <td>Date</td>
    </tr>
    <tr class="first line">
        <td colspan="2"><img src="/images/user_names/{{ $formProcess->id }}.jpg"></td>
    </tr>
    <tr class="second">
        <td colspan="2">Applicant Signature</td>
    </tr>
</table>