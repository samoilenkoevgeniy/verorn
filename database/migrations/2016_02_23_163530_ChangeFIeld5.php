<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFIeld5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->text("date_pass_exams");
            $table->boolean("not_rn");
            $table->text("edu_pass_board_year");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->dropColumn([
                "date_pass_exams",
                "not_rn",
                "edu_pass_board_year"
            ]);
        });
    }
}
