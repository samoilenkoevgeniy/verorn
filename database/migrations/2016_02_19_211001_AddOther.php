<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOther extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_application_form_question_block_items', function (Blueprint $table) {
            $table->boolean("other");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_application_form_question_block_items', function (Blueprint $table) {
            $table->dropColumn("other");
        });
    }
}
