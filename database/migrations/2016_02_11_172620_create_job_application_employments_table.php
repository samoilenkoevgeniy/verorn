<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationEmploymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_application_employments', function (Blueprint $table) {
            $table->increments('id');
            $table->text("facility");
            $table->text("city_state");
            $table->date("date_start");
            $table->date("date_end")->nullable();
            $table->boolean("present");
            $table->text("supervisor");
            $table->text("phone");
            $table->text("unit");
            $table->text("number_of_beds");
            $table->boolean("travel");
            $table->boolean("fulltime");
            $table->text("charge_experience");
            $table->text("reason_for_leaving");
            $table->boolean("may_contact");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_application_employments');
    }
}
