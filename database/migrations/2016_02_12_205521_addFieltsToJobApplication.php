<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieltsToJobApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->text("first_name");
            $table->text("mi");
            $table->text("last_name");
            $table->text("maiden");
            $table->text("current_address");
            $table->text("city_state");
            $table->text("zip_code");
            $table->text("good_until");
            $table->text("permanent_address");
            $table->text("home_phone");
            $table->text("work_phone");
            $table->text("cellular_phone");
            $table->text("social_security");
            $table->date("birth_date");
            $table->text("email_address");
            $table->text("employment_desired");
            $table->date("date_available");
            $table->text("shift_preferred");
            $table->text("how_did_hear");
            $table->text("emergency_contact");
            $table->text("relationship");
            $table->text("contact_phone");
            $table->text("other");
            $table->boolean("crime");
            $table->boolean("license");
            $table->boolean("limitations");
            $table->boolean("privileges");
            $table->boolean("malpractice_claim");
            $table->boolean("submit_criminal_background");
            $table->text("name_signature");
            $table->date("date_signature");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->dropColumn([
                "first_name",
                "mi",
                "last_name",
                "maiden",
                "current_address",
                "city_state",
                "zip_code",
                "good_until",
                "permanent_address",
                "home_phone",
                "work_phone",
                "cellular_phone",
                "social_security",
                "birth_date",
                "email_address",
                "employment_desired",
                "date_available",
                "shift_preferred",
                "how_did_hear",
                "emergency_contact",
                "relationship",
                "contact_phone",
                "other",
                "crime",
                "license",
                "limitations",
                "privileges",
                "malpractice_claim",
                "submit_criminal_background",
                "name_signature",
                "date_signature",
            ]);
        });
    }
}
