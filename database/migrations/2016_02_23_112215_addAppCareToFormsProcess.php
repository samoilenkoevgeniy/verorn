<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAppCareToFormsProcess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("form_processes", function(Blueprint $table){
            $table->text("app_care");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("form_processes", function(Blueprint $table){
            $table->dropColumn("app_care");
        });
    }
}
