<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFIeld7 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("job_application_employments", function(Blueprint $table){
            $table->text("clinical_reference");
            $table->text("reference_phone_number");
            $table->text("contact_reference");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("job_application_employments", function(Blueprint $table){
            $table->dropColumn([
                "clinical_reference",
                "reference_phone_number",
                "contact_reference"
            ]);
        });
    }
}
