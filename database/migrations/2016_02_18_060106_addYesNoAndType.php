<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYesNoAndType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_application_form_question_block_items', function (Blueprint $table) {
            $table->boolean("yes_no");
            $table->boolean("type");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_application_form_question_block_items', function (Blueprint $table) {
            $table->dropColumn("yes_no");
            $table->dropColumn("type");
        });
    }
}
