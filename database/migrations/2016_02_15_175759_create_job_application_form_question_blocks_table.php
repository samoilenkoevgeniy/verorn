<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationFormQuestionBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_application_form_question_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->text("name");
            $table->boolean("initials");
            $table->integer("form_id", false, true);
            $table->foreign("form_id")
                ->references("id")
                ->on("job_application_forms")
                ->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_application_form_question_blocks');
    }
}
