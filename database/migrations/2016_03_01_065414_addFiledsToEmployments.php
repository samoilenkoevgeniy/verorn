<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledsToEmployments extends Migration
{
    public $fields_for_add = [
        "number_of_beds_facility", "facility_state", "no_of_beds_unit",
        "supervisors_title", "avg_pat_ratio_unit", "position_held", "unit_description",
        "reason_of_leaving", "days_description", "employment_type", "agency_name", "facility_type"
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("job_application_employments", function(Blueprint $table){
            foreach($this->fields_for_add as $item) {
                $table->text($item);
            }
            $table->dropColumn([
                "number_of_beds"
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("job_application_employments", function(Blueprint $table){
            $table->dropColumn($this->fields_for_add);
            $table->text("number_of_beds");
        });
    }
}
