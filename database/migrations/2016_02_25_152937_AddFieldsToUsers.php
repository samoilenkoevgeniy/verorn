<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("users", function(Blueprint $table){
            $table->text("last_name");
            $table->text("country");
            $table->text("country_state_id");
            $table->text("phone");
            $table->integer("profession_id", false, true)->nullable();
            $table->integer("speciality_id", false, true)->nullable();
            $table->integer("years");

            $table->foreign("profession_id")->references("id")->on("professions")->onDelete("set null");
            $table->foreign("speciality_id")->references("id")->on("specialities")->onDelete("set null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
