<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFreaqQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->boolean("Declined");
            $table->text("GenderCode");
            $table->text("IsHispanic");
            $table->text("PrimaryRaceID");
            $table->text("SecondaryRaceID");
            $table->boolean("IsSpecialDisabled");
            $table->boolean("IsVietnamVeteran");
            $table->boolean("IsOther");
            $table->boolean("IsRecentlySeparated");
            $table->boolean("IsDisabledVeteran");
            $table->boolean("IsWarVeteran");
            $table->boolean("IsNonCombat");
            $table->boolean("IsRecentlySeparatedNew");
            $table->boolean("IsDisabled");
            $table->text("DisabilityDescription");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->dropColumn(["Declined"]);
            $table->dropColumn("GenderCode");
            $table->dropColumn("IsHispanic");
            $table->dropColumn(["PrimaryRaceID", "SecondaryRaceID"]);
            $table->dropColumn("IsSpecialDisabled");
            $table->dropColumn("IsVietnamVeteran");
            $table->dropColumn("IsOther");
            $table->dropColumn("IsRecentlySeparated");
            $table->dropColumn("IsDisabledVeteran");
            $table->dropColumn("IsWarVeteran");
            $table->dropColumn("IsNonCombat");
            $table->dropColumn("IsRecentlySeparatedNew");
            $table->dropColumn("IsDisabled");
            $table->dropColumn("DisabilityDescription");
        });
    }
}
