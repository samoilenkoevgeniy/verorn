<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_application_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->text("name");
            $table->text("header");
            $table->boolean("age_specific_block");
            $table->boolean("primary_experience_block");
            $table->boolean("certification_block");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_application_forms');
    }
}
