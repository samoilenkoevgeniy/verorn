<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormProcessQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_process_questions', function (Blueprint $table) {
            $table->increments('id');

            $table->text("value");

            $table->integer("q_id", false, true);
            $table->foreign("q_id")
                ->references("id")
                ->on("job_application_form_question_block_items")
                ->onDelete("cascade");
            $table->integer("form_process_id", false, true);
            $table->foreign("form_process_id")
                ->references("id")
                ->on("form_processes")
                ->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('form_process_questions');
    }
}
