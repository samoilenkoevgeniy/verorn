<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->text("city");
            $table->text("permanent_city");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->dropColumn("city");
            $table->dropColumn("permanent_city");
        });
    }
}
