<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_application_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->text("place");
            $table->text("city_state");
            $table->text("month_year_graduated");
            $table->text("type_of_degree");
            $table->integer("job_application_id", false, true);
            $table->foreign("job_application_id")->references("id")->on("job_applications")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_application_educations');
    }
}
