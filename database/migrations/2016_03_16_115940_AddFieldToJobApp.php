<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToJobApp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->boolean("united_states_citizen");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->dropColumn("united_states_citizen");
        });
    }
}
