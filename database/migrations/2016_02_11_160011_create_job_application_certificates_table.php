<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_application_certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->text("type");
            $table->text("number");
            $table->date("date");
            $table->date("expiration_date");
            $table->integer("job_application_id", false, true);
            $table->foreign("job_application_id")->references("id")->on("job_applications")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_application_certificates');
    }
}
