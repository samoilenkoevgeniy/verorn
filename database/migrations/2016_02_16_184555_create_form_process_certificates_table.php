<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormProcessCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_process_certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->text("value");
            $table->text("date");
            $table->integer("cert_id", false, true);
            $table->foreign("cert_id")
                ->references("id")
                ->on("job_application_form_certificates")
                ->onDelete("cascade");
            $table->integer("form_process_id", false, true);
            $table->foreign("form_process_id")
                ->references("id")
                ->on("form_processes")
                ->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('form_process_certificates');
    }
}
