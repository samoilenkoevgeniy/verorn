<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionSpecialitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profession_specialities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("profession_id", false, true);
            $table->integer("speciality_id", false, true);

            $table->foreign("profession_id")->references("id")->on("professions")->onDelete("cascade");
            $table->foreign("speciality_id")->references("id")->on("specialities")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profession_specialities');
    }
}
