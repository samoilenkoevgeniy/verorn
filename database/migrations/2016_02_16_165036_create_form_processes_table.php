<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_processes', function (Blueprint $table) {
            $table->increments('id');
            $table->text("first_name");
            $table->text("last_name");
            $table->text("social_number");
            $table->text("date");
            $table->text("email");
            $table->string("age_specific_1");
            $table->string("age_specific_2");
            $table->string("age_specific_3");
            $table->text("print_name");
            $table->text("conf_date");
            $table->text("signature");
            $table->integer("user_id", false, true);
            $table->foreign("user_id")
                ->references("id")
                ->on("users")
                ->onDelete("cascade");
            $table->integer("form_id", false, true);
            $table->foreign("form_id")
                ->references("id")
                ->on("job_application_forms")
                ->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('form_processes');
    }
}
