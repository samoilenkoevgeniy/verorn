<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLeftOrRightMarkToForms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("job_application_form_question_blocks", function(Blueprint $table){
            $table->boolean("left");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("job_application_form_question_blocks", function(Blueprint $table){
            $table->dropColumn("left");
        });
    }
}
