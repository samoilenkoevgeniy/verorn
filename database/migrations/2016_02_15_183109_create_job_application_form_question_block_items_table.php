<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationFormQuestionBlockItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_application_form_question_block_items', function (Blueprint $table) {
            $table->increments('id');
            $table->text("text");
            $table->boolean("is_question");
            $table->integer("parent_id", false, true)->nullable();
            $table->foreign("parent_id")
                ->references("id")
                ->on("job_application_form_question_block_items")
                ->onDelete("cascade");
            $table->integer("block_id", false, true);
            $table->foreign("block_id")
                ->references("id")
                ->on("job_application_form_question_blocks")
                ->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_application_form_question_block_items');
    }
}
