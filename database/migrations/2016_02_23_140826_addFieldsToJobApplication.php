<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToJobApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->text("permanent_city_state");
            $table->text("permanent_zip_code");
            $table->text("crime_description");
            $table->text("license_description");
            $table->text("limitations_description");
            $table->text("privileges_description");
            $table->text("malpractice_claim_description");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("job_applications", function(Blueprint $table){
            $table->dropColumn("permanent_city_state");
            $table->dropColumn("permanent_zip_code");
            $table->dropColumn("crime_description");
            $table->dropColumn("license_description");
            $table->dropColumn("limitations_description");
            $table->dropColumn("privileges_description");
            $table->dropColumn("malpractice_claim_description");
        });
    }
}
