<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobApplicationIdToEmployments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("job_application_employments", function(Blueprint $table){
            $table->integer("job_application_id", false, true);
            $table->foreign("job_application_id")->references("id")->on("job_applications")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("job_application_employments", function(Blueprint $table){
            $table->dropForeign("job_application_employments_job_application_id_foreign");
            $table->dropColumn("job_application_id");
        });
    }
}
